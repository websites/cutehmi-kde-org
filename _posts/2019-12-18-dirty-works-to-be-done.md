---
layout: post
title: Dirty works to be done 
author: MP
tags: [todo]
---

Second iteration of CuteHMI project is becoming decent, but we need to sort out
some mundane things.

First thing to focus on is a Git workflow. Normally we are not using merge
requests (except of external contributions), because for small teams it seems to
be more efficient and flexible to work directly on master branch. This has a
disadvantage that code may become broken from time to time, leaving people who
clone repository with broken software. To address this issue we need to
establish stable version of master branch, which would be frequently merged
against master if CI/CD pipelines succeed. Thus master branch would act as some
sort of auto merge-request. Conceptually this would look like on the following
diagram.

![Git workflow](/images/posts/git_workflow.svg)

Some effort is required to implement this behaviour correctly however. I've been
looking for various resources on how to do it.
[GitBot](https://about.gitlab.com/blog/2017/11/02/automating-boring-git-operations-gitlab-ci/)
was quite promising approach, but an idea to give private keys to GitLab in
multi-user environment had to be abandoned, because of security concerns.
Therefore my current idea is to write some custom scripts, which would rely on
GitLab REST API.

Another thing to face is documentation. I realize parts, which are missing are
rather extensive and I promised few months ago to improve it. This covers
providing "Getting Started" section, explaining examples, providing API docs
with code snippets. I would want to test all the examples and code snippets,
which land in Doxygen, so this will require some work. There is also technical
issue, which is that documentation for each branch/tag should be kept in
separate subdirectory denoting branch/tag it has been built from.

Last thing to do is to perform some name refactoring. This involves changing few
class names. I would also like tool names to be consistent with extensions
naming scheme, but I yet need to test it and see if there are any serious
drawbacks. These would be last name refactoring in current iteration, so after
that APIs could be considered stable and whole project should be hopefully
marked as stable for the first time.

