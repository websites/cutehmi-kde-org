---
layout: post
title: Enabling Atom (RSS-like) feeds
author: MP
tags: [rss, atom]
---

Atom (RSS-like) feeds have been enabled on the website.

Feeds can be accessed under following [link]({{site.url}}/feed.xml).

