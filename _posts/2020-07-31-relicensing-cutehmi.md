---
layout: post
title: Relicensing CuteHMI
author: MP
tags: [todo, license]
---

CuteHMI is going to be dually licensed: under the current LGPL license and
additionally under the terms of the 2-clause BSD license.

The main reason for introducing additional license is that Google and Apple
policies are hostile towards GPL and to some extent LGPL on their mobile
platforms [[1]](https://news.ycombinator.com/item?id=20145917),
[[2]](https://teamquest.pl/blog/1193_android-push-gpl),
[[3]](https://developer.apple.com/forums/thread/18922),
[[4]](https://www.reddit.com/r/iOSProgramming/comments/bfrit5/gpl_in_ios_app/),
[[5]](https://opensource.stackexchange.com/questions/8674/gpl-with-license-exception-for-ios).

While we realize that this policy is utterly wrong, currently we just want to
avoid troubles and prevail.

It will be also an opportunity to implement SPDX identifiers.

