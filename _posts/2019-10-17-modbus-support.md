---
layout: post
title: Modbus support
author: MP
tags: [modbus]
---

Modbus support has been restored for second iteration of CuteHMI project.

Previously we had *cutehmi_modbus_1* module along with *CuteHMI.Modbus.1* QML
extension. In second iteration of the project QML extensions and modules have
been unified and everything is simply available as *CuteHMI.Modbus.2* extension.
Another difference is that all configuration is done from QML instead of XML
files. Modbus extension comes with appropriate examples, which are provided as
separate extensions.

Extension is functional, although it still misses Designer support, which
should be reintroduced in near future. Another problem is Android support.
It comes out neither Qt Serial Bus nor Qt Serial Port have
[support](https://doc.qt.io/qt-5/qtmodules.html) for Android.
This means we will have to implement *libmodbus* backend if Android is going to
be supported. Other possible improvements involve addition of model-based API
for accessing registers.

