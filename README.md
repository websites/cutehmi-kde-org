# What is HMI and SCADA?

Software plays increasingly important role in our daily life, filling slowly all
of its aspects. In case of industrial installations its role is critical.
Programmable process controllers (PPC) have replaced relay-based techniques.
These controllers, along with the rest of the hardware and a software form
complex, distributed systems.

![Programmable automation controller](images/pac.png)

HMI (Human Machine Interface) is one of the subsystems of such complex system.
Generally the term refers to any set of means, which allow for interaction
between a human and a machine. Today, in the context of computerized automation
systems HMI is usually a display device with graphical user interface. The
purpose of this device is to allow for a control over some process and visualize
its parameters in a user-friendly fashion. Touch panels are currently most
popular kind of devices, however HMI can also run on desktop PCs, laptops,
tablets, smartphones and devices, which provide augmented reality.

SCADA (Supervisory Control And Data Acquisition) is another term related to
industrial automation. While HMI can be conceptually narrowed to user interface,
SCADA is a much broader concept. Its meaning is a bit fuzzy, but the role of the
SCADA is to control whole process flow, propagate signals, collect data, store
the data and perform data processing. SCADA can involve multiple devices, such
as PPCs, field devices, database servers, HMI panels (HMI is an integral part of
SCADA). It's needless to say that these devices are connected in a network. They
can talk to each other through different types of media and through many
different types of protocols. A flexible software architecture is required to
handle all the pieces of hardware and provide what is needed for the system.

# CuteHMI vision

CuteHMI is meant to be a set of software components (libraries and executables)
targeted at building HMI and SCADA applications.

These components aim to fill the gap, between open-source editions of Qt and
commercial software targeted at physical computing (HMI/SCADA/IoT/BMS/RMS/etc)
build around it, thus providing complete open-source suite for such types of
applications for home and industrial use.

Example use case is to create user interface (HMI) that can be used to
communicate with PPC to turn on air conditioning in the building for example. 

Another use case is to create a daemon which collects the data from temperature
sensors and exposes their actual values through Modbus protocol.

CuteHMI motto is: reusability, modularity, flexibility.

