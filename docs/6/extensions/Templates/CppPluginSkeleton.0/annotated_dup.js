var annotated_dup =
[
    [ "templates", "namespacetemplates.html", [
      [ "cpppluginskeleton", "namespacetemplates_1_1cpppluginskeleton.html", [
        [ "internal", "namespacetemplates_1_1cpppluginskeleton_1_1internal.html", [
          [ "QMLPlugin", "classtemplates_1_1cpppluginskeleton_1_1internal_1_1_q_m_l_plugin.html", "classtemplates_1_1cpppluginskeleton_1_1internal_1_1_q_m_l_plugin" ]
        ] ],
        [ "Exception", "classtemplates_1_1cpppluginskeleton_1_1_exception.html", null ],
        [ "Init", "classtemplates_1_1cpppluginskeleton_1_1_init.html", "classtemplates_1_1cpppluginskeleton_1_1_init" ]
      ] ]
    ] ]
];