var searchData=
[
  ['defaultstate_0',['defaultState',['http://doc.qt.io/qt-5/qhistorystate.html#defaultState-prop',1,'QHistoryState']]],
  ['defaulttransition_1',['defaultTransition',['http://doc.qt.io/qt-5/qhistorystate.html#defaultTransition-prop',1,'QHistoryState']]],
  ['direction_2',['direction',['http://doc.qt.io/qt-5/qabstractanimation.html#direction-prop',1,'QAbstractAnimation::direction()'],['http://doc.qt.io/qt-5/qtimeline.html#direction-prop',1,'QTimeLine::direction()']]],
  ['duration_3',['duration',['http://doc.qt.io/qt-5/qabstractanimation.html#duration-prop',1,'QAbstractAnimation::duration()'],['http://doc.qt.io/qt-5/qpauseanimation.html#duration-prop',1,'QPauseAnimation::duration()'],['http://doc.qt.io/qt-5/qtimeline.html#duration-prop',1,'QTimeLine::duration()'],['http://doc.qt.io/qt-5/qvariantanimation.html#duration-prop',1,'QVariantAnimation::duration()']]],
  ['dynamicsortfilter_4',['dynamicSortFilter',['http://doc.qt.io/qt-5/qsortfilterproxymodel.html#dynamicSortFilter-prop',1,'QSortFilterProxyModel']]]
];
