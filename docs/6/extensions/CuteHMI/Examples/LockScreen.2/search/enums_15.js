var searchData=
[
  ['variant_0',['Variant',['http://doc.qt.io/qt-5/quuid.html#Variant-enum',1,'QUuid']]],
  ['verbositylevel_1',['VerbosityLevel',['http://doc.qt.io/qt-5/qdebug.html#VerbosityLevel-enum',1,'QDebug']]],
  ['version_2',['Version',['http://doc.qt.io/qt-5/qdatastream.html#Version-enum',1,'QDataStream::Version()'],['http://doc.qt.io/qt-5/quuid.html#Version-enum',1,'QUuid::Version()']]],
  ['verticalalignment_3',['VerticalAlignment',['http://doc.qt.io/qt-5/qtextcharformat.html#VerticalAlignment-enum',1,'QTextCharFormat']]],
  ['verticalheaderformat_4',['VerticalHeaderFormat',['http://doc.qt.io/qt-5/qcalendarwidget.html#VerticalHeaderFormat-enum',1,'QCalendarWidget']]],
  ['viewitemfeature_5',['ViewItemFeature',['http://doc.qt.io/qt-5/qstyleoptionviewitem.html#ViewItemFeature-enum',1,'QStyleOptionViewItem']]],
  ['viewitemposition_6',['ViewItemPosition',['http://doc.qt.io/qt-5/qstyleoptionviewitem.html#ViewItemPosition-enum',1,'QStyleOptionViewItem']]],
  ['viewmode_7',['ViewMode',['http://doc.qt.io/qt-5/qfiledialog.html#ViewMode-enum',1,'QFileDialog::ViewMode()'],['http://doc.qt.io/qt-5/qlistview.html#ViewMode-enum',1,'QListView::ViewMode()'],['http://doc.qt.io/qt-5/qmdiarea.html#ViewMode-enum',1,'QMdiArea::ViewMode()']]],
  ['viewportanchor_8',['ViewportAnchor',['http://doc.qt.io/qt-5/qgraphicsview.html#ViewportAnchor-enum',1,'QGraphicsView']]],
  ['viewportupdatemode_9',['ViewportUpdateMode',['http://doc.qt.io/qt-5/qgraphicsview.html#ViewportUpdateMode-enum',1,'QGraphicsView']]],
  ['visibility_10',['Visibility',['http://doc.qt.io/qt-5/qwindow.html#Visibility-enum',1,'QWindow']]]
];
