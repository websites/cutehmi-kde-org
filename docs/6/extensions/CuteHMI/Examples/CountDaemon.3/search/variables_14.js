var searchData=
[
  ['uchar_0',['UChar',['http://doc.qt.io/qt-5/qmetatype.html#Type-enum',1,'QMetaType']]],
  ['udpsocket_1',['UdpSocket',['http://doc.qt.io/qt-5/qabstractsocket.html#SocketType-enum',1,'QAbstractSocket::UdpSocket()'],['http://doc.qt.io/qt-5/qnetworkproxyquery.html#QueryType-enum',1,'QNetworkProxyQuery::UdpSocket()']]],
  ['udptunnelingcapability_2',['UdpTunnelingCapability',['http://doc.qt.io/qt-5/qnetworkproxy.html#Capability-enum',1,'QNetworkProxy']]],
  ['uganda_3',['Uganda',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['ugaritic_4',['Ugaritic',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['ugariticscript_5',['UgariticScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['ui_5fanimatecombo_6',['UI_AnimateCombo',['http://doc.qt.io/qt-5/qt.html#UIEffect-enum',1,'Qt']]],
  ['ui_5fanimatemenu_7',['UI_AnimateMenu',['http://doc.qt.io/qt-5/qt.html#UIEffect-enum',1,'Qt']]],
  ['ui_5fanimatetoolbox_8',['UI_AnimateToolBox',['http://doc.qt.io/qt-5/qt.html#UIEffect-enum',1,'Qt']]],
  ['ui_5fanimatetooltip_9',['UI_AnimateTooltip',['http://doc.qt.io/qt-5/qt.html#UIEffect-enum',1,'Qt']]],
  ['ui_5ffademenu_10',['UI_FadeMenu',['http://doc.qt.io/qt-5/qt.html#UIEffect-enum',1,'Qt']]],
  ['ui_5ffadetooltip_11',['UI_FadeTooltip',['http://doc.qt.io/qt-5/qt.html#UIEffect-enum',1,'Qt']]],
  ['ui_5fgeneral_12',['UI_General',['http://doc.qt.io/qt-5/qt.html#UIEffect-enum',1,'Qt']]],
  ['uighur_13',['Uighur',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['uigur_14',['Uigur',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['uint_15',['UInt',['http://doc.qt.io/qt-5/qmetatype.html#Type-enum',1,'QMetaType::UInt()'],['http://doc.qt.io/qt-5/qvariant-obsolete.html#Type-enum',1,'QVariant::UInt()']]],
  ['ukraine_16',['Ukraine',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['ukrainian_17',['Ukrainian',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['ulong_18',['ULong',['http://doc.qt.io/qt-5/qmetatype.html#Type-enum',1,'QMetaType']]],
  ['ulonglong_19',['ULongLong',['http://doc.qt.io/qt-5/qvariant-obsolete.html#Type-enum',1,'QVariant::ULongLong()'],['http://doc.qt.io/qt-5/qmetatype.html#Type-enum',1,'QMetaType::ULongLong()']]],
  ['unabletodecodeissuerpublickey_20',['UnableToDecodeIssuerPublicKey',['http://doc.qt.io/qt-5/qsslerror.html#SslError-enum',1,'QSslError']]],
  ['unabletodecryptcertificatesignature_21',['UnableToDecryptCertificateSignature',['http://doc.qt.io/qt-5/qsslerror.html#SslError-enum',1,'QSslError']]],
  ['unabletogetissuercertificate_22',['UnableToGetIssuerCertificate',['http://doc.qt.io/qt-5/qsslerror.html#SslError-enum',1,'QSslError']]],
  ['unabletogetlocalissuercertificate_23',['UnableToGetLocalIssuerCertificate',['http://doc.qt.io/qt-5/qsslerror.html#SslError-enum',1,'QSslError']]],
  ['unabletoverifyfirstcertificate_24',['UnableToVerifyFirstCertificate',['http://doc.qt.io/qt-5/qsslerror.html#SslError-enum',1,'QSslError']]],
  ['unboundedintersection_25',['UnboundedIntersection',['http://doc.qt.io/qt-5/qlinef-obsolete.html#IntersectType-enum',1,'QLineF']]],
  ['unbuffered_26',['Unbuffered',['http://doc.qt.io/qt-5/qiodevice.html#OpenModeFlag-enum',1,'QIODevice']]],
  ['unchecked_27',['Unchecked',['http://doc.qt.io/qt-5/qt.html#CheckState-enum',1,'Qt']]],
  ['uncodedlanguages_28',['UncodedLanguages',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['unconnectedstate_29',['UnconnectedState',['http://doc.qt.io/qt-5/qlocalsocket.html#LocalSocketState-enum',1,'QLocalSocket::UnconnectedState()'],['http://doc.qt.io/qt-5/qabstractsocket.html#SocketState-enum',1,'QAbstractSocket::UnconnectedState()']]],
  ['undefined_30',['Undefined',['http://doc.qt.io/qt-5/qcborvalue.html#Type-enum',1,'QCborValue::Undefined()'],['http://doc.qt.io/qt-5/qjsonvalue.html#Type-enum',1,'QJsonValue::Undefined()'],['http://doc.qt.io/qt-5/qnetworkconfiguration.html#StateFlag-enum',1,'QNetworkConfiguration::Undefined()']]],
  ['undefinedvalue_31',['UndefinedValue',['http://doc.qt.io/qt-5/qjsvalue.html#SpecialValue-enum',1,'QJSValue']]],
  ['underlyingsocketerror_32',['UnderlyingSocketError',['http://doc.qt.io/qt-5/qdtls.html#QDtlsError-enum',1,'QDtls']]],
  ['unencryptedmode_33',['UnencryptedMode',['http://doc.qt.io/qt-5/qsslsocket.html#SslMode-enum',1,'QSslSocket']]],
  ['unexpectedbreak_34',['UnexpectedBreak',['http://doc.qt.io/qt-5/qcborerror.html#Code-enum',1,'QCborError']]],
  ['unexpectedelementerror_35',['UnexpectedElementError',['http://doc.qt.io/qt-5/qxmlstreamreader.html#Error-enum',1,'QXmlStreamReader']]],
  ['unfinishedsocketoperationerror_36',['UnfinishedSocketOperationError',['http://doc.qt.io/qt-5/qabstractsocket.html#SocketError-enum',1,'QAbstractSocket']]],
  ['ungrabkeyboard_37',['UngrabKeyboard',['http://doc.qt.io/qt-5/qevent.html#Type-enum',1,'QEvent']]],
  ['ungrabmouse_38',['UngrabMouse',['http://doc.qt.io/qt-5/qevent.html#Type-enum',1,'QEvent']]],
  ['unicode_5f10_5f0_39',['Unicode_10_0',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f11_5f0_40',['Unicode_11_0',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f12_5f0_41',['Unicode_12_0',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f12_5f1_42',['Unicode_12_1',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f13_5f0_43',['Unicode_13_0',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f1_5f1_44',['Unicode_1_1',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f2_5f0_45',['Unicode_2_0',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f2_5f1_5f2_46',['Unicode_2_1_2',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f3_5f0_47',['Unicode_3_0',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f3_5f1_48',['Unicode_3_1',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f3_5f2_49',['Unicode_3_2',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f4_5f0_50',['Unicode_4_0',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f4_5f1_51',['Unicode_4_1',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f5_5f0_52',['Unicode_5_0',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f5_5f1_53',['Unicode_5_1',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f5_5f2_54',['Unicode_5_2',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f6_5f0_55',['Unicode_6_0',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f6_5f1_56',['Unicode_6_1',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f6_5f2_57',['Unicode_6_2',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f6_5f3_58',['Unicode_6_3',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f7_5f0_59',['Unicode_7_0',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f8_5f0_60',['Unicode_8_0',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5f9_5f0_61',['Unicode_9_0',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicode_5faccel_62',['UNICODE_ACCEL',['http://doc.qt.io/qt-5/qt.html#Modifier-enum',1,'Qt']]],
  ['unicode_5funassigned_63',['Unicode_Unassigned',['http://doc.qt.io/qt-5/qchar.html#UnicodeVersion-enum',1,'QChar']]],
  ['unicodeutf8_64',['UnicodeUTF8',['http://doc.qt.io/qt-5/qcoreapplication-obsolete.html#Encoding-enum',1,'QCoreApplication']]],
  ['uniqueconnection_65',['UniqueConnection',['http://doc.qt.io/qt-5/qt.html#ConnectionType-enum',1,'Qt']]],
  ['unitedarabemirates_66',['UnitedArabEmirates',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['unitedkingdom_67',['UnitedKingdom',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['unitedstates_68',['UnitedStates',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['unitedstatesminoroutlyingislands_69',['UnitedStatesMinorOutlyingIslands',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['unitedstatesvirginislands_70',['UnitedStatesVirginIslands',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['unknown_71',['Unknown',['http://doc.qt.io/qt-5/qnetworkinterface.html#InterfaceType-enum',1,'QNetworkInterface::Unknown()'],['http://doc.qt.io/qt-5/qocspresponse.html#QOcspCertificateStatus-enum',1,'QOcspResponse::Unknown()'],['http://doc.qt.io/qt-5/qoperatingsystemversion.html#OSType-enum',1,'QOperatingSystemVersion::Unknown()']]],
  ['unknownaccessibility_72',['UnknownAccessibility',['http://doc.qt.io/qt-5/qnetworkaccessmanager-obsolete.html#NetworkAccessibility-enum',1,'QNetworkAccessManager']]],
  ['unknowncontenterror_73',['UnknownContentError',['http://doc.qt.io/qt-5/qnetworkreply.html#NetworkError-enum',1,'QNetworkReply']]],
  ['unknownerror_74',['UnknownError',['http://doc.qt.io/qt-5/qsystemsemaphore.html#SystemSemaphoreError-enum',1,'QSystemSemaphore::UnknownError()'],['http://doc.qt.io/qt-5/qhostinfo.html#HostInfoError-enum',1,'QHostInfo::UnknownError()'],['http://doc.qt.io/qt-5/qsharedmemory.html#SharedMemoryError-enum',1,'QSharedMemory::UnknownError()'],['http://doc.qt.io/qt-5/qprocess.html#ProcessError-enum',1,'QProcess::UnknownError()'],['http://doc.qt.io/qt-5/qlockfile.html#LockError-enum',1,'QLockFile::UnknownError()'],['http://doc.qt.io/qt-5/qcborerror.html#Code-enum',1,'QCborError::UnknownError()']]],
  ['unknownnetworkerror_75',['UnknownNetworkError',['http://doc.qt.io/qt-5/qnetworkreply.html#NetworkError-enum',1,'QNetworkReply']]],
  ['unknownnetworklayerprotocol_76',['UnknownNetworkLayerProtocol',['http://doc.qt.io/qt-5/qabstractsocket.html#NetworkLayerProtocol-enum',1,'QAbstractSocket']]],
  ['unknownoperation_77',['UnknownOperation',['http://doc.qt.io/qt-5/qnetworkaccessmanager.html#Operation-enum',1,'QNetworkAccessManager']]],
  ['unknownprotocol_78',['UnknownProtocol',['http://doc.qt.io/qt-5/qssl.html#SslProtocol-enum',1,'QSsl']]],
  ['unknownproxyerror_79',['UnknownProxyError',['http://doc.qt.io/qt-5/qnetworkreply.html#NetworkError-enum',1,'QNetworkReply']]],
  ['unknownpurpose_80',['UnknownPurpose',['http://doc.qt.io/qt-5/qnetworkconfiguration.html#Purpose-enum',1,'QNetworkConfiguration']]],
  ['unknownservererror_81',['UnknownServerError',['http://doc.qt.io/qt-5/qnetworkreply.html#NetworkError-enum',1,'QNetworkReply']]],
  ['unknownsessionerror_82',['UnknownSessionError',['http://doc.qt.io/qt-5/qnetworksession.html#SessionError-enum',1,'QNetworkSession']]],
  ['unknownsocketerror_83',['UnknownSocketError',['http://doc.qt.io/qt-5/qlocalsocket.html#LocalSocketError-enum',1,'QLocalSocket::UnknownSocketError()'],['http://doc.qt.io/qt-5/qabstractsocket.html#SocketError-enum',1,'QAbstractSocket::UnknownSocketError()']]],
  ['unknownsockettype_84',['UnknownSocketType',['http://doc.qt.io/qt-5/qabstractsocket.html#SocketType-enum',1,'QAbstractSocket']]],
  ['unknowntype_85',['UnknownType',['http://doc.qt.io/qt-5/qcborerror.html#Code-enum',1,'QCborError::UnknownType()'],['http://doc.qt.io/qt-5/qmetatype.html#Type-enum',1,'QMetaType::UnknownType()']]],
  ['unsafeparameterserror_86',['UnsafeParametersError',['http://doc.qt.io/qt-5/qssldiffiehellmanparameters.html#Error-enum',1,'QSslDiffieHellmanParameters']]],
  ['unset_87',['Unset',['http://doc.qt.io/qt-5/qt.html#HighDpiScaleFactorRoundingPolicy-enum',1,'Qt']]],
  ['unsignedinteger_88',['UnsignedInteger',['http://doc.qt.io/qt-5/qcborstreamreader.html#Type-enum',1,'QCborStreamReader']]],
  ['unsorted_89',['Unsorted',['http://doc.qt.io/qt-5/qdir.html#SortFlag-enum',1,'QDir']]],
  ['unspecified_90',['Unspecified',['http://doc.qt.io/qt-5/qocspresponse.html#QOcspRevocationReason-enum',1,'QOcspResponse']]],
  ['unspecifiederror_91',['UnspecifiedError',['http://doc.qt.io/qt-5/qfiledevice.html#FileError-enum',1,'QFileDevice::UnspecifiedError()'],['http://doc.qt.io/qt-5/qsslerror.html#SslError-enum',1,'QSslError::UnspecifiedError()']]],
  ['unsupportedsocketoperationerror_92',['UnsupportedSocketOperationError',['http://doc.qt.io/qt-5/qlocalsocket.html#LocalSocketError-enum',1,'QLocalSocket::UnsupportedSocketOperationError()'],['http://doc.qt.io/qt-5/qabstractsocket.html#SocketError-enum',1,'QAbstractSocket::UnsupportedSocketOperationError()']]],
  ['unsupportedtype_93',['UnsupportedType',['http://doc.qt.io/qt-5/qcborerror.html#Code-enum',1,'QCborError']]],
  ['unterminatedarray_94',['UnterminatedArray',['http://doc.qt.io/qt-5/qjsonparseerror.html#ParseError-enum',1,'QJsonParseError']]],
  ['unterminatedobject_95',['UnterminatedObject',['http://doc.qt.io/qt-5/qjsonparseerror.html#ParseError-enum',1,'QJsonParseError']]],
  ['unterminatedstring_96',['UnterminatedString',['http://doc.qt.io/qt-5/qjsonparseerror.html#ParseError-enum',1,'QJsonParseError']]],
  ['uparrow_97',['UpArrow',['http://doc.qt.io/qt-5/qt.html#ArrowType-enum',1,'Qt']]],
  ['uparrowcursor_98',['UpArrowCursor',['http://doc.qt.io/qt-5/qt.html#CursorShape-enum',1,'Qt']]],
  ['updatelater_99',['UpdateLater',['http://doc.qt.io/qt-5/qevent.html#Type-enum',1,'QEvent']]],
  ['updaterequest_100',['UpdateRequest',['http://doc.qt.io/qt-5/qevent.html#Type-enum',1,'QEvent']]],
  ['uppercasebase_101',['UppercaseBase',['http://doc.qt.io/qt-5/qtextstream.html#NumberFlag-enum',1,'QTextStream']]],
  ['uppercasedigits_102',['UppercaseDigits',['http://doc.qt.io/qt-5/qtextstream.html#NumberFlag-enum',1,'QTextStream']]],
  ['uppersorbian_103',['UpperSorbian',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['urdu_104',['Urdu',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['urierror_105',['URIError',['http://doc.qt.io/qt-5/qjsvalue.html#ErrorType-enum',1,'QJSValue']]],
  ['url_106',['Url',['http://doc.qt.io/qt-5/qvariant-obsolete.html#Type-enum',1,'QVariant::Url()'],['http://doc.qt.io/qt-5/qcborvalue.html#Type-enum',1,'QCborValue::Url()']]],
  ['urlrequest_107',['UrlRequest',['http://doc.qt.io/qt-5/qnetworkproxyquery.html#QueryType-enum',1,'QNetworkProxyQuery']]],
  ['urlstring_108',['UrlString',['http://doc.qt.io/qt-5/qqmlabstracturlinterceptor.html#DataType-enum',1,'QQmlAbstractUrlInterceptor']]],
  ['uruguay_109',['Uruguay',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['usagepolicies_110',['UsagePolicies',['http://doc.qt.io/qt-5/qnetworksession.html#UsagePolicy-enum',1,'QNetworkSession']]],
  ['usefloat_111',['UseFloat',['http://doc.qt.io/qt-5/qcborvalue.html#EncodingOption-enum',1,'QCborValue']]],
  ['usefloat16_112',['UseFloat16',['http://doc.qt.io/qt-5/qcborvalue.html#EncodingOption-enum',1,'QCborValue']]],
  ['useintegers_113',['UseIntegers',['http://doc.qt.io/qt-5/qcborvalue.html#EncodingOption-enum',1,'QCborValue']]],
  ['user_114',['User',['http://doc.qt.io/qt-5/qcalendar.html#System-enum',1,'QCalendar::User()'],['http://doc.qt.io/qt-5/qevent.html#Type-enum',1,'QEvent::User()'],['http://doc.qt.io/qt-5/qnetworkrequest.html#Attribute-enum',1,'QNetworkRequest::User()'],['http://doc.qt.io/qt-5/qmetatype.html#Type-enum',1,'QMetaType::User()']]],
  ['useraccessoption_115',['UserAccessOption',['http://doc.qt.io/qt-5/qlocalserver.html#SocketOption-enum',1,'QLocalServer']]],
  ['useragentheader_116',['UserAgentHeader',['http://doc.qt.io/qt-5/qnetworkrequest.html#KnownHeaders-enum',1,'QNetworkRequest']]],
  ['userchoice_117',['UserChoice',['http://doc.qt.io/qt-5/qnetworkconfiguration.html#Type-enum',1,'QNetworkConfiguration']]],
  ['userinputresolutionoptions_118',['UserInputResolutionOptions',['http://doc.qt.io/qt-5/qurl.html#UserInputResolutionOption-enum',1,'QUrl']]],
  ['usermax_119',['UserMax',['http://doc.qt.io/qt-5/qnetworkrequest.html#Attribute-enum',1,'QNetworkRequest']]],
  ['userrole_120',['UserRole',['http://doc.qt.io/qt-5/qt.html#ItemDataRole-enum',1,'Qt']]],
  ['userscope_121',['UserScope',['http://doc.qt.io/qt-5/qsettings.html#Scope-enum',1,'QSettings']]],
  ['usertype_122',['UserType',['http://doc.qt.io/qt-5/qvariant-obsolete.html#Type-enum',1,'QVariant']]],
  ['userverifiedredirectpolicy_123',['UserVerifiedRedirectPolicy',['http://doc.qt.io/qt-5/qnetworkrequest.html#RedirectPolicy-enum',1,'QNetworkRequest']]],
  ['useunicodepropertiesoption_124',['UseUnicodePropertiesOption',['http://doc.qt.io/qt-5/qregularexpression.html#PatternOption-enum',1,'QRegularExpression']]],
  ['ushort_125',['UShort',['http://doc.qt.io/qt-5/qmetatype.html#Type-enum',1,'QMetaType']]],
  ['utc_126',['UTC',['http://doc.qt.io/qt-5/qt.html#TimeSpec-enum',1,'Qt']]],
  ['uuid_127',['Uuid',['http://doc.qt.io/qt-5/qvariant-obsolete.html#Type-enum',1,'QVariant::Uuid()'],['http://doc.qt.io/qt-5/qcborvalue.html#Type-enum',1,'QCborValue::Uuid()']]],
  ['uzbek_128',['Uzbek',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['uzbekistan_129',['Uzbekistan',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]]
];
