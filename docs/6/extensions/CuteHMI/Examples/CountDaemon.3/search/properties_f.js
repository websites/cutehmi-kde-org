var searchData=
[
  ['selectedindexes_0',['selectedIndexes',['http://doc.qt.io/qt-5/qitemselectionmodel.html#selectedIndexes-prop',1,'QItemSelectionModel']]],
  ['senderobject_1',['senderObject',['http://doc.qt.io/qt-5/qsignaltransition.html#senderObject-prop',1,'QSignalTransition']]],
  ['signal_2',['signal',['http://doc.qt.io/qt-5/qsignaltransition.html#signal-prop',1,'QSignalTransition']]],
  ['singleshot_3',['singleShot',['http://doc.qt.io/qt-5/qtimer.html#singleShot-prop',1,'QTimer']]],
  ['socketoptions_4',['socketOptions',['http://doc.qt.io/qt-5/qlocalserver.html#socketOptions-prop',1,'QLocalServer']]],
  ['sortcasesensitivity_5',['sortCaseSensitivity',['http://doc.qt.io/qt-5/qsortfilterproxymodel.html#sortCaseSensitivity-prop',1,'QSortFilterProxyModel']]],
  ['sortrole_6',['sortRole',['http://doc.qt.io/qt-5/qsortfilterproxymodel.html#sortRole-prop',1,'QSortFilterProxyModel']]],
  ['sourcemodel_7',['sourceModel',['http://doc.qt.io/qt-5/qabstractproxymodel.html#sourceModel-prop',1,'QAbstractProxyModel']]],
  ['sourcestate_8',['sourceState',['http://doc.qt.io/qt-5/qabstracttransition.html#sourceState-prop',1,'QAbstractTransition']]],
  ['stacksize_9',['stackSize',['http://doc.qt.io/qt-5/qthreadpool.html#stackSize-prop',1,'QThreadPool']]],
  ['startvalue_10',['startValue',['http://doc.qt.io/qt-5/qvariantanimation.html#startValue-prop',1,'QVariantAnimation']]],
  ['state_11',['state',['http://doc.qt.io/qt-5/qabstractanimation.html#state-prop',1,'QAbstractAnimation']]],
  ['status_12',['status',['http://doc.qt.io/qt-5/qqmlcomponent.html#status-prop',1,'QQmlComponent']]],
  ['suffixes_13',['suffixes',['http://doc.qt.io/qt-5/qmimetype.html#suffixes-prop',1,'QMimeType']]]
];
