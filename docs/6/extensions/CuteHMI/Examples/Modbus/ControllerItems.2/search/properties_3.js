var searchData=
[
  ['databits_0',['dataBits',['../../../../Modbus.3/classcutehmi_1_1modbus_1_1_r_t_u_server.html#a57f4112455007ea23019e0164c611e63',1,'cutehmi::modbus::RTUServer::dataBits()'],['../../../../Modbus.3/classcutehmi_1_1modbus_1_1_r_t_u_client.html#afce6389d851636cb81eb4b1544a5c69f',1,'cutehmi::modbus::RTUClient::dataBits()']]],
  ['date_1',['date',['http://doc.qt.io/qt-5/qdatetimeedit.html#date-prop',1,'QDateTimeEdit']]],
  ['dateeditacceptdelay_2',['dateEditAcceptDelay',['http://doc.qt.io/qt-5/qcalendarwidget.html#dateEditAcceptDelay-prop',1,'QCalendarWidget']]],
  ['dateeditenabled_3',['dateEditEnabled',['http://doc.qt.io/qt-5/qcalendarwidget.html#dateEditEnabled-prop',1,'QCalendarWidget']]],
  ['datetime_4',['dateTime',['http://doc.qt.io/qt-5/qdatetimeedit.html#dateTime-prop',1,'QDateTimeEdit']]],
  ['decimals_5',['decimals',['http://doc.qt.io/qt-5/qdoublevalidator.html#decimals-prop',1,'QDoubleValidator::decimals()'],['http://doc.qt.io/qt-5/qdoublespinbox.html#decimals-prop',1,'QDoubleSpinBox::decimals()']]],
  ['default_6',['default',['http://doc.qt.io/qt-5/qpushbutton.html#default-prop',1,'QPushButton']]],
  ['defaultalignment_7',['defaultAlignment',['http://doc.qt.io/qt-5/qheaderview.html#defaultAlignment-prop',1,'QHeaderView']]],
  ['defaultdropaction_8',['defaultDropAction',['http://doc.qt.io/qt-5/qabstractitemview.html#defaultDropAction-prop',1,'QAbstractItemView']]],
  ['defaultfont_9',['defaultFont',['http://doc.qt.io/qt-5/qtextdocument.html#defaultFont-prop',1,'QTextDocument']]],
  ['defaultsectionsize_10',['defaultSectionSize',['http://doc.qt.io/qt-5/qheaderview.html#defaultSectionSize-prop',1,'QHeaderView']]],
  ['defaultstate_11',['defaultState',['http://doc.qt.io/qt-5/qhistorystate.html#defaultState-prop',1,'QHistoryState']]],
  ['defaultstylesheet_12',['defaultStyleSheet',['http://doc.qt.io/qt-5/qtextdocument.html#defaultStyleSheet-prop',1,'QTextDocument']]],
  ['defaultsuffix_13',['defaultSuffix',['http://doc.qt.io/qt-5/qfiledialog.html#defaultSuffix-prop',1,'QFileDialog']]],
  ['defaulttextoption_14',['defaultTextOption',['http://doc.qt.io/qt-5/qtextdocument.html#defaultTextOption-prop',1,'QTextDocument']]],
  ['defaulttransition_15',['defaultTransition',['http://doc.qt.io/qt-5/qhistorystate.html#defaultTransition-prop',1,'QHistoryState']]],
  ['defaultup_16',['defaultUp',['http://doc.qt.io/qt-5/qmenubar.html#defaultUp-prop',1,'QMenuBar']]],
  ['delegate_17',['delegate',['../../../../Modbus.3/class_cute_h_m_i_1_1_modbus_1_1_coil_item.html#a7b46f1f6fb72e8cbe59f2c8a0a5ed332',1,'CuteHMI::Modbus::CoilItem::delegate()'],['../../../../Modbus.3/class_cute_h_m_i_1_1_modbus_1_1_input_register_item.html#a656bca0b798a76adab1147d927e75ed8',1,'CuteHMI::Modbus::InputRegisterItem::delegate()'],['../../../../Modbus.3/class_cute_h_m_i_1_1_modbus_1_1_holding_register_item.html#ada93db5e02da9e933dc84938238e3746',1,'CuteHMI::Modbus::HoldingRegisterItem::delegate()'],['../../../../Modbus.3/class_cute_h_m_i_1_1_modbus_1_1_discrete_input_item.html#a384e28ac0d86b86528c3d0fa88675755',1,'CuteHMI::Modbus::DiscreteInputItem::delegate()']]],
  ['delegateproperty_18',['delegateProperty',['../../../../Modbus.3/class_cute_h_m_i_1_1_modbus_1_1_coil_item.html#a170ea82cad954125ef9e48d2d9d3483c',1,'CuteHMI::Modbus::CoilItem::delegateProperty()'],['../../../../Modbus.3/class_cute_h_m_i_1_1_modbus_1_1_discrete_input_item.html#a43c7b13ac10e82f1963b5cf3b52971bc',1,'CuteHMI::Modbus::DiscreteInputItem::delegateProperty()'],['../../../../Modbus.3/class_cute_h_m_i_1_1_modbus_1_1_holding_register_item.html#a29a9e73527a5077e32fab211f63a594c',1,'CuteHMI::Modbus::HoldingRegisterItem::delegateProperty()'],['../../../../Modbus.3/class_cute_h_m_i_1_1_modbus_1_1_input_register_item.html#ac1c70863490af380b1fb8fd12ef945ab',1,'CuteHMI::Modbus::InputRegisterItem::delegateProperty()']]],
  ['delta_19',['delta',['http://doc.qt.io/qt-5/qpangesture.html#delta-prop',1,'QPanGesture']]],
  ['depth_20',['depth',['http://doc.qt.io/qt-5/qscreen.html#depth-prop',1,'QScreen']]],
  ['description_21',['description',['http://doc.qt.io/qt-5/qcommandlinkbutton.html#description-prop',1,'QCommandLinkButton']]],
  ['desktopfilename_22',['desktopFileName',['http://doc.qt.io/qt-5/qguiapplication.html#desktopFileName-prop',1,'QGuiApplication']]],
  ['detailedtext_23',['detailedText',['http://doc.qt.io/qt-5/qmessagebox.html#detailedText-prop',1,'QMessageBox::detailedText()'],['../../../../../CuteHMI.2/classcutehmi_1_1_message.html#aef9e3e3852608680dd5bf45d5a7dd73c',1,'cutehmi::Message::detailedText()']]],
  ['device_24',['device',['../../../../Modbus.3/classcutehmi_1_1modbus_1_1_abstract_register_controller.html#a32c7c37c7db6c65a3a34e7ea0e13a107',1,'cutehmi::modbus::AbstractRegisterController']]],
  ['devicepixelratio_25',['devicePixelRatio',['http://doc.qt.io/qt-5/qscreen.html#devicePixelRatio-prop',1,'QScreen']]],
  ['digitcount_26',['digitCount',['http://doc.qt.io/qt-5/qlcdnumber.html#digitCount-prop',1,'QLCDNumber']]],
  ['direction_27',['direction',['http://doc.qt.io/qt-5/qabstractanimation.html#direction-prop',1,'QAbstractAnimation::direction()'],['http://doc.qt.io/qt-5/qtimeline.html#direction-prop',1,'QTimeLine::direction()']]],
  ['disconnectlatency_28',['disconnectLatency',['../../../../Modbus.3/classcutehmi_1_1modbus_1_1_dummy_client.html#ae1dd41917a9db777db8db5f30cd6a2f8',1,'cutehmi::modbus::DummyClient']]],
  ['displayedsections_29',['displayedSections',['http://doc.qt.io/qt-5/qdatetimeedit.html#displayedSections-prop',1,'QDateTimeEdit']]],
  ['displayformat_30',['displayFormat',['http://doc.qt.io/qt-5/qdatetimeedit.html#displayFormat-prop',1,'QDateTimeEdit']]],
  ['displayintegerbase_31',['displayIntegerBase',['http://doc.qt.io/qt-5/qspinbox.html#displayIntegerBase-prop',1,'QSpinBox']]],
  ['displaytext_32',['displayText',['http://doc.qt.io/qt-5/qlineedit.html#displayText-prop',1,'QLineEdit']]],
  ['docknestingenabled_33',['dockNestingEnabled',['http://doc.qt.io/qt-5/qmainwindow.html#dockNestingEnabled-prop',1,'QMainWindow']]],
  ['dockoptions_34',['dockOptions',['http://doc.qt.io/qt-5/qmainwindow.html#dockOptions-prop',1,'QMainWindow']]],
  ['document_35',['document',['http://doc.qt.io/qt-5/qtextedit.html#document-prop',1,'QTextEdit']]],
  ['documentmargin_36',['documentMargin',['http://doc.qt.io/qt-5/qtextdocument.html#documentMargin-prop',1,'QTextDocument']]],
  ['documentmode_37',['documentMode',['http://doc.qt.io/qt-5/qmainwindow.html#documentMode-prop',1,'QMainWindow::documentMode()'],['http://doc.qt.io/qt-5/qmdiarea.html#documentMode-prop',1,'QMdiArea::documentMode()'],['http://doc.qt.io/qt-5/qtabbar.html#documentMode-prop',1,'QTabBar::documentMode()'],['http://doc.qt.io/qt-5/qtabwidget.html#documentMode-prop',1,'QTabWidget::documentMode()']]],
  ['documenttitle_38',['documentTitle',['http://doc.qt.io/qt-5/qplaintextedit.html#documentTitle-prop',1,'QPlainTextEdit::documentTitle()'],['http://doc.qt.io/qt-5/qtextedit.html#documentTitle-prop',1,'QTextEdit::documentTitle()']]],
  ['doubleclickinterval_39',['doubleClickInterval',['http://doc.qt.io/qt-5/qapplication.html#doubleClickInterval-prop',1,'QApplication']]],
  ['doubledecimals_40',['doubleDecimals',['http://doc.qt.io/qt-5/qinputdialog.html#doubleDecimals-prop',1,'QInputDialog']]],
  ['doublemaximum_41',['doubleMaximum',['http://doc.qt.io/qt-5/qinputdialog.html#doubleMaximum-prop',1,'QInputDialog']]],
  ['doubleminimum_42',['doubleMinimum',['http://doc.qt.io/qt-5/qinputdialog.html#doubleMinimum-prop',1,'QInputDialog']]],
  ['doublestep_43',['doubleStep',['http://doc.qt.io/qt-5/qinputdialog.html#doubleStep-prop',1,'QInputDialog']]],
  ['doublevalue_44',['doubleValue',['http://doc.qt.io/qt-5/qinputdialog.html#doubleValue-prop',1,'QInputDialog']]],
  ['down_45',['down',['http://doc.qt.io/qt-5/qabstractbutton.html#down-prop',1,'QAbstractButton']]],
  ['dragdropmode_46',['dragDropMode',['http://doc.qt.io/qt-5/qabstractitemview.html#dragDropMode-prop',1,'QAbstractItemView']]],
  ['dragdropoverwritemode_47',['dragDropOverwriteMode',['http://doc.qt.io/qt-5/qabstractitemview.html#dragDropOverwriteMode-prop',1,'QAbstractItemView']]],
  ['dragenabled_48',['dragEnabled',['http://doc.qt.io/qt-5/qlineedit.html#dragEnabled-prop',1,'QLineEdit::dragEnabled()'],['http://doc.qt.io/qt-5/qabstractitemview.html#dragEnabled-prop',1,'QAbstractItemView::dragEnabled()']]],
  ['dragmode_49',['dragMode',['http://doc.qt.io/qt-5/qgraphicsview.html#dragMode-prop',1,'QGraphicsView']]],
  ['drawbase_50',['drawBase',['http://doc.qt.io/qt-5/qtabbar.html#drawBase-prop',1,'QTabBar']]],
  ['duplicatesenabled_51',['duplicatesEnabled',['http://doc.qt.io/qt-5/qcombobox.html#duplicatesEnabled-prop',1,'QComboBox']]],
  ['duration_52',['duration',['http://doc.qt.io/qt-5/qabstractanimation.html#duration-prop',1,'QAbstractAnimation::duration()'],['http://doc.qt.io/qt-5/qpauseanimation.html#duration-prop',1,'QPauseAnimation::duration()'],['http://doc.qt.io/qt-5/qtimeline.html#duration-prop',1,'QTimeLine::duration()'],['http://doc.qt.io/qt-5/qvariantanimation.html#duration-prop',1,'QVariantAnimation::duration()']]],
  ['dynamicsortfilter_53',['dynamicSortFilter',['http://doc.qt.io/qt-5/qsortfilterproxymodel.html#dynamicSortFilter-prop',1,'QSortFilterProxyModel']]]
];
