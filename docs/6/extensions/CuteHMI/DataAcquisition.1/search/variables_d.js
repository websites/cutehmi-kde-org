var searchData=
[
  ['nabataeanscript_0',['NabataeanScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['nama_1',['Nama',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['name_2',['name',['../../SharedDatabase.1/classcutehmi_1_1shareddatabase_1_1internal_1_1_database_config_1_1_data.html#ad79ddc21f78ed3d3b98196268358547e',1,'cutehmi::shareddatabase::internal::DatabaseConfig::Data']]],
  ['name_3',['Name',['http://doc.qt.io/qt-5/qdir.html#SortFlag-enum',1,'QDir::Name()'],['http://doc.qt.io/qt-5/quuid.html#Version-enum',1,'QUuid::Name()']]],
  ['nameandvalueonly_4',['NameAndValueOnly',['http://doc.qt.io/qt-5/qnetworkcookie.html#RawForm-enum',1,'QNetworkCookie']]],
  ['namedbinding_5',['NamedBinding',['http://doc.qt.io/qt-5/qsqlresult.html#BindingSyntax-enum',1,'QSqlResult']]],
  ['namedplaceholders_6',['NamedPlaceholders',['http://doc.qt.io/qt-5/qsqldriver.html#DriverFeature-enum',1,'QSqlDriver']]],
  ['namibia_7',['Namibia',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['narrow_8',['Narrow',['http://doc.qt.io/qt-5/qchar.html#Decomposition-enum',1,'QChar']]],
  ['narrowformat_9',['NarrowFormat',['http://doc.qt.io/qt-5/qlocale.html#FormatType-enum',1,'QLocale']]],
  ['nativeformat_10',['NativeFormat',['http://doc.qt.io/qt-5/qsettings.html#Format-enum',1,'QSettings']]],
  ['nativegesture_11',['NativeGesture',['http://doc.qt.io/qt-5/qevent.html#Type-enum',1,'QEvent']]],
  ['naurucountry_12',['NauruCountry',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['naurulanguage_13',['NauruLanguage',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['navaho_14',['Navaho',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['navigationmodecursorauto_15',['NavigationModeCursorAuto',['http://doc.qt.io/qt-5/qt.html#NavigationMode-enum',1,'Qt']]],
  ['navigationmodecursorforcevisible_16',['NavigationModeCursorForceVisible',['http://doc.qt.io/qt-5/qt.html#NavigationMode-enum',1,'Qt']]],
  ['navigationmodekeypaddirectional_17',['NavigationModeKeypadDirectional',['http://doc.qt.io/qt-5/qt.html#NavigationMode-enum',1,'Qt']]],
  ['navigationmodekeypadtaborder_18',['NavigationModeKeypadTabOrder',['http://doc.qt.io/qt-5/qt.html#NavigationMode-enum',1,'Qt']]],
  ['navigationmodenone_19',['NavigationModeNone',['http://doc.qt.io/qt-5/qt.html#NavigationMode-enum',1,'Qt']]],
  ['nbsp_20',['Nbsp',['http://doc.qt.io/qt-5/qchar.html#SpecialCharacter-enum',1,'QChar']]],
  ['ncs_21',['NCS',['http://doc.qt.io/qt-5/quuid.html#Variant-enum',1,'QUuid']]],
  ['ncurvetypes_22',['NCurveTypes',['http://doc.qt.io/qt-5/qeasingcurve.html#Type-enum',1,'QEasingCurve']]],
  ['ndonga_23',['Ndonga',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['needsconstruction_24',['NeedsConstruction',['http://doc.qt.io/qt-5/qmetatype.html#TypeFlag-enum',1,'QMetaType']]],
  ['needsdestruction_25',['NeedsDestruction',['http://doc.qt.io/qt-5/qmetatype.html#TypeFlag-enum',1,'QMetaType']]],
  ['negativeinteger_26',['NegativeInteger',['http://doc.qt.io/qt-5/qcborstreamreader.html#Type-enum',1,'QCborStreamReader']]],
  ['nepal_27',['Nepal',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['nepali_28',['Nepali',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['nestingtoodeep_29',['NestingTooDeep',['http://doc.qt.io/qt-5/qcborerror.html#Code-enum',1,'QCborError']]],
  ['netherlands_30',['Netherlands',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['networkerror_31',['NetworkError',['http://doc.qt.io/qt-5/qabstractsocket.html#SocketError-enum',1,'QAbstractSocket']]],
  ['networkreplyupdated_32',['NetworkReplyUpdated',['http://doc.qt.io/qt-5/qevent.html#Type-enum',1,'QEvent']]],
  ['networksessionfailederror_33',['NetworkSessionFailedError',['http://doc.qt.io/qt-5/qnetworkreply.html#NetworkError-enum',1,'QNetworkReply']]],
  ['networksessionrequired_34',['NetworkSessionRequired',['http://doc.qt.io/qt-5/qnetworkconfigurationmanager.html#Capability-enum',1,'QNetworkConfigurationManager']]],
  ['newari_35',['Newari',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['newascript_36',['NewaScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['newcaledonia_37',['NewCaledonia',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['newonly_38',['NewOnly',['http://doc.qt.io/qt-5/qiodevice.html#OpenModeFlag-enum',1,'QIODevice']]],
  ['newtailuescript_39',['NewTaiLueScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['newzealand_40',['NewZealand',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['nextprotocolnegotiationnegotiated_41',['NextProtocolNegotiationNegotiated',['http://doc.qt.io/qt-5/qsslconfiguration.html#NextProtocolNegotiationStatus-enum',1,'QSslConfiguration']]],
  ['nextprotocolnegotiationnone_42',['NextProtocolNegotiationNone',['http://doc.qt.io/qt-5/qsslconfiguration.html#NextProtocolNegotiationStatus-enum',1,'QSslConfiguration']]],
  ['nextprotocolnegotiationunsupported_43',['NextProtocolNegotiationUnsupported',['http://doc.qt.io/qt-5/qsslconfiguration.html#NextProtocolNegotiationStatus-enum',1,'QSslConfiguration']]],
  ['ngiemboon_44',['Ngiemboon',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['ngomba_45',['Ngomba',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['nicaragua_46',['Nicaragua',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['niger_47',['Niger',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['nigeria_48',['Nigeria',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['nigerianpidgin_49',['NigerianPidgin',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['niue_50',['Niue',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['nko_51',['Nko',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['nkoscript_52',['NkoScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['no_5fbutton_53',['NO_BUTTON',['../../../CuteHMI.2/classcutehmi_1_1_message.html#ac42671a854e0bd2cc30b627750e14040a9bc7cd60141474b2479554167e23872a',1,'cutehmi::Message']]],
  ['noalpha_54',['NoAlpha',['http://doc.qt.io/qt-5/qt.html#ImageConversionFlag-enum',1,'Qt']]],
  ['noarrow_55',['NoArrow',['http://doc.qt.io/qt-5/qt.html#ArrowType-enum',1,'Qt']]],
  ['nobackgroundtrafficpolicy_56',['NoBackgroundTrafficPolicy',['http://doc.qt.io/qt-5/qnetworksession.html#UsagePolicy-enum',1,'QNetworkSession']]],
  ['nobreak_57',['NoBreak',['http://doc.qt.io/qt-5/qchar.html#Decomposition-enum',1,'QChar']]],
  ['nobrush_58',['NoBrush',['http://doc.qt.io/qt-5/qt.html#BrushStyle-enum',1,'Qt']]],
  ['nobutton_59',['NoButton',['http://doc.qt.io/qt-5/qt.html#MouseButton-enum',1,'Qt']]],
  ['noclip_60',['NoClip',['http://doc.qt.io/qt-5/qt.html#ClipOperation-enum',1,'Qt']]],
  ['nocommonancestorfortransitionerror_61',['NoCommonAncestorForTransitionError',['http://doc.qt.io/qt-5/qstatemachine.html#Error-enum',1,'QStateMachine']]],
  ['nocompression_62',['NoCompression',['http://doc.qt.io/qt-5/qresource.html#Compression-enum',1,'QResource']]],
  ['nocontextmenu_63',['NoContextMenu',['http://doc.qt.io/qt-5/qt.html#ContextMenuPolicy-enum',1,'Qt']]],
  ['nodecomposition_64',['NoDecomposition',['http://doc.qt.io/qt-5/qchar.html#Decomposition-enum',1,'QChar']]],
  ['nodefaultstateinhistorystateerror_65',['NoDefaultStateInHistoryStateError',['http://doc.qt.io/qt-5/qstatemachine.html#Error-enum',1,'QStateMachine']]],
  ['nodockwidgetarea_66',['NoDockWidgetArea',['http://doc.qt.io/qt-5/qt.html#DockWidgetArea-enum',1,'Qt']]],
  ['nodot_67',['NoDot',['http://doc.qt.io/qt-5/qdir.html#Filter-enum',1,'QDir']]],
  ['nodotanddotdot_68',['NoDotAndDotDot',['http://doc.qt.io/qt-5/qdir.html#Filter-enum',1,'QDir']]],
  ['nodotdot_69',['NoDotDot',['http://doc.qt.io/qt-5/qdir.html#Filter-enum',1,'QDir']]],
  ['nodropshadowwindowhint_70',['NoDropShadowWindowHint',['http://doc.qt.io/qt-5/qt.html#WindowType-enum',1,'Qt']]],
  ['noerror_71',['NoError',['http://doc.qt.io/qt-5/qsharedmemory.html#SharedMemoryError-enum',1,'QSharedMemory::NoError()'],['http://doc.qt.io/qt-5/qsqlerror.html#ErrorType-enum',1,'QSqlError::NoError()'],['http://doc.qt.io/qt-5/qsettings.html#Status-enum',1,'QSettings::NoError()'],['http://doc.qt.io/qt-5/qdnslookup.html#Error-enum',1,'QDnsLookup::NoError()'],['http://doc.qt.io/qt-5/qdtls.html#QDtlsError-enum',1,'QDtls::NoError()'],['http://doc.qt.io/qt-5/qhostinfo.html#HostInfoError-enum',1,'QHostInfo::NoError()'],['http://doc.qt.io/qt-5/qnetworkreply.html#NetworkError-enum',1,'QNetworkReply::NoError()'],['http://doc.qt.io/qt-5/qjsvalue.html#ErrorType-enum',1,'QJSValue::NoError()'],['http://doc.qt.io/qt-5/qssldiffiehellmanparameters.html#Error-enum',1,'QSslDiffieHellmanParameters::NoError()'],['http://doc.qt.io/qt-5/qxmlstreamreader.html#Error-enum',1,'QXmlStreamReader::NoError()'],['http://doc.qt.io/qt-5/qsystemsemaphore.html#SystemSemaphoreError-enum',1,'QSystemSemaphore::NoError()'],['http://doc.qt.io/qt-5/qstatemachine.html#Error-enum',1,'QStateMachine::NoError()'],['http://doc.qt.io/qt-5/qlockfile.html#LockError-enum',1,'QLockFile::NoError()'],['http://doc.qt.io/qt-5/qjsonparseerror.html#ParseError-enum',1,'QJsonParseError::NoError()'],['http://doc.qt.io/qt-5/qcborerror.html#Code-enum',1,'QCborError::NoError()'],['http://doc.qt.io/qt-5/qsslerror.html#SslError-enum',1,'QSslError::NoError()'],['http://doc.qt.io/qt-5/qfiledevice.html#FileError-enum',1,'QFileDevice::NoError()']]],
  ['nofilter_72',['NoFilter',['http://doc.qt.io/qt-5/qdir.html#Filter-enum',1,'QDir']]],
  ['nofocus_73',['NoFocus',['http://doc.qt.io/qt-5/qt.html#FocusPolicy-enum',1,'Qt']]],
  ['nofocusreason_74',['NoFocusReason',['http://doc.qt.io/qt-5/qt.html#FocusReason-enum',1,'Qt']]],
  ['noformatconversion_75',['NoFormatConversion',['http://doc.qt.io/qt-5/qt.html#ImageConversionFlag-enum',1,'Qt']]],
  ['nogesture_76',['NoGesture',['http://doc.qt.io/qt-5/qt.html#GestureState-enum',1,'Qt']]],
  ['noinitialstateerror_77',['NoInitialStateError',['http://doc.qt.io/qt-5/qstatemachine.html#Error-enum',1,'QStateMachine']]],
  ['nointersection_78',['NoIntersection',['http://doc.qt.io/qt-5/qlinef-obsolete.html#IntersectType-enum',1,'QLineF']]],
  ['noitemflags_79',['NoItemFlags',['http://doc.qt.io/qt-5/qt.html#ItemFlag-enum',1,'Qt']]],
  ['noiteratorflags_80',['NoIteratorFlags',['http://doc.qt.io/qt-5/qdiriterator.html#IteratorFlag-enum',1,'QDirIterator']]],
  ['nolayoutchangehint_81',['NoLayoutChangeHint',['http://doc.qt.io/qt-5/qabstractitemmodel.html#LayoutChangeHint-enum',1,'QAbstractItemModel']]],
  ['nolesssaferedirectpolicy_82',['NoLessSafeRedirectPolicy',['http://doc.qt.io/qt-5/qnetworkrequest.html#RedirectPolicy-enum',1,'QNetworkRequest']]],
  ['nomatch_83',['NoMatch',['http://doc.qt.io/qt-5/qregularexpression.html#MatchType-enum',1,'QRegularExpression']]],
  ['nomatchoption_84',['NoMatchOption',['http://doc.qt.io/qt-5/qregularexpression.html#MatchOption-enum',1,'QRegularExpression']]],
  ['nomodifier_85',['NoModifier',['http://doc.qt.io/qt-5/qt.html#KeyboardModifier-enum',1,'Qt']]],
  ['nonclientareamousebuttondblclick_86',['NonClientAreaMouseButtonDblClick',['http://doc.qt.io/qt-5/qevent.html#Type-enum',1,'QEvent']]],
  ['nonclientareamousebuttonpress_87',['NonClientAreaMouseButtonPress',['http://doc.qt.io/qt-5/qevent.html#Type-enum',1,'QEvent']]],
  ['nonclientareamousebuttonrelease_88',['NonClientAreaMouseButtonRelease',['http://doc.qt.io/qt-5/qevent.html#Type-enum',1,'QEvent']]],
  ['nonclientareamousemove_89',['NonClientAreaMouseMove',['http://doc.qt.io/qt-5/qevent.html#Type-enum',1,'QEvent']]],
  ['none_90',['None',['http://doc.qt.io/qt-5/qocspresponse.html#QOcspRevocationReason-enum',1,'QOcspResponse::None()'],['http://doc.qt.io/qt-5/qurl.html#UrlFormattingOption-enum',1,'QUrl::None()'],['http://doc.qt.io/qt-5/qevent.html#Type-enum',1,'QEvent::None()']]],
  ['nonmodal_91',['NonModal',['http://doc.qt.io/qt-5/qt.html#WindowModality-enum',1,'Qt']]],
  ['nonrecursive_92',['NonRecursive',['http://doc.qt.io/qt-5/qreadwritelock.html#RecursionMode-enum',1,'QReadWriteLock::NonRecursive()'],['http://doc.qt.io/qt-5/qmutex.html#RecursionMode-enum',1,'QMutex::NonRecursive()']]],
  ['noopaquedetection_93',['NoOpaqueDetection',['http://doc.qt.io/qt-5/qt.html#ImageConversionFlag-enum',1,'Qt']]],
  ['nooption_94',['NoOption',['http://doc.qt.io/qt-5/qabstractitemmodel.html#CheckIndexOption-enum',1,'QAbstractItemModel']]],
  ['nooptions_95',['NoOptions',['http://doc.qt.io/qt-5/qfiledevice.html#MemoryMapFlags-enum',1,'QFileDevice::NoOptions()'],['http://doc.qt.io/qt-5/qlocalserver.html#SocketOption-enum',1,'QLocalServer::NoOptions()']]],
  ['nopatternoption_96',['NoPatternOption',['http://doc.qt.io/qt-5/qregularexpression.html#PatternOption-enum',1,'QRegularExpression']]],
  ['nopeercertificate_97',['NoPeerCertificate',['http://doc.qt.io/qt-5/qsslerror.html#SslError-enum',1,'QSslError']]],
  ['nopen_98',['NoPen',['http://doc.qt.io/qt-5/qt.html#PenStyle-enum',1,'Qt']]],
  ['nopolicy_99',['NoPolicy',['http://doc.qt.io/qt-5/qnetworksession.html#UsagePolicy-enum',1,'QNetworkSession']]],
  ['noproxy_100',['NoProxy',['http://doc.qt.io/qt-5/qnetworkproxy.html#ProxyType-enum',1,'QNetworkProxy']]],
  ['norfolkisland_101',['NorfolkIsland',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['normal_102',['Normal',['http://doc.qt.io/qt-5/qqmlproperty.html#PropertyTypeCategory-enum',1,'QQmlProperty']]],
  ['normaleventpriority_103',['NormalEventPriority',['http://doc.qt.io/qt-5/qt.html#EventPriority-enum',1,'Qt']]],
  ['normalexit_104',['NormalExit',['http://doc.qt.io/qt-5/qprocess.html#ExitStatus-enum',1,'QProcess']]],
  ['normalizationform_5fc_105',['NormalizationForm_C',['http://doc.qt.io/qt-5/qstring.html#NormalizationForm-enum',1,'QString']]],
  ['normalizationform_5fd_106',['NormalizationForm_D',['http://doc.qt.io/qt-5/qstring.html#NormalizationForm-enum',1,'QString']]],
  ['normalizationform_5fkc_107',['NormalizationForm_KC',['http://doc.qt.io/qt-5/qstring.html#NormalizationForm-enum',1,'QString']]],
  ['normalizationform_5fkd_108',['NormalizationForm_KD',['http://doc.qt.io/qt-5/qstring.html#NormalizationForm-enum',1,'QString']]],
  ['normalizepathsegments_109',['NormalizePathSegments',['http://doc.qt.io/qt-5/qurl.html#UrlFormattingOption-enum',1,'QUrl']]],
  ['normalmatch_110',['NormalMatch',['http://doc.qt.io/qt-5/qregularexpression.html#MatchType-enum',1,'QRegularExpression']]],
  ['normalpriority_111',['NormalPriority',['http://doc.qt.io/qt-5/qnetworkrequest.html#Priority-enum',1,'QNetworkRequest::NormalPriority()'],['http://doc.qt.io/qt-5/qthread.html#Priority-enum',1,'QThread::NormalPriority()'],['http://doc.qt.io/qt-5/qstatemachine.html#EventPriority-enum',1,'QStateMachine::NormalPriority()']]],
  ['northernluri_112',['NorthernLuri',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['northernmarianaislands_113',['NorthernMarianaIslands',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['northernsami_114',['NorthernSami',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['northernsotho_115',['NorthernSotho',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['northernthai_116',['NorthernThai',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['northkorea_117',['NorthKorea',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['northndebele_118',['NorthNdebele',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['norway_119',['Norway',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['norwegian_120',['Norwegian',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['norwegianbokmal_121',['NorwegianBokmal',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['norwegiannynorsk_122',['NorwegianNynorsk',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['noscrollphase_123',['NoScrollPhase',['http://doc.qt.io/qt-5/qt.html#ScrollPhase-enum',1,'Qt']]],
  ['nosection_124',['NoSection',['http://doc.qt.io/qt-5/qt.html#WindowFrameSection-enum',1,'Qt']]],
  ['nosort_125',['NoSort',['http://doc.qt.io/qt-5/qdir.html#SortFlag-enum',1,'QDir']]],
  ['nosslsupport_126',['NoSslSupport',['http://doc.qt.io/qt-5/qsslerror.html#SslError-enum',1,'QSslError']]],
  ['nosymlinks_127',['NoSymLinks',['http://doc.qt.io/qt-5/qdir.html#Filter-enum',1,'QDir']]],
  ['notabfocus_128',['NoTabFocus',['http://doc.qt.io/qt-5/qt.html#TabFocusBehavior-enum',1,'Qt']]],
  ['notaccessible_129',['NotAccessible',['http://doc.qt.io/qt-5/qnetworkaccessmanager-obsolete.html#NetworkAccessibility-enum',1,'QNetworkAccessManager']]],
  ['notatboundary_130',['NotAtBoundary',['http://doc.qt.io/qt-5/qtextboundaryfinder.html#BoundaryReason-enum',1,'QTextBoundaryFinder']]],
  ['notavailable_131',['NotAvailable',['http://doc.qt.io/qt-5/qnetworksession.html#State-enum',1,'QNetworkSession']]],
  ['notextinteraction_132',['NoTextInteraction',['http://doc.qt.io/qt-5/qt.html#TextInteractionFlag-enum',1,'Qt']]],
  ['notfound_133',['NotFound',['http://doc.qt.io/qt-5/qsharedmemory.html#SharedMemoryError-enum',1,'QSharedMemory::NotFound()'],['http://doc.qt.io/qt-5/qsystemsemaphore.html#SystemSemaphoreError-enum',1,'QSystemSemaphore::NotFound()']]],
  ['notfounderror_134',['NotFoundError',['http://doc.qt.io/qt-5/qdnslookup.html#Error-enum',1,'QDnsLookup']]],
  ['notoken_135',['NoToken',['http://doc.qt.io/qt-5/qxmlstreamreader.html#TokenType-enum',1,'QXmlStreamReader']]],
  ['notoolbararea_136',['NoToolBarArea',['http://doc.qt.io/qt-5/qt.html#ToolBarArea-enum',1,'Qt']]],
  ['notopen_137',['NotOpen',['http://doc.qt.io/qt-5/qiodevice.html#OpenModeFlag-enum',1,'QIODevice']]],
  ['notransformation_138',['NoTransformation',['http://doc.qt.io/qt-5/qcborvalue.html#EncodingOption-enum',1,'QCborValue']]],
  ['notrunning_139',['NotRunning',['http://doc.qt.io/qt-5/qtimeline.html#State-enum',1,'QTimeLine::NotRunning()'],['http://doc.qt.io/qt-5/qprocess.html#ProcessState-enum',1,'QProcess::NotRunning()']]],
  ['notwellformederror_140',['NotWellFormedError',['http://doc.qt.io/qt-5/qxmlstreamreader.html#Error-enum',1,'QXmlStreamReader']]],
  ['noupdate_141',['NoUpdate',['http://doc.qt.io/qt-5/qitemselectionmodel.html#SelectionFlag-enum',1,'QItemSelectionModel']]],
  ['ns_142',['NS',['http://doc.qt.io/qt-5/qdnslookup.html#Type-enum',1,'QDnsLookup']]],
  ['nsizehints_143',['NSizeHints',['http://doc.qt.io/qt-5/qt.html#SizeHint-enum',1,'Qt']]],
  ['nuer_144',['Nuer',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['null_145',['Null',['http://doc.qt.io/qt-5/qqmlincubator.html#Status-enum',1,'QQmlIncubator::Null()'],['http://doc.qt.io/qt-5/qqmlcomponent.html#Status-enum',1,'QQmlComponent::Null()'],['http://doc.qt.io/qt-5/qhostaddress.html#SpecialAddress-enum',1,'QHostAddress::Null()'],['http://doc.qt.io/qt-5/qjsonvalue.html#Type-enum',1,'QJsonValue::Null()'],['http://doc.qt.io/qt-5/qchar.html#SpecialCharacter-enum',1,'QChar::Null()'],['http://doc.qt.io/qt-5/qcborvalue.html#Type-enum',1,'QCborValue::Null()']]],
  ['nullptr_146',['Nullptr',['http://doc.qt.io/qt-5/qmetatype.html#Type-enum',1,'QMetaType']]],
  ['nullvalue_147',['NullValue',['http://doc.qt.io/qt-5/qjsvalue.html#SpecialValue-enum',1,'QJSValue']]],
  ['number_5fdecimaldigit_148',['Number_DecimalDigit',['http://doc.qt.io/qt-5/qchar.html#Category-enum',1,'QChar']]],
  ['number_5fletter_149',['Number_Letter',['http://doc.qt.io/qt-5/qchar.html#Category-enum',1,'QChar']]],
  ['number_5fother_150',['Number_Other',['http://doc.qt.io/qt-5/qchar.html#Category-enum',1,'QChar']]],
  ['numberflags_151',['NumberFlags',['http://doc.qt.io/qt-5/qtextstream.html#NumberFlag-enum',1,'QTextStream']]],
  ['numberoptions_152',['NumberOptions',['http://doc.qt.io/qt-5/qlocale.html#NumberOption-enum',1,'QLocale']]],
  ['nyanja_153',['Nyanja',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['nyankole_154',['Nyankole',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]]
];
