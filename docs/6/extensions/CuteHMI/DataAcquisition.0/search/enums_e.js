var searchData=
[
  ['objectownership_0',['ObjectOwnership',['http://doc.qt.io/qt-5/qqmlengine.html#ObjectOwnership-enum',1,'QQmlEngine']]],
  ['openmodeflag_1',['OpenModeFlag',['http://doc.qt.io/qt-5/qiodevice.html#OpenModeFlag-enum',1,'QIODevice']]],
  ['operation_2',['Operation',['http://doc.qt.io/qt-5/qnetworkaccessmanager.html#Operation-enum',1,'QNetworkAccessManager']]],
  ['optionsafterpositionalargumentsmode_3',['OptionsAfterPositionalArgumentsMode',['http://doc.qt.io/qt-5/qcommandlineparser.html#OptionsAfterPositionalArgumentsMode-enum',1,'QCommandLineParser']]],
  ['orientation_4',['Orientation',['http://doc.qt.io/qt-5/qt.html#Orientation-enum',1,'Qt']]],
  ['ostype_5',['OSType',['http://doc.qt.io/qt-5/qoperatingsystemversion.html#OSType-enum',1,'QOperatingSystemVersion']]]
];
