var hierarchy =
[
    [ "cutehmi::dataacquisition::internal::EventCollective::ColumnValues", "structcutehmi_1_1dataacquisition_1_1internal_1_1_event_collective_1_1_column_values.html", null ],
    [ "cutehmi::dataacquisition::internal::HistoryCollective::ColumnValues", "structcutehmi_1_1dataacquisition_1_1internal_1_1_history_collective_1_1_column_values.html", null ],
    [ "cutehmi::dataacquisition::internal::RecencyCollective::ColumnValues", "structcutehmi_1_1dataacquisition_1_1internal_1_1_recency_collective_1_1_column_values.html", null ],
    [ "cutehmi::dataacquisition::internal::DbServiceableMixin< DERIVED >", "classcutehmi_1_1dataacquisition_1_1internal_1_1_db_serviceable_mixin.html", null ],
    [ "cutehmi::dataacquisition::internal::DbServiceableMixin< AbstractListModel >", "classcutehmi_1_1dataacquisition_1_1internal_1_1_db_serviceable_mixin.html", [
      [ "cutehmi::dataacquisition::AbstractListModel", "classcutehmi_1_1dataacquisition_1_1_abstract_list_model.html", [
        [ "cutehmi::dataacquisition::EventModel", "classcutehmi_1_1dataacquisition_1_1_event_model.html", null ],
        [ "cutehmi::dataacquisition::HistoryModel", "classcutehmi_1_1dataacquisition_1_1_history_model.html", null ],
        [ "cutehmi::dataacquisition::RecencyModel", "classcutehmi_1_1dataacquisition_1_1_recency_model.html", null ]
      ] ]
    ] ],
    [ "cutehmi::dataacquisition::internal::DbServiceableMixin< EventWriter >", "classcutehmi_1_1dataacquisition_1_1internal_1_1_db_serviceable_mixin.html", [
      [ "cutehmi::dataacquisition::EventWriter", "classcutehmi_1_1dataacquisition_1_1_event_writer.html", null ]
    ] ],
    [ "cutehmi::dataacquisition::internal::DbServiceableMixin< HistoryWriter >", "classcutehmi_1_1dataacquisition_1_1internal_1_1_db_serviceable_mixin.html", [
      [ "cutehmi::dataacquisition::HistoryWriter", "classcutehmi_1_1dataacquisition_1_1_history_writer.html", null ]
    ] ],
    [ "cutehmi::dataacquisition::internal::DbServiceableMixin< RecencyWriter >", "classcutehmi_1_1dataacquisition_1_1internal_1_1_db_serviceable_mixin.html", [
      [ "cutehmi::dataacquisition::RecencyWriter", "classcutehmi_1_1dataacquisition_1_1_recency_writer.html", null ]
    ] ],
    [ "cutehmi::dataacquisition::internal::ModelMixin< DERIVED >", "classcutehmi_1_1dataacquisition_1_1internal_1_1_model_mixin.html", null ],
    [ "cutehmi::dataacquisition::internal::ModelMixin< EventModel >", "classcutehmi_1_1dataacquisition_1_1internal_1_1_model_mixin.html", [
      [ "cutehmi::dataacquisition::EventModel", "classcutehmi_1_1dataacquisition_1_1_event_model.html", null ]
    ] ],
    [ "cutehmi::dataacquisition::internal::ModelMixin< HistoryModel >", "classcutehmi_1_1dataacquisition_1_1internal_1_1_model_mixin.html", [
      [ "cutehmi::dataacquisition::HistoryModel", "classcutehmi_1_1dataacquisition_1_1_history_model.html", null ]
    ] ],
    [ "cutehmi::dataacquisition::internal::ModelMixin< RecencyModel >", "classcutehmi_1_1dataacquisition_1_1internal_1_1_model_mixin.html", [
      [ "cutehmi::dataacquisition::RecencyModel", "classcutehmi_1_1dataacquisition_1_1_recency_model.html", null ]
    ] ],
    [ "cutehmi::NonCopyable", "../../CuteHMI.2/classcutehmi_1_1_non_copyable.html", [
      [ "cutehmi::Initializer< Init >", "../../CuteHMI.2/classcutehmi_1_1_initializer.html", [
        [ "cutehmi::dataacquisition::Init", "classcutehmi_1_1dataacquisition_1_1_init.html", null ]
      ] ]
    ] ],
    [ "QException", "http://doc.qt.io/qt-5/qexception.html", [
      [ "cutehmi::Exception", "../../CuteHMI.2/classcutehmi_1_1_exception.html", [
        [ "cutehmi::ExceptionMixin< Exception >", "../../CuteHMI.2/classcutehmi_1_1_exception_mixin.html", [
          [ "cutehmi::dataacquisition::Exception", "classcutehmi_1_1dataacquisition_1_1_exception.html", null ]
        ] ]
      ] ]
    ] ],
    [ "QObject", "http://doc.qt.io/qt-5/qobject.html", [
      [ "QAbstractItemModel", "http://doc.qt.io/qt-5/qabstractitemmodel.html", [
        [ "QAbstractListModel", "http://doc.qt.io/qt-5/qabstractlistmodel.html", [
          [ "cutehmi::dataacquisition::AbstractListModel", "classcutehmi_1_1dataacquisition_1_1_abstract_list_model.html", null ]
        ] ]
      ] ],
      [ "QQmlEngineExtensionPlugin", "http://doc.qt.io/qt-5/qqmlengineextensionplugin.html", [
        [ "cutehmi::dataacquisition::internal::QMLPlugin", "classcutehmi_1_1dataacquisition_1_1internal_1_1_q_m_l_plugin.html", null ]
      ] ],
      [ "cutehmi::dataacquisition::AbstractWriter", "classcutehmi_1_1dataacquisition_1_1_abstract_writer.html", [
        [ "cutehmi::dataacquisition::EventWriter", "classcutehmi_1_1dataacquisition_1_1_event_writer.html", null ],
        [ "cutehmi::dataacquisition::HistoryWriter", "classcutehmi_1_1dataacquisition_1_1_history_writer.html", null ],
        [ "cutehmi::dataacquisition::RecencyWriter", "classcutehmi_1_1dataacquisition_1_1_recency_writer.html", null ]
      ] ],
      [ "cutehmi::dataacquisition::AbstractWriterAttachedType", "classcutehmi_1_1dataacquisition_1_1_abstract_writer_attached_type.html", null ],
      [ "cutehmi::dataacquisition::TagValue", "classcutehmi_1_1dataacquisition_1_1_tag_value.html", null ],
      [ "cutehmi::shareddatabase::DataObject", "../SharedDatabase.0/classcutehmi_1_1shareddatabase_1_1_data_object.html", [
        [ "cutehmi::dataacquisition::Schema", "classcutehmi_1_1dataacquisition_1_1_schema.html", null ],
        [ "cutehmi::dataacquisition::internal::TableObject", "classcutehmi_1_1dataacquisition_1_1internal_1_1_table_object.html", [
          [ "cutehmi::dataacquisition::internal::TableCollective", "classcutehmi_1_1dataacquisition_1_1internal_1_1_table_collective.html", [
            [ "cutehmi::dataacquisition::internal::EventCollective", "classcutehmi_1_1dataacquisition_1_1internal_1_1_event_collective.html", null ],
            [ "cutehmi::dataacquisition::internal::HistoryCollective", "classcutehmi_1_1dataacquisition_1_1internal_1_1_history_collective.html", null ],
            [ "cutehmi::dataacquisition::internal::RecencyCollective", "classcutehmi_1_1dataacquisition_1_1internal_1_1_recency_collective.html", null ]
          ] ],
          [ "cutehmi::dataacquisition::internal::TagCache", "classcutehmi_1_1dataacquisition_1_1internal_1_1_tag_cache.html", null ]
        ] ]
      ] ]
    ] ],
    [ "QtQml::QtObject", null, [
      [ "CuteHMI::DataAcquisition::Console", "class_cute_h_m_i_1_1_data_acquisition_1_1_console.html", null ]
    ] ],
    [ "cutehmi::services::Serviceable", "../Services.2/classcutehmi_1_1services_1_1_serviceable.html", [
      [ "cutehmi::dataacquisition::AbstractListModel", "classcutehmi_1_1dataacquisition_1_1_abstract_list_model.html", null ],
      [ "cutehmi::dataacquisition::AbstractWriter", "classcutehmi_1_1dataacquisition_1_1_abstract_writer.html", null ]
    ] ],
    [ "cutehmi::dataacquisition::internal::TableNameTraits< T >", "structcutehmi_1_1dataacquisition_1_1internal_1_1_table_name_traits.html", null ],
    [ "cutehmi::dataacquisition::internal::TableNameTraits< bool >", "structcutehmi_1_1dataacquisition_1_1internal_1_1_table_name_traits_3_01bool_01_4.html", null ],
    [ "cutehmi::dataacquisition::internal::TableNameTraits< double >", "structcutehmi_1_1dataacquisition_1_1internal_1_1_table_name_traits_3_01double_01_4.html", null ],
    [ "cutehmi::dataacquisition::internal::TableNameTraits< int >", "structcutehmi_1_1dataacquisition_1_1internal_1_1_table_name_traits_3_01int_01_4.html", null ],
    [ "cutehmi::dataacquisition::internal::EventCollective::Tuple", "structcutehmi_1_1dataacquisition_1_1internal_1_1_event_collective_1_1_tuple.html", null ],
    [ "cutehmi::dataacquisition::internal::HistoryCollective::Tuple", "structcutehmi_1_1dataacquisition_1_1internal_1_1_history_collective_1_1_tuple.html", null ],
    [ "cutehmi::dataacquisition::internal::RecencyCollective::Tuple", "structcutehmi_1_1dataacquisition_1_1internal_1_1_recency_collective_1_1_tuple.html", null ]
];