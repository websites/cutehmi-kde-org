var annotated_dup =
[
    [ "CuteHMI", "namespace_cute_h_m_i.html", [
      [ "DataAcquisition", "namespace_cute_h_m_i_1_1_data_acquisition.html", [
        [ "Console", "class_cute_h_m_i_1_1_data_acquisition_1_1_console.html", "class_cute_h_m_i_1_1_data_acquisition_1_1_console" ]
      ] ]
    ] ],
    [ "cutehmi", "namespacecutehmi.html", [
      [ "dataacquisition", "namespacecutehmi_1_1dataacquisition.html", [
        [ "internal", "namespacecutehmi_1_1dataacquisition_1_1internal.html", [
          [ "DbServiceableMixin", "classcutehmi_1_1dataacquisition_1_1internal_1_1_db_serviceable_mixin.html", "classcutehmi_1_1dataacquisition_1_1internal_1_1_db_serviceable_mixin" ],
          [ "EventCollective", "classcutehmi_1_1dataacquisition_1_1internal_1_1_event_collective.html", "classcutehmi_1_1dataacquisition_1_1internal_1_1_event_collective" ],
          [ "HistoryCollective", "classcutehmi_1_1dataacquisition_1_1internal_1_1_history_collective.html", "classcutehmi_1_1dataacquisition_1_1internal_1_1_history_collective" ],
          [ "ModelMixin", "classcutehmi_1_1dataacquisition_1_1internal_1_1_model_mixin.html", "classcutehmi_1_1dataacquisition_1_1internal_1_1_model_mixin" ],
          [ "QMLPlugin", "classcutehmi_1_1dataacquisition_1_1internal_1_1_q_m_l_plugin.html", null ],
          [ "RecencyCollective", "classcutehmi_1_1dataacquisition_1_1internal_1_1_recency_collective.html", "classcutehmi_1_1dataacquisition_1_1internal_1_1_recency_collective" ],
          [ "TableCollective", "classcutehmi_1_1dataacquisition_1_1internal_1_1_table_collective.html", "classcutehmi_1_1dataacquisition_1_1internal_1_1_table_collective" ],
          [ "TableNameTraits", "structcutehmi_1_1dataacquisition_1_1internal_1_1_table_name_traits.html", null ],
          [ "TableNameTraits< bool >", "structcutehmi_1_1dataacquisition_1_1internal_1_1_table_name_traits_3_01bool_01_4.html", null ],
          [ "TableNameTraits< double >", "structcutehmi_1_1dataacquisition_1_1internal_1_1_table_name_traits_3_01double_01_4.html", null ],
          [ "TableNameTraits< int >", "structcutehmi_1_1dataacquisition_1_1internal_1_1_table_name_traits_3_01int_01_4.html", null ],
          [ "TableObject", "classcutehmi_1_1dataacquisition_1_1internal_1_1_table_object.html", "classcutehmi_1_1dataacquisition_1_1internal_1_1_table_object" ],
          [ "TagCache", "classcutehmi_1_1dataacquisition_1_1internal_1_1_tag_cache.html", "classcutehmi_1_1dataacquisition_1_1internal_1_1_tag_cache" ]
        ] ],
        [ "AbstractListModel", "classcutehmi_1_1dataacquisition_1_1_abstract_list_model.html", "classcutehmi_1_1dataacquisition_1_1_abstract_list_model" ],
        [ "AbstractWriter", "classcutehmi_1_1dataacquisition_1_1_abstract_writer.html", "classcutehmi_1_1dataacquisition_1_1_abstract_writer" ],
        [ "AbstractWriterAttachedType", "classcutehmi_1_1dataacquisition_1_1_abstract_writer_attached_type.html", "classcutehmi_1_1dataacquisition_1_1_abstract_writer_attached_type" ],
        [ "EventModel", "classcutehmi_1_1dataacquisition_1_1_event_model.html", "classcutehmi_1_1dataacquisition_1_1_event_model" ],
        [ "EventWriter", "classcutehmi_1_1dataacquisition_1_1_event_writer.html", "classcutehmi_1_1dataacquisition_1_1_event_writer" ],
        [ "Exception", "classcutehmi_1_1dataacquisition_1_1_exception.html", null ],
        [ "HistoryModel", "classcutehmi_1_1dataacquisition_1_1_history_model.html", "classcutehmi_1_1dataacquisition_1_1_history_model" ],
        [ "HistoryWriter", "classcutehmi_1_1dataacquisition_1_1_history_writer.html", "classcutehmi_1_1dataacquisition_1_1_history_writer" ],
        [ "Init", "classcutehmi_1_1dataacquisition_1_1_init.html", "classcutehmi_1_1dataacquisition_1_1_init" ],
        [ "RecencyModel", "classcutehmi_1_1dataacquisition_1_1_recency_model.html", "classcutehmi_1_1dataacquisition_1_1_recency_model" ],
        [ "RecencyWriter", "classcutehmi_1_1dataacquisition_1_1_recency_writer.html", "classcutehmi_1_1dataacquisition_1_1_recency_writer" ],
        [ "Schema", "classcutehmi_1_1dataacquisition_1_1_schema.html", "classcutehmi_1_1dataacquisition_1_1_schema" ],
        [ "TagValue", "classcutehmi_1_1dataacquisition_1_1_tag_value.html", "classcutehmi_1_1dataacquisition_1_1_tag_value" ]
      ] ]
    ] ]
];