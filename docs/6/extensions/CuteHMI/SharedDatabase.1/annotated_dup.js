var annotated_dup =
[
    [ "CuteHMI", "namespace_cute_h_m_i.html", [
      [ "SharedDatabase", "namespace_cute_h_m_i_1_1_shared_database.html", [
        [ "Console", "class_cute_h_m_i_1_1_shared_database_1_1_console.html", "class_cute_h_m_i_1_1_shared_database_1_1_console" ],
        [ "Database", "class_cute_h_m_i_1_1_shared_database_1_1_database.html", null ],
        [ "DataObject", "class_cute_h_m_i_1_1_shared_database_1_1_data_object.html", null ],
        [ "PostgresMaintenance", "class_cute_h_m_i_1_1_shared_database_1_1_postgres_maintenance.html", null ]
      ] ]
    ] ],
    [ "cutehmi", "namespacecutehmi.html", [
      [ "shareddatabase", "namespacecutehmi_1_1shareddatabase.html", [
        [ "internal", "namespacecutehmi_1_1shareddatabase_1_1internal.html", [
          [ "DatabaseConfig", "classcutehmi_1_1shareddatabase_1_1internal_1_1_database_config.html", "classcutehmi_1_1shareddatabase_1_1internal_1_1_database_config" ],
          [ "DatabaseConnectionHandler", "classcutehmi_1_1shareddatabase_1_1internal_1_1_database_connection_handler.html", "classcutehmi_1_1shareddatabase_1_1internal_1_1_database_connection_handler" ],
          [ "DatabaseDictionary", "classcutehmi_1_1shareddatabase_1_1internal_1_1_database_dictionary.html", "classcutehmi_1_1shareddatabase_1_1internal_1_1_database_dictionary" ],
          [ "DatabaseThread", "classcutehmi_1_1shareddatabase_1_1internal_1_1_database_thread.html", "classcutehmi_1_1shareddatabase_1_1internal_1_1_database_thread" ],
          [ "QMLPlugin", "classcutehmi_1_1shareddatabase_1_1internal_1_1_q_m_l_plugin.html", null ]
        ] ],
        [ "Database", "classcutehmi_1_1shareddatabase_1_1_database.html", "classcutehmi_1_1shareddatabase_1_1_database" ],
        [ "DatabaseWorker", "classcutehmi_1_1shareddatabase_1_1_database_worker.html", "classcutehmi_1_1shareddatabase_1_1_database_worker" ],
        [ "DataObject", "classcutehmi_1_1shareddatabase_1_1_data_object.html", "classcutehmi_1_1shareddatabase_1_1_data_object" ],
        [ "Exception", "classcutehmi_1_1shareddatabase_1_1_exception.html", null ],
        [ "PostgresMaintenance", "classcutehmi_1_1shareddatabase_1_1_postgres_maintenance.html", "classcutehmi_1_1shareddatabase_1_1_postgres_maintenance" ]
      ] ]
    ] ]
];