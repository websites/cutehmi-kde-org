var annotated_dup =
[
    [ "cutehmi", "namespacecutehmi.html", [
      [ "services", "namespacecutehmi_1_1services.html", [
        [ "internal", "namespacecutehmi_1_1services_1_1internal.html", [
          [ "QMLPlugin", "classcutehmi_1_1services_1_1internal_1_1_q_m_l_plugin.html", null ],
          [ "StateInterface", "classcutehmi_1_1services_1_1internal_1_1_state_interface.html", "classcutehmi_1_1services_1_1internal_1_1_state_interface" ]
        ] ],
        [ "Init", "classcutehmi_1_1services_1_1_init.html", "classcutehmi_1_1services_1_1_init" ],
        [ "PollingTimer", "classcutehmi_1_1services_1_1_polling_timer.html", "classcutehmi_1_1services_1_1_polling_timer" ],
        [ "Service", "classcutehmi_1_1services_1_1_service.html", "classcutehmi_1_1services_1_1_service" ],
        [ "Serviceable", "classcutehmi_1_1services_1_1_serviceable.html", "classcutehmi_1_1services_1_1_serviceable" ],
        [ "ServiceListModel", "classcutehmi_1_1services_1_1_service_list_model.html", "classcutehmi_1_1services_1_1_service_list_model" ],
        [ "ServiceManager", "classcutehmi_1_1services_1_1_service_manager.html", "classcutehmi_1_1services_1_1_service_manager" ]
      ] ]
    ] ],
    [ "CuteHMI", "namespace_cute_h_m_i.html", [
      [ "Services", "namespace_cute_h_m_i_1_1_services.html", [
        [ "PollingTimer", "class_cute_h_m_i_1_1_services_1_1_polling_timer.html", null ],
        [ "Service", "class_cute_h_m_i_1_1_services_1_1_service.html", null ],
        [ "ServiceManager", "class_cute_h_m_i_1_1_services_1_1_service_manager.html", null ]
      ] ]
    ] ]
];