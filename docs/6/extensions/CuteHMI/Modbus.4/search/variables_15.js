var searchData=
[
  ['vai_0',['Vai',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['vaiscript_1',['VaiScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['validate_2',['Validate',['http://doc.qt.io/qt-5/qjsondocument.html#DataValidation-enum',1,'QJsonDocument']]],
  ['value_3',['Value',['http://doc.qt.io/qt-5/qaccessible.html#Text-enum',1,'QAccessible']]],
  ['value_5ftype_4',['value_type',['http://doc.qt.io/qt-5/qvarlengtharray.html#value_type-typedef',1,'QVarLengthArray::value_type()'],['http://doc.qt.io/qt-5/qvector.html#value_type-typedef',1,'QVector::value_type()'],['http://doc.qt.io/qt-5/qstringview.html#value_type-typedef',1,'QStringView::value_type()'],['http://doc.qt.io/qt-5/qstring.html#value_type-typedef',1,'QString::value_type()'],['http://doc.qt.io/qt-5/qset-iterator.html#value_type-typedef',1,'QSet::iterator::value_type()'],['http://doc.qt.io/qt-5/qset-const-iterator.html#value_type-typedef',1,'QSet::const_iterator::value_type()'],['http://doc.qt.io/qt-5/qset.html#value_type-typedef',1,'QSet::value_type()'],['http://doc.qt.io/qt-5/qlinkedlist.html#value_type-typedef',1,'QLinkedList::value_type()'],['http://doc.qt.io/qt-5/qlatin1string.html#value_type-alias',1,'QLatin1String::value_type()'],['http://doc.qt.io/qt-5/qjsonarray.html#value_type-typedef',1,'QJsonArray::value_type()'],['http://doc.qt.io/qt-5/qfuture-const-iterator.html#value_type-typedef',1,'QFuture::const_iterator::value_type()'],['http://doc.qt.io/qt-5/qcbormap.html#value_type-typedef',1,'QCborMap::value_type()'],['http://doc.qt.io/qt-5/qcborarray.html#value_type-typedef',1,'QCborArray::value_type()'],['http://doc.qt.io/qt-5/qlist.html#value_type-typedef',1,'QList::value_type()']]],
  ['valuechanged_5',['ValueChanged',['http://doc.qt.io/qt-5/qaccessible.html#Event-enum',1,'QAccessible']]],
  ['valueinterface_6',['ValueInterface',['http://doc.qt.io/qt-5/qaccessible.html#InterfaceType-enum',1,'QAccessible']]],
  ['vanuatu_7',['Vanuatu',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['varangkshitiscript_8',['VarangKshitiScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['variablelength_9',['VariableLength',['http://doc.qt.io/qt-5/qtextlength.html#Type-enum',1,'QTextLength']]],
  ['varunknown_10',['VarUnknown',['http://doc.qt.io/qt-5/quuid.html#Variant-enum',1,'QUuid']]],
  ['vaticancitystate_11',['VaticanCityState',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['vector2d_12',['Vector2D',['http://doc.qt.io/qt-5/qvariant-obsolete.html#Type-enum',1,'QVariant']]],
  ['vector3d_13',['Vector3D',['http://doc.qt.io/qt-5/qvariant-obsolete.html#Type-enum',1,'QVariant']]],
  ['vector4d_14',['Vector4D',['http://doc.qt.io/qt-5/qvariant-obsolete.html#Type-enum',1,'QVariant']]],
  ['velocity_15',['Velocity',['http://doc.qt.io/qt-5/qtouchdevice.html#CapabilityFlag-enum',1,'QTouchDevice']]],
  ['venda_16',['Venda',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['vendornameobjectid_17',['VendorNameObjectId',['http://doc.qt.io/qt-5/qmodbusdeviceidentification.html#ObjectId-enum',1,'QModbusDeviceIdentification']]],
  ['vendorurlobjectid_18',['VendorUrlObjectId',['http://doc.qt.io/qt-5/qmodbusdeviceidentification.html#ObjectId-enum',1,'QModbusDeviceIdentification']]],
  ['venezuela_19',['Venezuela',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['verifynone_20',['VerifyNone',['http://doc.qt.io/qt-5/qsslsocket.html#PeerVerifyMode-enum',1,'QSslSocket']]],
  ['verifypeer_21',['VerifyPeer',['http://doc.qt.io/qt-5/qsslsocket.html#PeerVerifyMode-enum',1,'QSslSocket']]],
  ['verpattern_22',['VerPattern',['http://doc.qt.io/qt-5/qt.html#BrushStyle-enum',1,'Qt']]],
  ['vertex_23',['Vertex',['http://doc.qt.io/qt-5/qopenglshader.html#ShaderTypeBit-enum',1,'QOpenGLShader']]],
  ['vertexbuffer_24',['VertexBuffer',['http://doc.qt.io/qt-5/qopenglbuffer.html#Type-enum',1,'QOpenGLBuffer']]],
  ['vertical_25',['Vertical',['http://doc.qt.io/qt-5/qchar.html#Decomposition-enum',1,'QChar::Vertical()'],['http://doc.qt.io/qt-5/qt.html#Orientation-enum',1,'Qt::Vertical()']]],
  ['verticalsorthint_26',['VerticalSortHint',['http://doc.qt.io/qt-5/qabstractitemmodel.html#LayoutChangeHint-enum',1,'QAbstractItemModel']]],
  ['verunknown_27',['VerUnknown',['http://doc.qt.io/qt-5/quuid.html#Version-enum',1,'QUuid']]],
  ['verycoarsetimer_28',['VeryCoarseTimer',['http://doc.qt.io/qt-5/qt.html#TimerType-enum',1,'Qt']]],
  ['viciousstance_29',['ViciousStance',['http://doc.qt.io/qt-5/qgradient.html#Preset-enum',1,'QGradient']]],
  ['vietnam_30',['Vietnam',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['vietnamese_31',['Vietnamese',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale::Vietnamese()'],['http://doc.qt.io/qt-5/qfontdatabase.html#WritingSystem-enum',1,'QFontDatabase::Vietnamese()']]],
  ['viewportstate_32',['ViewportState',['http://doc.qt.io/qt-5/qsgrendernode.html#StateFlag-enum',1,'QSGRenderNode']]],
  ['virtual_33',['Virtual',['http://doc.qt.io/qt-5/qnetworkinterface.html#InterfaceType-enum',1,'QNetworkInterface']]],
  ['visibledatachanged_34',['VisibleDataChanged',['http://doc.qt.io/qt-5/qaccessible.html#Event-enum',1,'QAccessible']]],
  ['visualmovestyle_35',['VisualMoveStyle',['http://doc.qt.io/qt-5/qt.html#CursorMoveStyle-enum',1,'Qt']]],
  ['void_36',['Void',['http://doc.qt.io/qt-5/qmetatype.html#Type-enum',1,'QMetaType']]],
  ['voidstar_37',['VoidStar',['http://doc.qt.io/qt-5/qmetatype.html#Type-enum',1,'QMetaType']]],
  ['volapuk_38',['Volapuk',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['vulkaninstanceresource_39',['VulkanInstanceResource',['http://doc.qt.io/qt-5/qsgrendererinterface.html#Resource-enum',1,'QSGRendererInterface']]],
  ['vulkanrhi_40',['VulkanRhi',['http://doc.qt.io/qt-5/qsgrendererinterface.html#GraphicsApi-enum',1,'QSGRendererInterface']]],
  ['vulkansurface_41',['VulkanSurface',['http://doc.qt.io/qt-5/qsurface.html#SurfaceType-enum',1,'QSurface']]],
  ['vunjo_42',['Vunjo',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]]
];
