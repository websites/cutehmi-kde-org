var annotated_dup =
[
    [ "CuteHMI", "namespace_cute_h_m_i.html", [
      [ "GUI", "namespace_cute_h_m_i_1_1_g_u_i.html", [
        [ "ColorSet", "class_cute_h_m_i_1_1_g_u_i_1_1_color_set.html", null ],
        [ "CuteApplication", "class_cute_h_m_i_1_1_g_u_i_1_1_cute_application.html", "class_cute_h_m_i_1_1_g_u_i_1_1_cute_application" ],
        [ "Element", "class_cute_h_m_i_1_1_g_u_i_1_1_element.html", "class_cute_h_m_i_1_1_g_u_i_1_1_element" ],
        [ "Fonts", "class_cute_h_m_i_1_1_g_u_i_1_1_fonts.html", null ],
        [ "NumberDisplay", "class_cute_h_m_i_1_1_g_u_i_1_1_number_display.html", "class_cute_h_m_i_1_1_g_u_i_1_1_number_display" ],
        [ "Palette", "class_cute_h_m_i_1_1_g_u_i_1_1_palette.html", null ],
        [ "PropItem", "class_cute_h_m_i_1_1_g_u_i_1_1_prop_item.html", "class_cute_h_m_i_1_1_g_u_i_1_1_prop_item" ],
        [ "Theme", "class_cute_h_m_i_1_1_g_u_i_1_1_theme.html", null ],
        [ "Units", "class_cute_h_m_i_1_1_g_u_i_1_1_units.html", null ]
      ] ]
    ] ],
    [ "cutehmi", "namespacecutehmi.html", [
      [ "gui", "namespacecutehmi_1_1gui.html", [
        [ "internal", "namespacecutehmi_1_1gui_1_1internal.html", [
          [ "QMLPlugin", "classcutehmi_1_1gui_1_1internal_1_1_q_m_l_plugin.html", null ]
        ] ],
        [ "ColorSet", "classcutehmi_1_1gui_1_1_color_set.html", "classcutehmi_1_1gui_1_1_color_set" ],
        [ "CuteApplication", "classcutehmi_1_1gui_1_1_cute_application.html", "classcutehmi_1_1gui_1_1_cute_application" ],
        [ "Fonts", "classcutehmi_1_1gui_1_1_fonts.html", "classcutehmi_1_1gui_1_1_fonts" ],
        [ "Palette", "classcutehmi_1_1gui_1_1_palette.html", "classcutehmi_1_1gui_1_1_palette" ],
        [ "Theme", "classcutehmi_1_1gui_1_1_theme.html", "classcutehmi_1_1gui_1_1_theme" ],
        [ "Units", "classcutehmi_1_1gui_1_1_units.html", "classcutehmi_1_1gui_1_1_units" ]
      ] ]
    ] ]
];