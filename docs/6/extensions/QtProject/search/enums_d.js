var searchData=
[
  ['nametype_0',['NameType',['http://doc.qt.io/qt-5/qtimezone.html#NameType-enum',1,'QTimeZone']]],
  ['nativegesturetype_1',['NativeGestureType',['http://doc.qt.io/qt-5/qt.html#NativeGestureType-enum',1,'Qt']]],
  ['navigationmode_2',['NavigationMode',['http://doc.qt.io/qt-5/qt.html#NavigationMode-enum',1,'Qt']]],
  ['normalizationform_3',['NormalizationForm',['http://doc.qt.io/qt-5/qstring.html#NormalizationForm-enum',1,'QString']]],
  ['numberflag_4',['NumberFlag',['http://doc.qt.io/qt-5/qtextstream.html#NumberFlag-enum',1,'QTextStream']]],
  ['numberoption_5',['NumberOption',['http://doc.qt.io/qt-5/qlocale.html#NumberOption-enum',1,'QLocale']]]
];
