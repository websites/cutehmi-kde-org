var searchData=
[
  ['readelementtextbehaviour_0',['ReadElementTextBehaviour',['http://doc.qt.io/qt-5/qxmlstreamreader.html#ReadElementTextBehaviour-enum',1,'QXmlStreamReader']]],
  ['realnumbernotation_1',['RealNumberNotation',['http://doc.qt.io/qt-5/qtextstream.html#RealNumberNotation-enum',1,'QTextStream']]],
  ['recursionmode_2',['RecursionMode',['http://doc.qt.io/qt-5/qmutex.html#RecursionMode-enum',1,'QMutex::RecursionMode()'],['http://doc.qt.io/qt-5/qreadwritelock.html#RecursionMode-enum',1,'QReadWriteLock::RecursionMode()']]],
  ['restorepolicy_3',['RestorePolicy',['http://doc.qt.io/qt-5/qstate.html#RestorePolicy-enum',1,'QState']]],
  ['returnbyvalueconstant_4',['ReturnByValueConstant',['http://doc.qt.io/qt-5/qt.html#ReturnByValueConstant-enum',1,'Qt']]]
];
