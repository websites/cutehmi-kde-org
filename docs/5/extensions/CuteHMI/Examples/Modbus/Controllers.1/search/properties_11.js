var searchData=
[
  ['scale_25057',['scale',['http://doc.qt.io/qt-5/qquickitem.html#scale-prop',1,'QQuickItem::scale()'],['http://doc.qt.io/qt-5/qgraphicsobject.html#scale-prop',1,'QGraphicsObject::scale()']]],
  ['scaledcontents_25058',['scaledContents',['http://doc.qt.io/qt-5/qlabel.html#scaledContents-prop',1,'QLabel']]],
  ['scalefactor_25059',['scaleFactor',['http://doc.qt.io/qt-5/qpinchgesture.html#scaleFactor-prop',1,'QPinchGesture']]],
  ['scenerect_25060',['sceneRect',['http://doc.qt.io/qt-5/qgraphicsscene.html#sceneRect-prop',1,'QGraphicsScene::sceneRect()'],['http://doc.qt.io/qt-5/qgraphicsview.html#sceneRect-prop',1,'QGraphicsView::sceneRect()']]],
  ['screencount_25061',['screenCount',['http://doc.qt.io/qt-5/qdesktopwidget-obsolete.html#screenCount-prop',1,'QDesktopWidget']]],
  ['scrollerproperties_25062',['scrollerProperties',['http://doc.qt.io/qt-5/qscroller.html#scrollerProperties-prop',1,'QScroller']]],
  ['searchpaths_25063',['searchPaths',['http://doc.qt.io/qt-5/qtextbrowser.html#searchPaths-prop',1,'QTextBrowser']]],
  ['sectioncount_25064',['sectionCount',['http://doc.qt.io/qt-5/qdatetimeedit.html#sectionCount-prop',1,'QDateTimeEdit']]],
  ['segmentstyle_25065',['segmentStyle',['http://doc.qt.io/qt-5/qlcdnumber.html#segmentStyle-prop',1,'QLCDNumber']]],
  ['selecteddate_25066',['selectedDate',['http://doc.qt.io/qt-5/qcalendarwidget.html#selectedDate-prop',1,'QCalendarWidget']]],
  ['selectedindexes_25067',['selectedIndexes',['http://doc.qt.io/qt-5/qitemselectionmodel.html#selectedIndexes-prop',1,'QItemSelectionModel']]],
  ['selectedtext_25068',['selectedText',['http://doc.qt.io/qt-5/qlineedit.html#selectedText-prop',1,'QLineEdit::selectedText()'],['http://doc.qt.io/qt-5/qlabel.html#selectedText-prop',1,'QLabel::selectedText()']]],
  ['selectionbehavior_25069',['selectionBehavior',['http://doc.qt.io/qt-5/qabstractitemview.html#selectionBehavior-prop',1,'QAbstractItemView']]],
  ['selectionbehavioronremove_25070',['selectionBehaviorOnRemove',['http://doc.qt.io/qt-5/qtabbar.html#selectionBehaviorOnRemove-prop',1,'QTabBar']]],
  ['selectionmode_25071',['selectionMode',['http://doc.qt.io/qt-5/qabstractitemview.html#selectionMode-prop',1,'QAbstractItemView::selectionMode()'],['http://doc.qt.io/qt-5/qcalendarwidget.html#selectionMode-prop',1,'QCalendarWidget::selectionMode()']]],
  ['selectionrectvisible_25072',['selectionRectVisible',['http://doc.qt.io/qt-5/qlistview.html#selectionRectVisible-prop',1,'QListView']]],
  ['senderobject_25073',['senderObject',['http://doc.qt.io/qt-5/qsignaltransition.html#senderObject-prop',1,'QSignalTransition']]],
  ['separatorscollapsible_25074',['separatorsCollapsible',['http://doc.qt.io/qt-5/qmenu.html#separatorsCollapsible-prop',1,'QMenu']]],
  ['serialnumber_25075',['serialNumber',['http://doc.qt.io/qt-5/qscreen.html#serialNumber-prop',1,'QScreen']]],
  ['service_25076',['service',['../class_cute_h_m_i_1_1_examples_1_1_modbus_1_1_controllers_1_1_service_control.html#a8c20c1320fbb209bbaa3c1a9cb65b1ca',1,'CuteHMI::Examples::Modbus::Controllers::ServiceControl']]],
  ['serviceable_25077',['serviceable',['../../../../Services.2/classcutehmi_1_1services_1_1_service.html#ad9acc74d3b815af767167553a05d8bee',1,'cutehmi::services::Service']]],
  ['setfocusontouchrelease_25078',['setFocusOnTouchRelease',['http://doc.qt.io/qt-5/qstylehints.html#setFocusOnTouchRelease-prop',1,'QStyleHints']]],
  ['shade_25079',['shade',['../../../../GUI.1/classcutehmi_1_1gui_1_1_color_set.html#a6b8ce608b2c6dad626cae4ff5bd68859',1,'cutehmi::gui::ColorSet']]],
  ['shape_25080',['shape',['http://doc.qt.io/qt-5/qtabbar.html#shape-prop',1,'QTabBar']]],
  ['shortcut_25081',['shortcut',['http://doc.qt.io/qt-5/qabstractbutton.html#shortcut-prop',1,'QAbstractButton::shortcut()'],['http://doc.qt.io/qt-5/qaction.html#shortcut-prop',1,'QAction::shortcut()']]],
  ['shortcutcontext_25082',['shortcutContext',['http://doc.qt.io/qt-5/qaction.html#shortcutContext-prop',1,'QAction']]],
  ['shortcutvisibleincontextmenu_25083',['shortcutVisibleInContextMenu',['http://doc.qt.io/qt-5/qaction.html#shortcutVisibleInContextMenu-prop',1,'QAction']]],
  ['showdropindicator_25084',['showDropIndicator',['http://doc.qt.io/qt-5/qabstractitemview.html#showDropIndicator-prop',1,'QAbstractItemView']]],
  ['showgrid_25085',['showGrid',['http://doc.qt.io/qt-5/qtableview.html#showGrid-prop',1,'QTableView']]],
  ['showgroupseparator_25086',['showGroupSeparator',['http://doc.qt.io/qt-5/qabstractspinbox.html#showGroupSeparator-prop',1,'QAbstractSpinBox']]],
  ['showisfullscreen_25087',['showIsFullScreen',['http://doc.qt.io/qt-5/qstylehints.html#showIsFullScreen-prop',1,'QStyleHints']]],
  ['showismaximized_25088',['showIsMaximized',['http://doc.qt.io/qt-5/qstylehints.html#showIsMaximized-prop',1,'QStyleHints']]],
  ['showshortcutsincontextmenus_25089',['showShortcutsInContextMenus',['http://doc.qt.io/qt-5/qstylehints.html#showShortcutsInContextMenus-prop',1,'QStyleHints']]],
  ['showsortindicator_25090',['showSortIndicator',['http://doc.qt.io/qt-5/qheaderview.html#showSortIndicator-prop',1,'QHeaderView']]],
  ['signal_25091',['signal',['http://doc.qt.io/qt-5/qsignaltransition.html#signal-prop',1,'QSignalTransition']]],
  ['singleclickactivation_25092',['singleClickActivation',['http://doc.qt.io/qt-5/qstylehints.html#singleClickActivation-prop',1,'QStyleHints']]],
  ['singleshot_25093',['singleShot',['http://doc.qt.io/qt-5/qtimer.html#singleShot-prop',1,'QTimer']]],
  ['singlestep_25094',['singleStep',['http://doc.qt.io/qt-5/qabstractslider.html#singleStep-prop',1,'QAbstractSlider::singleStep()'],['http://doc.qt.io/qt-5/qspinbox.html#singleStep-prop',1,'QSpinBox::singleStep()'],['http://doc.qt.io/qt-5/qdoublespinbox.html#singleStep-prop',1,'QDoubleSpinBox::singleStep()']]],
  ['size_25095',['size',['http://doc.qt.io/qt-5/qtextdocument.html#size-prop',1,'QTextDocument::size()'],['http://doc.qt.io/qt-5/qscreen.html#size-prop',1,'QScreen::size()'],['http://doc.qt.io/qt-5/qwidget.html#size-prop',1,'QWidget::size()'],['http://doc.qt.io/qt-5/qgraphicswidget.html#size-prop',1,'QGraphicsWidget::size()']]],
  ['sizeadjustpolicy_25096',['sizeAdjustPolicy',['http://doc.qt.io/qt-5/qabstractscrollarea.html#sizeAdjustPolicy-prop',1,'QAbstractScrollArea::sizeAdjustPolicy()'],['http://doc.qt.io/qt-5/qcombobox.html#sizeAdjustPolicy-prop',1,'QComboBox::sizeAdjustPolicy()']]],
  ['sizeconstraint_25097',['sizeConstraint',['http://doc.qt.io/qt-5/qlayout.html#sizeConstraint-prop',1,'QLayout']]],
  ['sizegripenabled_25098',['sizeGripEnabled',['http://doc.qt.io/qt-5/qdialog.html#sizeGripEnabled-prop',1,'QDialog::sizeGripEnabled()'],['http://doc.qt.io/qt-5/qstatusbar.html#sizeGripEnabled-prop',1,'QStatusBar::sizeGripEnabled()']]],
  ['sizehint_25099',['sizeHint',['http://doc.qt.io/qt-5/qwidget.html#sizeHint-prop',1,'QWidget']]],
  ['sizeincrement_25100',['sizeIncrement',['http://doc.qt.io/qt-5/qwidget.html#sizeIncrement-prop',1,'QWidget']]],
  ['sizepolicy_25101',['sizePolicy',['http://doc.qt.io/qt-5/qwidget.html#sizePolicy-prop',1,'QWidget::sizePolicy()'],['http://doc.qt.io/qt-5/qgraphicsanchor.html#sizePolicy-prop',1,'QGraphicsAnchor::sizePolicy()'],['http://doc.qt.io/qt-5/qgraphicswidget.html#sizePolicy-prop',1,'QGraphicsWidget::sizePolicy()']]],
  ['slaveaddress_25102',['slaveAddress',['../../../../Modbus.2/classcutehmi_1_1modbus_1_1_r_t_u_client.html#aaca6731cbb1effa0f0782a6b7b7d1edd',1,'cutehmi::modbus::RTUClient::slaveAddress()'],['../../../../Modbus.2/classcutehmi_1_1modbus_1_1_r_t_u_server.html#a8a618be1709f48204cde0ad58e2b0fe7',1,'cutehmi::modbus::RTUServer::slaveAddress()'],['../../../../Modbus.2/classcutehmi_1_1modbus_1_1_t_c_p_client.html#a5b4005c8fbff614f855b7f3fb71f0c66',1,'cutehmi::modbus::TCPClient::slaveAddress()'],['../../../../Modbus.2/classcutehmi_1_1modbus_1_1_t_c_p_server.html#a0ccdc71e22495a5cda35d975a1b65521',1,'cutehmi::modbus::TCPServer::slaveAddress()']]],
  ['sliderdown_25103',['sliderDown',['http://doc.qt.io/qt-5/qabstractslider.html#sliderDown-prop',1,'QAbstractSlider']]],
  ['sliderposition_25104',['sliderPosition',['http://doc.qt.io/qt-5/qabstractslider.html#sliderPosition-prop',1,'QAbstractSlider']]],
  ['smalldecimalpoint_25105',['smallDecimalPoint',['http://doc.qt.io/qt-5/qlcdnumber.html#smallDecimalPoint-prop',1,'QLCDNumber']]],
  ['smooth_25106',['smooth',['http://doc.qt.io/qt-5/qquickitem.html#smooth-prop',1,'QQuickItem']]],
  ['socketoptions_25107',['socketOptions',['http://doc.qt.io/qt-5/qlocalserver.html#socketOptions-prop',1,'QLocalServer']]],
  ['sortcacheenabled_25108',['sortCacheEnabled',['http://doc.qt.io/qt-5/qgraphicsscene-obsolete.html#sortCacheEnabled-prop',1,'QGraphicsScene']]],
  ['sortcasesensitivity_25109',['sortCaseSensitivity',['http://doc.qt.io/qt-5/qsortfilterproxymodel.html#sortCaseSensitivity-prop',1,'QSortFilterProxyModel']]],
  ['sortingenabled_25110',['sortingEnabled',['http://doc.qt.io/qt-5/qlistwidget.html#sortingEnabled-prop',1,'QListWidget::sortingEnabled()'],['http://doc.qt.io/qt-5/qtableview.html#sortingEnabled-prop',1,'QTableView::sortingEnabled()'],['http://doc.qt.io/qt-5/qtreeview.html#sortingEnabled-prop',1,'QTreeView::sortingEnabled()']]],
  ['sortrole_25111',['sortRole',['http://doc.qt.io/qt-5/qsortfilterproxymodel.html#sortRole-prop',1,'QSortFilterProxyModel::sortRole()'],['http://doc.qt.io/qt-5/qstandarditemmodel.html#sortRole-prop',1,'QStandardItemModel::sortRole()']]],
  ['source_25112',['source',['http://doc.qt.io/qt-5/qquickview.html#source-prop',1,'QQuickView::source()'],['http://doc.qt.io/qt-5/qquickwidget.html#source-prop',1,'QQuickWidget::source()'],['http://doc.qt.io/qt-5/qtextbrowser.html#source-prop',1,'QTextBrowser::source()'],['../class_cute_h_m_i_1_1_examples_1_1_modbus_1_1_controllers_1_1_device_loader.html#a9d109df828fc56142f86dbe1ef379e87',1,'CuteHMI::Examples::Modbus::Controllers::DeviceLoader::source()']]],
  ['sourcemodel_25113',['sourceModel',['http://doc.qt.io/qt-5/qabstractproxymodel.html#sourceModel-prop',1,'QAbstractProxyModel']]],
  ['sourcestate_25114',['sourceState',['http://doc.qt.io/qt-5/qabstracttransition.html#sourceState-prop',1,'QAbstractTransition']]],
  ['spacing_25115',['spacing',['http://doc.qt.io/qt-5/qlayout.html#spacing-prop',1,'QLayout::spacing()'],['http://doc.qt.io/qt-5/qgraphicsanchor.html#spacing-prop',1,'QGraphicsAnchor::spacing()'],['http://doc.qt.io/qt-5/qlistview.html#spacing-prop',1,'QListView::spacing()']]],
  ['specialvaluetext_25116',['specialValueText',['http://doc.qt.io/qt-5/qabstractspinbox.html#specialValueText-prop',1,'QAbstractSpinBox']]],
  ['speed_25117',['speed',['http://doc.qt.io/qt-5/qmovie.html#speed-prop',1,'QMovie']]],
  ['stackingmode_25118',['stackingMode',['http://doc.qt.io/qt-5/qstackedlayout.html#stackingMode-prop',1,'QStackedLayout']]],
  ['stacksize_25119',['stackSize',['http://doc.qt.io/qt-5/qthreadpool.html#stackSize-prop',1,'QThreadPool']]],
  ['standard_25120',['standard',['../../../../GUI.1/classcutehmi_1_1gui_1_1_fonts.html#ae28b1926345be08e1f0794179e811fa8',1,'cutehmi::gui::Fonts']]],
  ['standardbuttons_25121',['standardButtons',['http://doc.qt.io/qt-5/qdialogbuttonbox.html#standardButtons-prop',1,'QDialogButtonBox::standardButtons()'],['http://doc.qt.io/qt-5/qmessagebox.html#standardButtons-prop',1,'QMessageBox::standardButtons()']]],
  ['startcenterpoint_25122',['startCenterPoint',['http://doc.qt.io/qt-5/qpinchgesture.html#startCenterPoint-prop',1,'QPinchGesture']]],
  ['startdragdistance_25123',['startDragDistance',['http://doc.qt.io/qt-5/qstylehints.html#startDragDistance-prop',1,'QStyleHints::startDragDistance()'],['http://doc.qt.io/qt-5/qapplication.html#startDragDistance-prop',1,'QApplication::startDragDistance()']]],
  ['startdragtime_25124',['startDragTime',['http://doc.qt.io/qt-5/qstylehints.html#startDragTime-prop',1,'QStyleHints::startDragTime()'],['http://doc.qt.io/qt-5/qapplication.html#startDragTime-prop',1,'QApplication::startDragTime()']]],
  ['startdragvelocity_25125',['startDragVelocity',['http://doc.qt.io/qt-5/qstylehints.html#startDragVelocity-prop',1,'QStyleHints']]],
  ['startid_25126',['startId',['http://doc.qt.io/qt-5/qwizard.html#startId-prop',1,'QWizard']]],
  ['starttimeout_25127',['startTimeout',['../../../../Services.2/classcutehmi_1_1services_1_1_service.html#a1c2eaf45b84b65b83929d7e788f77313',1,'cutehmi::services::Service']]],
  ['startvalue_25128',['startValue',['http://doc.qt.io/qt-5/qvariantanimation.html#startValue-prop',1,'QVariantAnimation']]],
  ['state_25129',['state',['http://doc.qt.io/qt-5/qabstractanimation.html#state-prop',1,'QAbstractAnimation::state()'],['http://doc.qt.io/qt-5/qquickitem.html#state-prop',1,'QQuickItem::state()'],['http://doc.qt.io/qt-5/qgesture.html#state-prop',1,'QGesture::state()'],['http://doc.qt.io/qt-5/qscroller.html#state-prop',1,'QScroller::state()'],['../../../../Modbus.2/classcutehmi_1_1modbus_1_1_abstract_device.html#a3a26b4494bbdcc7285b3207ebe1c3c8e',1,'cutehmi::modbus::AbstractDevice::state()']]],
  ['status_25130',['status',['http://doc.qt.io/qt-5/qqmlcomponent.html#status-prop',1,'QQmlComponent::status()'],['http://doc.qt.io/qt-5/qquickview.html#status-prop',1,'QQuickView::status()'],['http://doc.qt.io/qt-5/qquickwidget.html#status-prop',1,'QQuickWidget::status()'],['../../../../Services.2/classcutehmi_1_1services_1_1_service.html#abec6b729e69f9a2ac32df123563f12b7',1,'cutehmi::services::Service::status()'],['../../../../Services.2/classcutehmi_1_1services_1_1internal_1_1_state_interface.html#ae24d3f6f77b8207619a72360ac8fbd03',1,'cutehmi::services::internal::StateInterface::status()']]],
  ['statustip_25131',['statusTip',['http://doc.qt.io/qt-5/qwidget.html#statusTip-prop',1,'QWidget::statusTip()'],['http://doc.qt.io/qt-5/qaction.html#statusTip-prop',1,'QAction::statusTip()']]],
  ['steptype_25132',['stepType',['http://doc.qt.io/qt-5/qspinbox.html#stepType-prop',1,'QSpinBox::stepType()'],['http://doc.qt.io/qt-5/qdoublespinbox.html#stepType-prop',1,'QDoubleSpinBox::stepType()']]],
  ['stickyfocus_25133',['stickyFocus',['http://doc.qt.io/qt-5/qgraphicsscene.html#stickyFocus-prop',1,'QGraphicsScene']]],
  ['stopbits_25134',['stopBits',['../../../../Modbus.2/classcutehmi_1_1modbus_1_1_r_t_u_client.html#ae59f197b417271cd15cf30785beea928',1,'cutehmi::modbus::RTUClient::stopBits()'],['../../../../Modbus.2/classcutehmi_1_1modbus_1_1_r_t_u_server.html#ad96a9b7717012b4ba59dd189c6264940',1,'cutehmi::modbus::RTUServer::stopBits()']]],
  ['stoptimeout_25135',['stopTimeout',['../../../../Services.2/classcutehmi_1_1services_1_1_service.html#a3f2aebb081cad3cc934e6ea6b1c59ca4',1,'cutehmi::services::Service']]],
  ['strength_25136',['strength',['http://doc.qt.io/qt-5/qgraphicscolorizeeffect.html#strength-prop',1,'QGraphicsColorizeEffect']]],
  ['stretchlastsection_25137',['stretchLastSection',['http://doc.qt.io/qt-5/qheaderview.html#stretchLastSection-prop',1,'QHeaderView']]],
  ['stroke_25138',['stroke',['../../../../GUI.1/classcutehmi_1_1gui_1_1_color_set.html#ae3343875beaab2d218ea01880184b127',1,'cutehmi::gui::ColorSet']]],
  ['strokewidth_25139',['strokeWidth',['../../../../GUI.1/classcutehmi_1_1gui_1_1_units.html#a91dc062f0ace8f529237e31b21432f3c',1,'cutehmi::gui::Units']]],
  ['strokewidthratio_25140',['strokeWidthRatio',['../../../../GUI.1/classcutehmi_1_1gui_1_1_units.html#a9d242c7cdf7c5a598c763de6dfc1a598',1,'cutehmi::gui::Units']]],
  ['stylesheet_25141',['styleSheet',['http://doc.qt.io/qt-5/qwidget.html#styleSheet-prop',1,'QWidget::styleSheet()'],['http://doc.qt.io/qt-5/qapplication.html#styleSheet-prop',1,'QApplication::styleSheet()']]],
  ['submitpolicy_25142',['submitPolicy',['http://doc.qt.io/qt-5/qdatawidgetmapper.html#submitPolicy-prop',1,'QDataWidgetMapper']]],
  ['subtimer_25143',['subtimer',['../../../../Services.2/classcutehmi_1_1services_1_1_polling_timer.html#a785f626e0156a7a409d626222fd0927c',1,'cutehmi::services::PollingTimer']]],
  ['subtitle_25144',['subTitle',['http://doc.qt.io/qt-5/qwizardpage.html#subTitle-prop',1,'QWizardPage']]],
  ['subtitleformat_25145',['subTitleFormat',['http://doc.qt.io/qt-5/qwizard.html#subTitleFormat-prop',1,'QWizard']]],
  ['suffix_25146',['suffix',['http://doc.qt.io/qt-5/qspinbox.html#suffix-prop',1,'QSpinBox::suffix()'],['http://doc.qt.io/qt-5/qdoublespinbox.html#suffix-prop',1,'QDoubleSpinBox::suffix()']]],
  ['suffixes_25147',['suffixes',['http://doc.qt.io/qt-5/qmimetype.html#suffixes-prop',1,'QMimeType']]],
  ['supportedschemes_25148',['supportedSchemes',['http://doc.qt.io/qt-5/qfiledialog.html#supportedSchemes-prop',1,'QFileDialog']]],
  ['swipeangle_25149',['swipeAngle',['http://doc.qt.io/qt-5/qswipegesture.html#swipeAngle-prop',1,'QSwipeGesture']]]
];
