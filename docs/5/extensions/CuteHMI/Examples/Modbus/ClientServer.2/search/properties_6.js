var searchData=
[
  ['genericiconname_24836',['genericIconName',['http://doc.qt.io/qt-5/qmimetype.html#genericIconName-prop',1,'QMimeType']]],
  ['geometry_24837',['geometry',['http://doc.qt.io/qt-5/qscreen.html#geometry-prop',1,'QScreen::geometry()'],['http://doc.qt.io/qt-5/qwidget.html#geometry-prop',1,'QWidget::geometry()'],['http://doc.qt.io/qt-5/qgraphicswidget.html#geometry-prop',1,'QGraphicsWidget::geometry()']]],
  ['gesturecancelpolicy_24838',['gestureCancelPolicy',['http://doc.qt.io/qt-5/qgesture.html#gestureCancelPolicy-prop',1,'QGesture']]],
  ['gesturetype_24839',['gestureType',['http://doc.qt.io/qt-5/qgesture.html#gestureType-prop',1,'QGesture']]],
  ['globalrestorepolicy_24840',['globalRestorePolicy',['http://doc.qt.io/qt-5/qstatemachine.html#globalRestorePolicy-prop',1,'QStateMachine']]],
  ['globalstrut_24841',['globalStrut',['http://doc.qt.io/qt-5/qapplication.html#globalStrut-prop',1,'QApplication']]],
  ['globpatterns_24842',['globPatterns',['http://doc.qt.io/qt-5/qmimetype.html#globPatterns-prop',1,'QMimeType']]],
  ['gridsize_24843',['gridSize',['http://doc.qt.io/qt-5/qlistview.html#gridSize-prop',1,'QListView']]],
  ['gridstyle_24844',['gridStyle',['http://doc.qt.io/qt-5/qtableview.html#gridStyle-prop',1,'QTableView']]],
  ['gridvisible_24845',['gridVisible',['http://doc.qt.io/qt-5/qcalendarwidget.html#gridVisible-prop',1,'QCalendarWidget']]]
];
