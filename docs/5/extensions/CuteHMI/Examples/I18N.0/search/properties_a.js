var searchData=
[
  ['labelalignment_23579',['labelAlignment',['http://doc.qt.io/qt-5/qformlayout.html#labelAlignment-prop',1,'QFormLayout']]],
  ['labeltext_23580',['labelText',['http://doc.qt.io/qt-5/qinputdialog.html#labelText-prop',1,'QInputDialog::labelText()'],['http://doc.qt.io/qt-5/qprogressdialog.html#labelText-prop',1,'QProgressDialog::labelText()']]],
  ['lastcenterpoint_23581',['lastCenterPoint',['http://doc.qt.io/qt-5/qpinchgesture.html#lastCenterPoint-prop',1,'QPinchGesture']]],
  ['lastoffset_23582',['lastOffset',['http://doc.qt.io/qt-5/qpangesture.html#lastOffset-prop',1,'QPanGesture']]],
  ['lastrotationangle_23583',['lastRotationAngle',['http://doc.qt.io/qt-5/qpinchgesture.html#lastRotationAngle-prop',1,'QPinchGesture']]],
  ['lastscalefactor_23584',['lastScaleFactor',['http://doc.qt.io/qt-5/qpinchgesture.html#lastScaleFactor-prop',1,'QPinchGesture']]],
  ['layout_23585',['layout',['http://doc.qt.io/qt-5/qgraphicswidget.html#layout-prop',1,'QGraphicsWidget']]],
  ['layoutdirection_23586',['layoutDirection',['http://doc.qt.io/qt-5/qguiapplication.html#layoutDirection-prop',1,'QGuiApplication::layoutDirection()'],['http://doc.qt.io/qt-5/qwidget.html#layoutDirection-prop',1,'QWidget::layoutDirection()'],['http://doc.qt.io/qt-5/qgraphicswidget.html#layoutDirection-prop',1,'QGraphicsWidget::layoutDirection()']]],
  ['layoutmode_23587',['layoutMode',['http://doc.qt.io/qt-5/qlistview.html#layoutMode-prop',1,'QListView']]],
  ['lazychildcount_23588',['lazyChildCount',['http://doc.qt.io/qt-5/qdirmodel.html#lazyChildCount-prop',1,'QDirModel']]],
  ['leftpadding_23589',['leftPadding',['../../../GUI.1/class_cute_h_m_i_1_1_g_u_i_1_1_number_display.html#ad27b385df96bc486b913fb9e79007c48',1,'CuteHMI::GUI::NumberDisplay::leftPadding()'],['../../../GUI.1/class_cute_h_m_i_1_1_g_u_i_1_1_prop_item.html#a3493b835a8728fb04658fa11980ea998',1,'CuteHMI::GUI::PropItem::leftPadding()']]],
  ['linewidth_23590',['lineWidth',['http://doc.qt.io/qt-5/qframe.html#lineWidth-prop',1,'QFrame']]],
  ['linewrapcolumnorwidth_23591',['lineWrapColumnOrWidth',['http://doc.qt.io/qt-5/qtextedit.html#lineWrapColumnOrWidth-prop',1,'QTextEdit']]],
  ['linewrapmode_23592',['lineWrapMode',['http://doc.qt.io/qt-5/qtextedit.html#lineWrapMode-prop',1,'QTextEdit::lineWrapMode()'],['http://doc.qt.io/qt-5/qplaintextedit.html#lineWrapMode-prop',1,'QPlainTextEdit::lineWrapMode()']]],
  ['loadhints_23593',['loadHints',['http://doc.qt.io/qt-5/qlibrary.html#loadHints-prop',1,'QLibrary::loadHints()'],['http://doc.qt.io/qt-5/qpluginloader.html#loadHints-prop',1,'QPluginLoader::loadHints()']]],
  ['locale_23594',['locale',['http://doc.qt.io/qt-5/qinputmethod.html#locale-prop',1,'QInputMethod::locale()'],['http://doc.qt.io/qt-5/qwidget.html#locale-prop',1,'QWidget::locale()']]],
  ['loggingmode_23595',['loggingMode',['http://doc.qt.io/qt-5/qopengldebuglogger.html#loggingMode-prop',1,'QOpenGLDebugLogger']]],
  ['logicaldotsperinch_23596',['logicalDotsPerInch',['http://doc.qt.io/qt-5/qscreen.html#logicalDotsPerInch-prop',1,'QScreen']]],
  ['logicaldotsperinchx_23597',['logicalDotsPerInchX',['http://doc.qt.io/qt-5/qscreen.html#logicalDotsPerInchX-prop',1,'QScreen']]],
  ['logicaldotsperinchy_23598',['logicalDotsPerInchY',['http://doc.qt.io/qt-5/qscreen.html#logicalDotsPerInchY-prop',1,'QScreen']]],
  ['loopcount_23599',['loopCount',['http://doc.qt.io/qt-5/qabstractanimation.html#loopCount-prop',1,'QAbstractAnimation::loopCount()'],['http://doc.qt.io/qt-5/qtimeline.html#loopCount-prop',1,'QTimeLine::loopCount()']]]
];
