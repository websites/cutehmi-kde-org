var hierarchy =
[
    [ "ColumnLayout", null, [
      [ "CuteHMI::Examples::Symbols::HVAC::Gallery::ElementSettings", "class_cute_h_m_i_1_1_examples_1_1_symbols_1_1_h_v_a_c_1_1_gallery_1_1_element_settings.html", [
        [ "CuteHMI::Examples::Symbols::HVAC::Gallery::AirFilterSettings", "class_cute_h_m_i_1_1_examples_1_1_symbols_1_1_h_v_a_c_1_1_gallery_1_1_air_filter_settings.html", null ],
        [ "CuteHMI::Examples::Symbols::HVAC::Gallery::BasicDiscreteInstrumentSettings", "class_cute_h_m_i_1_1_examples_1_1_symbols_1_1_h_v_a_c_1_1_gallery_1_1_basic_discrete_instrument_settings.html", null ],
        [ "CuteHMI::Examples::Symbols::HVAC::Gallery::BladeDamperSettings", "class_cute_h_m_i_1_1_examples_1_1_symbols_1_1_h_v_a_c_1_1_gallery_1_1_blade_damper_settings.html", null ],
        [ "CuteHMI::Examples::Symbols::HVAC::Gallery::CentrifugalFanSettings", "class_cute_h_m_i_1_1_examples_1_1_symbols_1_1_h_v_a_c_1_1_gallery_1_1_centrifugal_fan_settings.html", null ],
        [ "CuteHMI::Examples::Symbols::HVAC::Gallery::HeatRecoveryWheelSettings", "class_cute_h_m_i_1_1_examples_1_1_symbols_1_1_h_v_a_c_1_1_gallery_1_1_heat_recovery_wheel_settings.html", null ],
        [ "CuteHMI::Examples::Symbols::HVAC::Gallery::MotorActuatorSettings", "class_cute_h_m_i_1_1_examples_1_1_symbols_1_1_h_v_a_c_1_1_gallery_1_1_motor_actuator_settings.html", null ],
        [ "CuteHMI::Examples::Symbols::HVAC::Gallery::PumpSettings", "class_cute_h_m_i_1_1_examples_1_1_symbols_1_1_h_v_a_c_1_1_gallery_1_1_pump_settings.html", null ],
        [ "CuteHMI::Examples::Symbols::HVAC::Gallery::TankSettings", "class_cute_h_m_i_1_1_examples_1_1_symbols_1_1_h_v_a_c_1_1_gallery_1_1_tank_settings.html", null ],
        [ "CuteHMI::Examples::Symbols::HVAC::Gallery::ValveSettings", "class_cute_h_m_i_1_1_examples_1_1_symbols_1_1_h_v_a_c_1_1_gallery_1_1_valve_settings.html", null ]
      ] ]
    ] ],
    [ "Item", null, [
      [ "CuteHMI::Examples::Symbols::HVAC::Gallery::tst_View", "class_cute_h_m_i_1_1_examples_1_1_symbols_1_1_h_v_a_c_1_1_gallery_1_1tst___view.html", null ]
    ] ],
    [ "QObject", "http://doc.qt.io/qt-5/qobject.html", [
      [ "test_QML", "classtest___q_m_l.html", null ]
    ] ],
    [ "Rectangle", null, [
      [ "CuteHMI::Examples::Symbols::HVAC::Gallery::View", "class_cute_h_m_i_1_1_examples_1_1_symbols_1_1_h_v_a_c_1_1_gallery_1_1_view.html", null ]
    ] ]
];