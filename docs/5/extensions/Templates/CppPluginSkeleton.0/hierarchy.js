var hierarchy =
[
    [ "cutehmi::NonCopyable", "../../CuteHMI.2/classcutehmi_1_1_non_copyable.html", [
      [ "cutehmi::Initializer< Init >", "../../CuteHMI.2/classcutehmi_1_1_initializer.html", [
        [ "templates::cpppluginskeleton::Init", "classtemplates_1_1cpppluginskeleton_1_1_init.html", null ]
      ] ],
      [ "cutehmi::Initializer< Init >", "../../CuteHMI.2/classcutehmi_1_1_initializer.html", null ]
    ] ],
    [ "QException", "http://doc.qt.io/qt-5/qexception.html", [
      [ "cutehmi::Exception", "../../CuteHMI.2/classcutehmi_1_1_exception.html", [
        [ "cutehmi::ExceptionMixin< Exception >", "../../CuteHMI.2/classcutehmi_1_1_exception_mixin.html", [
          [ "templates::cpppluginskeleton::Exception", "classtemplates_1_1cpppluginskeleton_1_1_exception.html", null ]
        ] ]
      ] ]
    ] ],
    [ "QObject", "http://doc.qt.io/qt-5/qobject.html", [
      [ "QQmlExtensionPlugin", "http://doc.qt.io/qt-5/qqmlextensionplugin.html", [
        [ "templates::cpppluginskeleton::internal::QMLPlugin", "classtemplates_1_1cpppluginskeleton_1_1internal_1_1_q_m_l_plugin.html", null ]
      ] ]
    ] ]
];