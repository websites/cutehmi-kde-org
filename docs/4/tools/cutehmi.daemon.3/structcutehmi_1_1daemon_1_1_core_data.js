var structcutehmi_1_1daemon_1_1_core_data =
[
    [ "Options", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html", "structcutehmi_1_1daemon_1_1_core_data_1_1_options" ],
    [ "app", "structcutehmi_1_1daemon_1_1_core_data.html#a99117220932de245965f8488dbd6dff0", null ],
    [ "cmd", "structcutehmi_1_1daemon_1_1_core_data.html#a1f0b22cf268b8b1f09ea3405b1775abc", null ],
    [ "daemonTranslationFile", "structcutehmi_1_1daemon_1_1_core_data.html#af6fa40015fd294b4a7332fdd23e1f523", null ],
    [ "daemonTranslationFilePrefix", "structcutehmi_1_1daemon_1_1_core_data.html#af9435bb62f66aec7dcd231ecacf5cb8d", null ],
    [ "daemonTranslator", "structcutehmi_1_1daemon_1_1_core_data.html#aef98462a66f55aa1e66ef048d7f2d6c7", null ],
    [ "failedTranslationsFiles", "structcutehmi_1_1daemon_1_1_core_data.html#a573e8962d948bf6cb7a434df0c7c130a", null ],
    [ "language", "structcutehmi_1_1daemon_1_1_core_data.html#a49e37e75e174770abde6ad7373e7170b", null ],
    [ "opt", "structcutehmi_1_1daemon_1_1_core_data.html#a665ff0b5ca13b1e784d6d6f12a606c0c", null ],
    [ "qtTranslator", "structcutehmi_1_1daemon_1_1_core_data.html#a626db196124a3a6dfb3f1c86fead476f", null ],
    [ "translationsDir", "structcutehmi_1_1daemon_1_1_core_data.html#a988112673b7d8e11ae9f85ba19725f04", null ]
];