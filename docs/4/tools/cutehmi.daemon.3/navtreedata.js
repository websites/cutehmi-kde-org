/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CuteHMI - Daemon (cutehmi.daemon.3)", "index.html", [
    [ "Main Page", "../../index.html", null ],
    [ "Extensions", "../../extensions_list.html", null ],
    [ "Tools", "../../tools_list.html", null ],
    [ "Daemon", "index.html", [
      [ "Linux", "index.html#autotoc_md1", [
        [ "Signals", "index.html#autotoc_md2", null ],
        [ "Forking", "index.html#autotoc_md3", [
          [ "Single forking daemon", "index.html#autotoc_md4", null ],
          [ "Double forking daemon", "index.html#autotoc_md5", null ],
          [ "No forking", "index.html#autotoc_md6", null ]
        ] ]
      ] ],
      [ "Changes", "index.html#autotoc_md8", null ],
      [ "References", "index.html#autotoc_md9", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"../../extensions_list.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';