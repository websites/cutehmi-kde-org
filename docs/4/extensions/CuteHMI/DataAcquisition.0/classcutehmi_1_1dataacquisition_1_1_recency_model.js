var classcutehmi_1_1dataacquisition_1_1_recency_model =
[
    [ "Role", "classcutehmi_1_1dataacquisition_1_1_recency_model.html#a98607b9e1fa0d9b48b153f54ce0ffaa7", [
      [ "TAG_ROLE", "classcutehmi_1_1dataacquisition_1_1_recency_model.html#a98607b9e1fa0d9b48b153f54ce0ffaa7a0d4120d434d6cf437e1e9d49f17be468", null ],
      [ "VALUE_ROLE", "classcutehmi_1_1dataacquisition_1_1_recency_model.html#a98607b9e1fa0d9b48b153f54ce0ffaa7afe6a6052549507b02449fc95f4ec073c", null ],
      [ "TIME_ROLE", "classcutehmi_1_1dataacquisition_1_1_recency_model.html#a98607b9e1fa0d9b48b153f54ce0ffaa7ac95acaa4ed282ac818979419b335786d", null ]
    ] ],
    [ "RecencyModel", "classcutehmi_1_1dataacquisition_1_1_recency_model.html#ab30853d1d3227cf084dd62b02e792564", null ],
    [ "busy", "classcutehmi_1_1dataacquisition_1_1_recency_model.html#a80a9694b00b186f5be2e6d277c9f6721", null ],
    [ "confirmUpdateFinished", "classcutehmi_1_1dataacquisition_1_1_recency_model.html#a2c63e17a596bf1e440934201968fdc72", null ],
    [ "data", "classcutehmi_1_1dataacquisition_1_1_recency_model.html#a68f1be1b2b4a9daf414903fce1d4543d", null ],
    [ "requestUpdate", "classcutehmi_1_1dataacquisition_1_1_recency_model.html#a0c572b3854b48dc39d4e2414d62d7176", null ],
    [ "roleNames", "classcutehmi_1_1dataacquisition_1_1_recency_model.html#a5bb554448dc99bc60af68d34f45ebcee", null ],
    [ "rowCount", "classcutehmi_1_1dataacquisition_1_1_recency_model.html#a7145ed71daa46202c70aef3beaa88916", null ],
    [ "setTags", "classcutehmi_1_1dataacquisition_1_1_recency_model.html#a3e1644964324a7a76d7b2a9639bb0b50", null ],
    [ "tags", "classcutehmi_1_1dataacquisition_1_1_recency_model.html#a1f45275b0c7b001babc8c3c15df2bec8", null ],
    [ "tagsChanged", "classcutehmi_1_1dataacquisition_1_1_recency_model.html#a8cbfc666cc4b3f1014ef211a995dc2b2", null ],
    [ "internal::ModelMixin< RecencyModel >", "classcutehmi_1_1dataacquisition_1_1_recency_model.html#a9f61bfbe38529e5024870479fb3819c6", null ],
    [ "tags", "classcutehmi_1_1dataacquisition_1_1_recency_model.html#a0cb2a04fd7ec542edac3e8d52d6770e7", null ]
];