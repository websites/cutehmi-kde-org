/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CuteHMI - Data Acquisition (CuteHMI.DataAcquisition.0)", "index.html", [
    [ "Main Page", "../../../index.html", null ],
    [ "Extensions", "../../../extensions_list.html", null ],
    [ "Tools", "../../../tools_list.html", null ],
    [ "Data Acquisition", "index.html", [
      [ "Creating the schema", "index.html#autotoc_md1", [
        [ "Console tool", "index.html#autotoc_md2", [
          [ "TL;DR", "index.html#autotoc_md3", null ],
          [ "Custom setup", "index.html#autotoc_md4", null ]
        ] ],
        [ "PostgreSQL", "index.html#autotoc_md5", null ],
        [ "SQLite", "index.html#autotoc_md6", null ]
      ] ]
    ] ],
    [ "Assumptions", "assumptions.html", null ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Properties", "functions_prop.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"../../../extensions_list.html",
"classcutehmi_1_1dataacquisition_1_1internal_1_1_event_collective.html#a887e7859beaaad623910d8eca2dc200f"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';