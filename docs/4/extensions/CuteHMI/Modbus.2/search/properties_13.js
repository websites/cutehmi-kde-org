var searchData=
[
  ['undoredoenabled_19738',['undoRedoEnabled',['http://doc.qt.io/qt-5/qtextdocument.html#undoRedoEnabled-prop',1,'QTextDocument']]],
  ['updateinterval_19739',['updateInterval',['http://doc.qt.io/qt-5/qtimeline.html#updateInterval-prop',1,'QTimeLine']]],
  ['url_19740',['url',['http://doc.qt.io/qt-5/qqmlcomponent.html#url-prop',1,'QQmlComponent::url()'],['http://doc.qt.io/qt-5/qquickitemgrabresult.html#url-prop',1,'QQuickItemGrabResult::url()']]],
  ['usedesignmetrics_19741',['useDesignMetrics',['http://doc.qt.io/qt-5/qtextdocument.html#useDesignMetrics-prop',1,'QTextDocument']]],
  ['usehovereffects_19742',['useHoverEffects',['http://doc.qt.io/qt-5/qstylehints.html#useHoverEffects-prop',1,'QStyleHints']]],
  ['usertlextensions_19743',['useRtlExtensions',['http://doc.qt.io/qt-5/qstylehints.html#useRtlExtensions-prop',1,'QStyleHints']]]
];
