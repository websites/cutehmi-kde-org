var searchData=
[
  ['objectname_19647',['objectName',['http://doc.qt.io/qt-5/qobject.html#objectName-prop',1,'QObject']]],
  ['offlinestoragepath_19648',['offlineStoragePath',['http://doc.qt.io/qt-5/qqmlengine.html#offlineStoragePath-prop',1,'QQmlEngine']]],
  ['opacity_19649',['opacity',['http://doc.qt.io/qt-5/qwindow.html#opacity-prop',1,'QWindow::opacity()'],['http://doc.qt.io/qt-5/qquickitem.html#opacity-prop',1,'QQuickItem::opacity()']]],
  ['organizationdomain_19650',['organizationDomain',['http://doc.qt.io/qt-5/qcoreapplication.html#organizationDomain-prop',1,'QCoreApplication']]],
  ['organizationname_19651',['organizationName',['http://doc.qt.io/qt-5/qcoreapplication.html#organizationName-prop',1,'QCoreApplication']]],
  ['orientation_19652',['orientation',['http://doc.qt.io/qt-5/qscreen.html#orientation-prop',1,'QScreen']]]
];
