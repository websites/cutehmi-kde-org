var searchData=
[
  ['genericiconname_24741',['genericIconName',['http://doc.qt.io/qt-5/qmimetype.html#genericIconName-prop',1,'QMimeType']]],
  ['geometry_24742',['geometry',['http://doc.qt.io/qt-5/qscreen.html#geometry-prop',1,'QScreen::geometry()'],['http://doc.qt.io/qt-5/qwidget.html#geometry-prop',1,'QWidget::geometry()'],['http://doc.qt.io/qt-5/qgraphicswidget.html#geometry-prop',1,'QGraphicsWidget::geometry()']]],
  ['gesturecancelpolicy_24743',['gestureCancelPolicy',['http://doc.qt.io/qt-5/qgesture.html#gestureCancelPolicy-prop',1,'QGesture']]],
  ['gesturetype_24744',['gestureType',['http://doc.qt.io/qt-5/qgesture.html#gestureType-prop',1,'QGesture']]],
  ['globalrestorepolicy_24745',['globalRestorePolicy',['http://doc.qt.io/qt-5/qstatemachine.html#globalRestorePolicy-prop',1,'QStateMachine']]],
  ['globalstrut_24746',['globalStrut',['http://doc.qt.io/qt-5/qapplication.html#globalStrut-prop',1,'QApplication']]],
  ['globpatterns_24747',['globPatterns',['http://doc.qt.io/qt-5/qmimetype.html#globPatterns-prop',1,'QMimeType']]],
  ['gridsize_24748',['gridSize',['http://doc.qt.io/qt-5/qlistview.html#gridSize-prop',1,'QListView']]],
  ['gridstyle_24749',['gridStyle',['http://doc.qt.io/qt-5/qtableview.html#gridStyle-prop',1,'QTableView']]],
  ['gridvisible_24750',['gridVisible',['http://doc.qt.io/qt-5/qcalendarwidget.html#gridVisible-prop',1,'QCalendarWidget']]]
];
