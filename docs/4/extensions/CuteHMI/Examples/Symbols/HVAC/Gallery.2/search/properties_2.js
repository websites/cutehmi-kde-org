var searchData=
[
  ['cachemode_23385',['cacheMode',['http://doc.qt.io/qt-5/qmovie.html#cacheMode-prop',1,'QMovie::cacheMode()'],['http://doc.qt.io/qt-5/qgraphicsview.html#cacheMode-prop',1,'QGraphicsView::cacheMode()']]],
  ['calendarpopup_23386',['calendarPopup',['http://doc.qt.io/qt-5/qdatetimeedit.html#calendarPopup-prop',1,'QDateTimeEdit']]],
  ['cancelbuttontext_23387',['cancelButtonText',['http://doc.qt.io/qt-5/qinputdialog.html#cancelButtonText-prop',1,'QInputDialog']]],
  ['canredo_23388',['canRedo',['http://doc.qt.io/qt-5/qundostack.html#canRedo-prop',1,'QUndoStack']]],
  ['canundo_23389',['canUndo',['http://doc.qt.io/qt-5/qundostack.html#canUndo-prop',1,'QUndoStack']]],
  ['cascadingsectionresizes_23390',['cascadingSectionResizes',['http://doc.qt.io/qt-5/qheaderview.html#cascadingSectionResizes-prop',1,'QHeaderView']]],
  ['casesensitivity_23391',['caseSensitivity',['http://doc.qt.io/qt-5/qcompleter.html#caseSensitivity-prop',1,'QCompleter']]],
  ['centerbuttons_23392',['centerButtons',['http://doc.qt.io/qt-5/qdialogbuttonbox.html#centerButtons-prop',1,'QDialogButtonBox']]],
  ['centeronscroll_23393',['centerOnScroll',['http://doc.qt.io/qt-5/qplaintextedit.html#centerOnScroll-prop',1,'QPlainTextEdit']]],
  ['centerpoint_23394',['centerPoint',['http://doc.qt.io/qt-5/qpinchgesture.html#centerPoint-prop',1,'QPinchGesture']]],
  ['changecurrentondrag_23395',['changeCurrentOnDrag',['http://doc.qt.io/qt-5/qtabbar.html#changeCurrentOnDrag-prop',1,'QTabBar']]],
  ['changeflags_23396',['changeFlags',['http://doc.qt.io/qt-5/qpinchgesture.html#changeFlags-prop',1,'QPinchGesture']]],
  ['checkable_23397',['checkable',['http://doc.qt.io/qt-5/qabstractbutton.html#checkable-prop',1,'QAbstractButton::checkable()'],['http://doc.qt.io/qt-5/qaction.html#checkable-prop',1,'QAction::checkable()'],['http://doc.qt.io/qt-5/qgroupbox.html#checkable-prop',1,'QGroupBox::checkable()']]],
  ['checked_23398',['checked',['http://doc.qt.io/qt-5/qabstractbutton.html#checked-prop',1,'QAbstractButton::checked()'],['http://doc.qt.io/qt-5/qaction.html#checked-prop',1,'QAction::checked()'],['http://doc.qt.io/qt-5/qgroupbox.html#checked-prop',1,'QGroupBox::checked()']]],
  ['childmode_23399',['childMode',['http://doc.qt.io/qt-5/qstate.html#childMode-prop',1,'QState']]],
  ['childrencollapsible_23400',['childrenCollapsible',['http://doc.qt.io/qt-5/qsplitter.html#childrenCollapsible-prop',1,'QSplitter']]],
  ['childrenrect_23401',['childrenRect',['http://doc.qt.io/qt-5/qquickitem.html#childrenRect-prop',1,'QQuickItem::childrenRect()'],['http://doc.qt.io/qt-5/qwidget.html#childrenRect-prop',1,'QWidget::childrenRect()']]],
  ['childrenregion_23402',['childrenRegion',['http://doc.qt.io/qt-5/qwidget.html#childrenRegion-prop',1,'QWidget']]],
  ['clean_23403',['clean',['http://doc.qt.io/qt-5/qundostack.html#clean-prop',1,'QUndoStack']]],
  ['cleanicon_23404',['cleanIcon',['http://doc.qt.io/qt-5/qundoview.html#cleanIcon-prop',1,'QUndoView']]],
  ['cleantext_23405',['cleanText',['http://doc.qt.io/qt-5/qspinbox.html#cleanText-prop',1,'QSpinBox::cleanText()'],['http://doc.qt.io/qt-5/qdoublespinbox.html#cleanText-prop',1,'QDoubleSpinBox::cleanText()']]],
  ['clearbuttonenabled_23406',['clearButtonEnabled',['http://doc.qt.io/qt-5/qlineedit.html#clearButtonEnabled-prop',1,'QLineEdit']]],
  ['clip_23407',['clip',['http://doc.qt.io/qt-5/qquickitem.html#clip-prop',1,'QQuickItem']]],
  ['clipping_23408',['clipping',['http://doc.qt.io/qt-5/qitemdelegate.html#clipping-prop',1,'QItemDelegate']]],
  ['clockwise_23409',['clockwise',['../../../../../Symbols/HVAC.1/class_cute_h_m_i_1_1_symbols_1_1_h_v_a_c_1_1_heat_recovery_wheel.html#adccca22f3f87c87047e6239da41a7f26',1,'CuteHMI::Symbols::HVAC::HeatRecoveryWheel']]],
  ['color_23410',['color',['http://doc.qt.io/qt-5/qquickwindow.html#color-prop',1,'QQuickWindow::color()'],['http://doc.qt.io/qt-5/qgraphicscolorizeeffect.html#color-prop',1,'QGraphicsColorizeEffect::color()'],['http://doc.qt.io/qt-5/qgraphicsdropshadoweffect.html#color-prop',1,'QGraphicsDropShadowEffect::color()'],['../../../../../GUI.1/class_cute_h_m_i_1_1_g_u_i_1_1_element.html#a27a0b117370e21acd3e02076c7df21c9',1,'CuteHMI::GUI::Element::color()'],['../../../../../GUI.1/class_cute_h_m_i_1_1_g_u_i_1_1_prop_item.html#a5dfb09b19777053f253db49bad3ce4db',1,'CuteHMI::GUI::PropItem::color()']]],
  ['colorset_23411',['colorSet',['../../../../../GUI.1/class_cute_h_m_i_1_1_g_u_i_1_1_element.html#a110bbc32210918e2628d2ef1d06b4609',1,'CuteHMI::GUI::Element']]],
  ['columncount_23412',['columnCount',['http://doc.qt.io/qt-5/qtablewidget.html#columnCount-prop',1,'QTableWidget::columnCount()'],['http://doc.qt.io/qt-5/qtreewidget.html#columnCount-prop',1,'QTreeWidget::columnCount()']]],
  ['comboboxeditable_23413',['comboBoxEditable',['http://doc.qt.io/qt-5/qinputdialog.html#comboBoxEditable-prop',1,'QInputDialog']]],
  ['comboboxitems_23414',['comboBoxItems',['http://doc.qt.io/qt-5/qinputdialog.html#comboBoxItems-prop',1,'QInputDialog']]],
  ['comment_23415',['comment',['http://doc.qt.io/qt-5/qmimetype.html#comment-prop',1,'QMimeType']]],
  ['completioncolumn_23416',['completionColumn',['http://doc.qt.io/qt-5/qcompleter.html#completionColumn-prop',1,'QCompleter']]],
  ['completionmode_23417',['completionMode',['http://doc.qt.io/qt-5/qcompleter.html#completionMode-prop',1,'QCompleter']]],
  ['completionprefix_23418',['completionPrefix',['http://doc.qt.io/qt-5/qcompleter.html#completionPrefix-prop',1,'QCompleter']]],
  ['completionrole_23419',['completionRole',['http://doc.qt.io/qt-5/qcompleter.html#completionRole-prop',1,'QCompleter']]],
  ['confirmoverwrite_23420',['confirmOverwrite',['http://doc.qt.io/qt-5/qfiledialog-obsolete.html#confirmOverwrite-prop',1,'QFileDialog']]],
  ['containmentmask_23421',['containmentMask',['http://doc.qt.io/qt-5/qquickitem.html#containmentMask-prop',1,'QQuickItem']]],
  ['content_23422',['content',['../../../../../Symbols/HVAC.1/class_cute_h_m_i_1_1_symbols_1_1_h_v_a_c_1_1_air_filter.html#ac1700e20aeb16385082d5c6688f3d3f1',1,'CuteHMI::Symbols::HVAC::AirFilter::content()'],['../../../../../Symbols/HVAC.1/class_cute_h_m_i_1_1_symbols_1_1_h_v_a_c_1_1_heat_exchanger.html#ad65877be77e2d9567ff03d50605c7d0c',1,'CuteHMI::Symbols::HVAC::HeatExchanger::content()']]],
  ['contentdata_23423',['contentData',['../../../../../GUI.1/class_cute_h_m_i_1_1_g_u_i_1_1_prop_item.html#af1b6ce0a5fe86bd576907808550f332a',1,'CuteHMI::GUI::PropItem']]],
  ['contentitem_23424',['contentItem',['http://doc.qt.io/qt-5/qquickwindow.html#contentItem-prop',1,'QQuickWindow']]],
  ['contentorientation_23425',['contentOrientation',['http://doc.qt.io/qt-5/qwindow.html#contentOrientation-prop',1,'QWindow']]],
  ['contentsscale_23426',['contentsScale',['http://doc.qt.io/qt-5/qquickpainteditem-obsolete.html#contentsScale-prop',1,'QQuickPaintedItem']]],
  ['contentssize_23427',['contentsSize',['http://doc.qt.io/qt-5/qquickpainteditem-obsolete.html#contentsSize-prop',1,'QQuickPaintedItem']]],
  ['context_23428',['context',['http://doc.qt.io/qt-5/qshortcut.html#context-prop',1,'QShortcut']]],
  ['contextmenupolicy_23429',['contextMenuPolicy',['http://doc.qt.io/qt-5/qwidget.html#contextMenuPolicy-prop',1,'QWidget']]],
  ['cornerbuttonenabled_23430',['cornerButtonEnabled',['http://doc.qt.io/qt-5/qtableview.html#cornerButtonEnabled-prop',1,'QTableView']]],
  ['correctionmode_23431',['correctionMode',['http://doc.qt.io/qt-5/qabstractspinbox.html#correctionMode-prop',1,'QAbstractSpinBox']]],
  ['count_23432',['count',['http://doc.qt.io/qt-5/qtabbar.html#count-prop',1,'QTabBar::count()'],['http://doc.qt.io/qt-5/qtabwidget.html#count-prop',1,'QTabWidget::count()'],['http://doc.qt.io/qt-5/qcombobox.html#count-prop',1,'QComboBox::count()'],['http://doc.qt.io/qt-5/qlistwidget.html#count-prop',1,'QListWidget::count()'],['http://doc.qt.io/qt-5/qstackedlayout.html#count-prop',1,'QStackedLayout::count()'],['http://doc.qt.io/qt-5/qstackedwidget.html#count-prop',1,'QStackedWidget::count()'],['http://doc.qt.io/qt-5/qtoolbox.html#count-prop',1,'QToolBox::count()']]],
  ['currentanimation_23433',['currentAnimation',['http://doc.qt.io/qt-5/qsequentialanimationgroup.html#currentAnimation-prop',1,'QSequentialAnimationGroup']]],
  ['currentcolor_23434',['currentColor',['http://doc.qt.io/qt-5/qcolordialog.html#currentColor-prop',1,'QColorDialog']]],
  ['currentdata_23435',['currentData',['http://doc.qt.io/qt-5/qcombobox.html#currentData-prop',1,'QComboBox']]],
  ['currentfont_23436',['currentFont',['http://doc.qt.io/qt-5/qfontcombobox.html#currentFont-prop',1,'QFontComboBox::currentFont()'],['http://doc.qt.io/qt-5/qfontdialog.html#currentFont-prop',1,'QFontDialog::currentFont()']]],
  ['currentid_23437',['currentId',['http://doc.qt.io/qt-5/qwizard.html#currentId-prop',1,'QWizard']]],
  ['currentindex_23438',['currentIndex',['http://doc.qt.io/qt-5/qtabbar.html#currentIndex-prop',1,'QTabBar::currentIndex()'],['http://doc.qt.io/qt-5/qtabwidget.html#currentIndex-prop',1,'QTabWidget::currentIndex()'],['http://doc.qt.io/qt-5/qcombobox.html#currentIndex-prop',1,'QComboBox::currentIndex()'],['http://doc.qt.io/qt-5/qdatawidgetmapper.html#currentIndex-prop',1,'QDataWidgetMapper::currentIndex()'],['http://doc.qt.io/qt-5/qstackedlayout.html#currentIndex-prop',1,'QStackedLayout::currentIndex()'],['http://doc.qt.io/qt-5/qstackedwidget.html#currentIndex-prop',1,'QStackedWidget::currentIndex()'],['http://doc.qt.io/qt-5/qtoolbox.html#currentIndex-prop',1,'QToolBox::currentIndex()']]],
  ['currentloop_23439',['currentLoop',['http://doc.qt.io/qt-5/qabstractanimation.html#currentLoop-prop',1,'QAbstractAnimation']]],
  ['currentrow_23440',['currentRow',['http://doc.qt.io/qt-5/qlistwidget.html#currentRow-prop',1,'QListWidget']]],
  ['currentsection_23441',['currentSection',['http://doc.qt.io/qt-5/qdatetimeedit.html#currentSection-prop',1,'QDateTimeEdit']]],
  ['currentsectionindex_23442',['currentSectionIndex',['http://doc.qt.io/qt-5/qdatetimeedit.html#currentSectionIndex-prop',1,'QDateTimeEdit']]],
  ['currenttext_23443',['currentText',['http://doc.qt.io/qt-5/qcombobox.html#currentText-prop',1,'QComboBox']]],
  ['currenttime_23444',['currentTime',['http://doc.qt.io/qt-5/qabstractanimation.html#currentTime-prop',1,'QAbstractAnimation::currentTime()'],['http://doc.qt.io/qt-5/qtimeline.html#currentTime-prop',1,'QTimeLine::currentTime()']]],
  ['currentvalue_23445',['currentValue',['http://doc.qt.io/qt-5/qvariantanimation.html#currentValue-prop',1,'QVariantAnimation']]],
  ['cursor_23446',['cursor',['http://doc.qt.io/qt-5/qwidget.html#cursor-prop',1,'QWidget']]],
  ['cursorflashtime_23447',['cursorFlashTime',['http://doc.qt.io/qt-5/qstylehints.html#cursorFlashTime-prop',1,'QStyleHints::cursorFlashTime()'],['http://doc.qt.io/qt-5/qapplication.html#cursorFlashTime-prop',1,'QApplication::cursorFlashTime()']]],
  ['cursormovestyle_23448',['cursorMoveStyle',['http://doc.qt.io/qt-5/qlineedit.html#cursorMoveStyle-prop',1,'QLineEdit']]],
  ['cursorposition_23449',['cursorPosition',['http://doc.qt.io/qt-5/qlineedit.html#cursorPosition-prop',1,'QLineEdit']]],
  ['cursorrectangle_23450',['cursorRectangle',['http://doc.qt.io/qt-5/qinputmethod.html#cursorRectangle-prop',1,'QInputMethod']]],
  ['cursorwidth_23451',['cursorWidth',['http://doc.qt.io/qt-5/qtextedit.html#cursorWidth-prop',1,'QTextEdit::cursorWidth()'],['http://doc.qt.io/qt-5/qplaintextedit.html#cursorWidth-prop',1,'QPlainTextEdit::cursorWidth()'],['http://doc.qt.io/qt-5/qplaintextdocumentlayout.html#cursorWidth-prop',1,'QPlainTextDocumentLayout::cursorWidth()']]],
  ['curveshape_23452',['curveShape',['http://doc.qt.io/qt-5/qtimeline.html#curveShape-prop',1,'QTimeLine']]]
];
