var searchData=
[
  ['handlewidth_23507',['handleWidth',['http://doc.qt.io/qt-5/qsplitter.html#handleWidth-prop',1,'QSplitter']]],
  ['hashotspot_23508',['hasHotSpot',['http://doc.qt.io/qt-5/qgesture.html#hasHotSpot-prop',1,'QGesture']]],
  ['hasselectedtext_23509',['hasSelectedText',['http://doc.qt.io/qt-5/qlineedit.html#hasSelectedText-prop',1,'QLineEdit::hasSelectedText()'],['http://doc.qt.io/qt-5/qlabel.html#hasSelectedText-prop',1,'QLabel::hasSelectedText()']]],
  ['headerhidden_23510',['headerHidden',['http://doc.qt.io/qt-5/qtreeview.html#headerHidden-prop',1,'QTreeView']]],
  ['height_23511',['height',['http://doc.qt.io/qt-5/qwindow.html#height-prop',1,'QWindow::height()'],['http://doc.qt.io/qt-5/qquickitem.html#height-prop',1,'QQuickItem::height()'],['http://doc.qt.io/qt-5/qwidget.html#height-prop',1,'QWidget::height()']]],
  ['highlightsections_23512',['highlightSections',['http://doc.qt.io/qt-5/qheaderview.html#highlightSections-prop',1,'QHeaderView']]],
  ['historytype_23513',['historyType',['http://doc.qt.io/qt-5/qhistorystate.html#historyType-prop',1,'QHistoryState']]],
  ['horizontaldirection_23514',['horizontalDirection',['http://doc.qt.io/qt-5/qswipegesture.html#horizontalDirection-prop',1,'QSwipeGesture']]],
  ['horizontalheaderformat_23515',['horizontalHeaderFormat',['http://doc.qt.io/qt-5/qcalendarwidget.html#horizontalHeaderFormat-prop',1,'QCalendarWidget']]],
  ['horizontalscrollbarpolicy_23516',['horizontalScrollBarPolicy',['http://doc.qt.io/qt-5/qabstractscrollarea.html#horizontalScrollBarPolicy-prop',1,'QAbstractScrollArea']]],
  ['horizontalscrollmode_23517',['horizontalScrollMode',['http://doc.qt.io/qt-5/qabstractitemview.html#horizontalScrollMode-prop',1,'QAbstractItemView']]],
  ['horizontalspacing_23518',['horizontalSpacing',['http://doc.qt.io/qt-5/qgridlayout.html#horizontalSpacing-prop',1,'QGridLayout::horizontalSpacing()'],['http://doc.qt.io/qt-5/qformlayout.html#horizontalSpacing-prop',1,'QFormLayout::horizontalSpacing()']]],
  ['hotspot_23519',['hotSpot',['http://doc.qt.io/qt-5/qgesture.html#hotSpot-prop',1,'QGesture']]],
  ['html_23520',['html',['http://doc.qt.io/qt-5/qtextedit.html#html-prop',1,'QTextEdit']]]
];
