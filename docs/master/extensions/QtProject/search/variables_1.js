var searchData=
[
  ['backbutton_0',['BackButton',['http://doc.qt.io/qt-5/qt.html#MouseButton-enum',1,'Qt']]],
  ['backgroundcolorrole_1',['BackgroundColorRole',['http://doc.qt.io/qt-5/qt.html#ItemDataRole-enum',1,'Qt']]],
  ['backgroundrole_2',['BackgroundRole',['http://doc.qt.io/qt-5/qt.html#ItemDataRole-enum',1,'Qt']]],
  ['backtabfocusreason_3',['BacktabFocusReason',['http://doc.qt.io/qt-5/qt.html#FocusReason-enum',1,'Qt']]],
  ['backward_4',['Backward',['http://doc.qt.io/qt-5/qtimeline.html#Direction-enum',1,'QTimeLine::Backward()'],['http://doc.qt.io/qt-5/qabstractanimation.html#Direction-enum',1,'QAbstractAnimation::Backward()']]],
  ['bafia_5',['Bafia',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['bahamas_6',['Bahamas',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['bahrain_7',['Bahrain',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['balinese_8',['Balinese',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['balinesescript_9',['BalineseScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['bambara_10',['Bambara',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['bamumscript_11',['BamumScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['bamun_12',['Bamun',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['bangladesh_13',['Bangladesh',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['barbados_14',['Barbados',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['basaa_15',['Basaa',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['base64encoding_16',['Base64Encoding',['http://doc.qt.io/qt-5/qbytearray.html#Base64Option-enum',1,'QByteArray']]],
  ['base64options_17',['Base64Options',['http://doc.qt.io/qt-5/qbytearray.html#Base64Option-enum',1,'QByteArray']]],
  ['base64urlencoding_18',['Base64UrlEncoding',['http://doc.qt.io/qt-5/qbytearray.html#Base64Option-enum',1,'QByteArray']]],
  ['bashkir_19',['Bashkir',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['basque_20',['Basque',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['bassa_21',['Bassa',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['bassavahscript_22',['BassaVahScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['batakscript_23',['BatakScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['bataktoba_24',['BatakToba',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['bdiagpattern_25',['BDiagPattern',['http://doc.qt.io/qt-5/qt.html#BrushStyle-enum',1,'Qt']]],
  ['beginnativegesture_26',['BeginNativeGesture',['http://doc.qt.io/qt-5/qt.html#NativeGestureType-enum',1,'Qt']]],
  ['belarus_27',['Belarus',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['belarusian_28',['Belarusian',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['belgium_29',['Belgium',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['belize_30',['Belize',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['bemba_31',['Bemba',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['bena_32',['Bena',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['bengali_33',['Bengali',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['bengaliscript_34',['BengaliScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['benin_35',['Benin',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['bermuda_36',['Bermuda',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['beveljoin_37',['BevelJoin',['http://doc.qt.io/qt-5/qt.html#PenJoinStyle-enum',1,'Qt']]],
  ['bezierspline_38',['BezierSpline',['http://doc.qt.io/qt-5/qeasingcurve.html#Type-enum',1,'QEasingCurve']]],
  ['bhaiksukiscript_39',['BhaiksukiScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['bhojpuri_40',['Bhojpuri',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['bhutan_41',['Bhutan',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['bhutani_42',['Bhutani',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['bigendian_43',['BigEndian',['http://doc.qt.io/qt-5/qdatastream.html#ByteOrder-enum',1,'QDataStream::BigEndian()'],['http://doc.qt.io/qt-5/qsysinfo.html#Endian-enum',1,'QSysInfo::BigEndian()']]],
  ['bihari_44',['Bihari',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['binariespath_45',['BinariesPath',['http://doc.qt.io/qt-5/qlibraryinfo.html#LibraryLocation-enum',1,'QLibraryInfo']]],
  ['bislama_46',['Bislama',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['bitarray_47',['BitArray',['http://doc.qt.io/qt-5/qvariant-obsolete.html#Type-enum',1,'QVariant']]],
  ['bitmap_48',['Bitmap',['http://doc.qt.io/qt-5/qvariant-obsolete.html#Type-enum',1,'QVariant']]],
  ['bitmapcursor_49',['BitmapCursor',['http://doc.qt.io/qt-5/qt.html#CursorShape-enum',1,'Qt']]],
  ['black_50',['black',['http://doc.qt.io/qt-5/qt.html#GlobalColor-enum',1,'Qt']]],
  ['blankcursor_51',['BlankCursor',['http://doc.qt.io/qt-5/qt.html#CursorShape-enum',1,'Qt']]],
  ['blin_52',['Blin',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['blockingqueuedconnection_53',['BlockingQueuedConnection',['http://doc.qt.io/qt-5/qt.html#ConnectionType-enum',1,'Qt']]],
  ['blue_54',['blue',['http://doc.qt.io/qt-5/qt.html#GlobalColor-enum',1,'Qt']]],
  ['bodo_55',['Bodo',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['bolivia_56',['Bolivia',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['bonaire_57',['Bonaire',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['bool_58',['Bool',['http://doc.qt.io/qt-5/qvariant-obsolete.html#Type-enum',1,'QVariant::Bool()'],['http://doc.qt.io/qt-5/qmetatype.html#Type-enum',1,'QMetaType::Bool()'],['http://doc.qt.io/qt-5/qjsonvalue.html#Type-enum',1,'QJsonValue::Bool()']]],
  ['bopomofoscript_59',['BopomofoScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['bosniaandherzegowina_60',['BosniaAndHerzegowina',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['bosnian_61',['Bosnian',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['botswana_62',['Botswana',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['bottomdockwidgetarea_63',['BottomDockWidgetArea',['http://doc.qt.io/qt-5/qt.html#DockWidgetArea-enum',1,'Qt']]],
  ['bottomedge_64',['BottomEdge',['http://doc.qt.io/qt-5/qt.html#Edge-enum',1,'Qt']]],
  ['bottomleftcorner_65',['BottomLeftCorner',['http://doc.qt.io/qt-5/qt.html#Corner-enum',1,'Qt']]],
  ['bottomleftsection_66',['BottomLeftSection',['http://doc.qt.io/qt-5/qt.html#WindowFrameSection-enum',1,'Qt']]],
  ['bottomrightcorner_67',['BottomRightCorner',['http://doc.qt.io/qt-5/qt.html#Corner-enum',1,'Qt']]],
  ['bottomrightsection_68',['BottomRightSection',['http://doc.qt.io/qt-5/qt.html#WindowFrameSection-enum',1,'Qt']]],
  ['bottomsection_69',['BottomSection',['http://doc.qt.io/qt-5/qt.html#WindowFrameSection-enum',1,'Qt']]],
  ['bottomtoolbararea_70',['BottomToolBarArea',['http://doc.qt.io/qt-5/qt.html#ToolBarArea-enum',1,'Qt']]],
  ['boundaryreasons_71',['BoundaryReasons',['http://doc.qt.io/qt-5/qtextboundaryfinder.html#BoundaryReason-enum',1,'QTextBoundaryFinder']]],
  ['boundedintersection_72',['BoundedIntersection',['http://doc.qt.io/qt-5/qlinef-obsolete.html#IntersectType-enum',1,'QLineF']]],
  ['bouvetisland_73',['BouvetIsland',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['brahmiscript_74',['BrahmiScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['braillescript_75',['BrailleScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['brazil_76',['Brazil',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['breakopportunity_77',['BreakOpportunity',['http://doc.qt.io/qt-5/qtextboundaryfinder.html#BoundaryReason-enum',1,'QTextBoundaryFinder']]],
  ['breton_78',['Breton',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['britishindianoceanterritory_79',['BritishIndianOceanTerritory',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['britishvirginislands_80',['BritishVirginIslands',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['brunei_81',['Brunei',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['brush_82',['Brush',['http://doc.qt.io/qt-5/qvariant-obsolete.html#Type-enum',1,'QVariant']]],
  ['buginese_83',['Buginese',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['buginesescript_84',['BugineseScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['buhid_85',['Buhid',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['buhidscript_86',['BuhidScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['bulgaria_87',['Bulgaria',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['bulgarian_88',['Bulgarian',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['burkinafaso_89',['BurkinaFaso',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['burmese_90',['Burmese',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['burundi_91',['Burundi',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['busycursor_92',['BusyCursor',['http://doc.qt.io/qt-5/qt.html#CursorShape-enum',1,'Qt']]],
  ['byelorussian_93',['Byelorussian',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['bypassgraphicsproxywidget_94',['BypassGraphicsProxyWidget',['http://doc.qt.io/qt-5/qt.html#WindowType-enum',1,'Qt']]],
  ['bypassvalidation_95',['BypassValidation',['http://doc.qt.io/qt-5/qjsondocument.html#DataValidation-enum',1,'QJsonDocument']]],
  ['bypasswindowmanagerhint_96',['BypassWindowManagerHint',['http://doc.qt.io/qt-5/qt.html#WindowType-enum',1,'Qt']]],
  ['bytearray_97',['ByteArray',['http://doc.qt.io/qt-5/qcborstreamreader.html#Type-enum',1,'QCborStreamReader::ByteArray()'],['http://doc.qt.io/qt-5/qcborvalue.html#Type-enum',1,'QCborValue::ByteArray()'],['http://doc.qt.io/qt-5/qvariant-obsolete.html#Type-enum',1,'QVariant::ByteArray()']]],
  ['byteorder_98',['ByteOrder',['http://doc.qt.io/qt-5/qsysinfo.html#Endian-enum',1,'QSysInfo']]],
  ['byteordermark_99',['ByteOrderMark',['http://doc.qt.io/qt-5/qchar.html#SpecialCharacter-enum',1,'QChar']]],
  ['byteorderswapped_100',['ByteOrderSwapped',['http://doc.qt.io/qt-5/qchar.html#SpecialCharacter-enum',1,'QChar']]],
  ['bytestring_101',['ByteString',['http://doc.qt.io/qt-5/qcborstreamreader.html#Type-enum',1,'QCborStreamReader']]]
];
