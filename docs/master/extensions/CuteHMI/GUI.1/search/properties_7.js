var searchData=
[
  ['handlewidth_0',['handleWidth',['http://doc.qt.io/qt-5/qsplitter.html#handleWidth-prop',1,'QSplitter']]],
  ['hashotspot_1',['hasHotSpot',['http://doc.qt.io/qt-5/qgesture.html#hasHotSpot-prop',1,'QGesture']]],
  ['hasselectedtext_2',['hasSelectedText',['http://doc.qt.io/qt-5/qlabel.html#hasSelectedText-prop',1,'QLabel::hasSelectedText()'],['http://doc.qt.io/qt-5/qlineedit.html#hasSelectedText-prop',1,'QLineEdit::hasSelectedText()']]],
  ['headerhidden_3',['headerHidden',['http://doc.qt.io/qt-5/qtreeview.html#headerHidden-prop',1,'QTreeView']]],
  ['height_4',['height',['http://doc.qt.io/qt-5/qwindow.html#height-prop',1,'QWindow::height()'],['http://doc.qt.io/qt-5/qquickitem.html#height-prop',1,'QQuickItem::height()'],['http://doc.qt.io/qt-5/qwidget.html#height-prop',1,'QWidget::height()']]],
  ['highlightsections_5',['highlightSections',['http://doc.qt.io/qt-5/qheaderview.html#highlightSections-prop',1,'QHeaderView']]],
  ['historytype_6',['historyType',['http://doc.qt.io/qt-5/qhistorystate.html#historyType-prop',1,'QHistoryState']]],
  ['horizontaldirection_7',['horizontalDirection',['http://doc.qt.io/qt-5/qswipegesture.html#horizontalDirection-prop',1,'QSwipeGesture']]],
  ['horizontalheaderformat_8',['horizontalHeaderFormat',['http://doc.qt.io/qt-5/qcalendarwidget.html#horizontalHeaderFormat-prop',1,'QCalendarWidget']]],
  ['horizontalscrollbarpolicy_9',['horizontalScrollBarPolicy',['http://doc.qt.io/qt-5/qabstractscrollarea.html#horizontalScrollBarPolicy-prop',1,'QAbstractScrollArea']]],
  ['horizontalscrollmode_10',['horizontalScrollMode',['http://doc.qt.io/qt-5/qabstractitemview.html#horizontalScrollMode-prop',1,'QAbstractItemView']]],
  ['horizontalspacing_11',['horizontalSpacing',['http://doc.qt.io/qt-5/qformlayout.html#horizontalSpacing-prop',1,'QFormLayout::horizontalSpacing()'],['http://doc.qt.io/qt-5/qgridlayout.html#horizontalSpacing-prop',1,'QGridLayout::horizontalSpacing()']]],
  ['hotspot_12',['hotSpot',['http://doc.qt.io/qt-5/qgesture.html#hotSpot-prop',1,'QGesture']]],
  ['html_13',['html',['http://doc.qt.io/qt-5/qtextedit.html#html-prop',1,'QTextEdit']]]
];
