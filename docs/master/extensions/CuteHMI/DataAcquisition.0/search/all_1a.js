var searchData=
[
  ['zambia_0',['Zambia',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['zarma_1',['Zarma',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['zaxis_2',['ZAxis',['http://doc.qt.io/qt-5/qt.html#Axis-enum',1,'Qt']]],
  ['zero_3',['zero',['https://en.cppreference.com/w/cpp/chrono/duration/zero.html',1,'std::chrono::nanoseconds::zero()'],['https://en.cppreference.com/w/cpp/chrono/duration/zero.html',1,'std::chrono::microseconds::zero()'],['https://en.cppreference.com/w/cpp/chrono/duration_values/zero.html',1,'std::chrono::duration_values::zero()'],['https://en.cppreference.com/w/cpp/chrono/duration/zero.html',1,'std::chrono::hours::zero()'],['https://en.cppreference.com/w/cpp/chrono/duration/zero.html',1,'std::chrono::milliseconds::zero()'],['https://en.cppreference.com/w/cpp/chrono/duration/zero.html',1,'std::chrono::duration::zero()'],['https://en.cppreference.com/w/cpp/chrono/duration/zero.html',1,'std::chrono::seconds::zero()'],['https://en.cppreference.com/w/cpp/chrono/duration/zero.html',1,'std::chrono::minutes::zero()']]],
  ['zerodigit_4',['zeroDigit',['http://doc.qt.io/qt-5/qlocale.html#zeroDigit',1,'QLocale']]],
  ['zerotimerevent_5',['ZeroTimerEvent',['http://doc.qt.io/qt-5/qevent.html#Type-enum',1,'QEvent']]],
  ['zetta_6',['zetta',['https://en.cppreference.com/w/cpp/numeric/ratio/ratio.html',1,'std']]],
  ['zhuang_7',['Zhuang',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['zimbabwe_8',['Zimbabwe',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['zlibcompression_9',['ZlibCompression',['http://doc.qt.io/qt-5/qresource.html#Compression-enum',1,'QResource']]],
  ['zoomnativegesture_10',['ZoomNativeGesture',['http://doc.qt.io/qt-5/qt.html#NativeGestureType-enum',1,'Qt']]],
  ['zorderchange_11',['ZOrderChange',['http://doc.qt.io/qt-5/qevent.html#Type-enum',1,'QEvent']]],
  ['zstdcompression_12',['ZstdCompression',['http://doc.qt.io/qt-5/qresource.html#Compression-enum',1,'QResource']]],
  ['zulu_13',['Zulu',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]]
];
