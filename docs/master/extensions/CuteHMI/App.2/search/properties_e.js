var searchData=
[
  ['pagesize_22427',['pageSize',['http://doc.qt.io/qt-5/qtextdocument.html#pageSize-prop',1,'QTextDocument']]],
  ['pagestep_22428',['pageStep',['http://doc.qt.io/qt-5/qabstractslider.html#pageStep-prop',1,'QAbstractSlider']]],
  ['palette_22429',['palette',['http://doc.qt.io/qt-5/qwidget.html#palette-prop',1,'QWidget::palette()'],['http://doc.qt.io/qt-5/qgraphicswidget.html#palette-prop',1,'QGraphicsWidget::palette()'],['http://doc.qt.io/qt-5/qgraphicsscene.html#palette-prop',1,'QGraphicsScene::palette()'],['../../../CuteHMI.2/class_cute_h_m_i_1_1_element.html#aa7c5e43e2ba686642ff9efdf8df0df48',1,'CuteHMI::Element::palette()']]],
  ['parent_22430',['parent',['http://doc.qt.io/qt-5/qgraphicsobject.html#parent-prop',1,'QGraphicsObject']]],
  ['parentmimetypes_22431',['parentMimeTypes',['http://doc.qt.io/qt-5/qmimetype.html#parentMimeTypes-prop',1,'QMimeType']]],
  ['passwordmaskcharacter_22432',['passwordMaskCharacter',['http://doc.qt.io/qt-5/qstylehints.html#passwordMaskCharacter-prop',1,'QStyleHints']]],
  ['passwordmaskdelay_22433',['passwordMaskDelay',['http://doc.qt.io/qt-5/qstylehints.html#passwordMaskDelay-prop',1,'QStyleHints']]],
  ['physicaldotsperinch_22434',['physicalDotsPerInch',['http://doc.qt.io/qt-5/qscreen.html#physicalDotsPerInch-prop',1,'QScreen']]],
  ['physicaldotsperinchx_22435',['physicalDotsPerInchX',['http://doc.qt.io/qt-5/qscreen.html#physicalDotsPerInchX-prop',1,'QScreen']]],
  ['physicaldotsperinchy_22436',['physicalDotsPerInchY',['http://doc.qt.io/qt-5/qscreen.html#physicalDotsPerInchY-prop',1,'QScreen']]],
  ['physicalsize_22437',['physicalSize',['http://doc.qt.io/qt-5/qscreen.html#physicalSize-prop',1,'QScreen']]],
  ['pixmap_22438',['pixmap',['http://doc.qt.io/qt-5/qlabel.html#pixmap-prop',1,'QLabel']]],
  ['placeholdertext_22439',['placeholderText',['http://doc.qt.io/qt-5/qlineedit.html#placeholderText-prop',1,'QLineEdit::placeholderText()'],['http://doc.qt.io/qt-5/qtextedit.html#placeholderText-prop',1,'QTextEdit::placeholderText()'],['http://doc.qt.io/qt-5/qplaintextedit.html#placeholderText-prop',1,'QPlainTextEdit::placeholderText()']]],
  ['plaintext_22440',['plainText',['http://doc.qt.io/qt-5/qtextedit.html#plainText-prop',1,'QTextEdit::plainText()'],['http://doc.qt.io/qt-5/qplaintextedit.html#plainText-prop',1,'QPlainTextEdit::plainText()']]],
  ['platformname_22441',['platformName',['http://doc.qt.io/qt-5/qguiapplication.html#platformName-prop',1,'QGuiApplication']]],
  ['popupmode_22442',['popupMode',['http://doc.qt.io/qt-5/qtoolbutton.html#popupMode-prop',1,'QToolButton']]],
  ['pos_22443',['pos',['http://doc.qt.io/qt-5/qwidget.html#pos-prop',1,'QWidget::pos()'],['http://doc.qt.io/qt-5/qgraphicsobject.html#pos-prop',1,'QGraphicsObject::pos()']]],
  ['position_22444',['position',['http://doc.qt.io/qt-5/qtapgesture.html#position-prop',1,'QTapGesture::position()'],['http://doc.qt.io/qt-5/qtapandholdgesture.html#position-prop',1,'QTapAndHoldGesture::position()']]],
  ['preferredsize_22445',['preferredSize',['http://doc.qt.io/qt-5/qgraphicswidget.html#preferredSize-prop',1,'QGraphicsWidget']]],
  ['preferredsuffix_22446',['preferredSuffix',['http://doc.qt.io/qt-5/qmimetype.html#preferredSuffix-prop',1,'QMimeType']]],
  ['prefix_22447',['prefix',['http://doc.qt.io/qt-5/qspinbox.html#prefix-prop',1,'QSpinBox::prefix()'],['http://doc.qt.io/qt-5/qdoublespinbox.html#prefix-prop',1,'QDoubleSpinBox::prefix()']]],
  ['primaryorientation_22448',['primaryOrientation',['http://doc.qt.io/qt-5/qscreen.html#primaryOrientation-prop',1,'QScreen']]],
  ['primaryscreen_22449',['primaryScreen',['http://doc.qt.io/qt-5/qguiapplication.html#primaryScreen-prop',1,'QGuiApplication::primaryScreen()'],['http://doc.qt.io/qt-5/qdesktopwidget-obsolete.html#primaryScreen-prop',1,'QDesktopWidget::primaryScreen()']]],
  ['priority_22450',['priority',['http://doc.qt.io/qt-5/qaction.html#priority-prop',1,'QAction']]],
  ['progress_22451',['progress',['http://doc.qt.io/qt-5/qqmlcomponent.html#progress-prop',1,'QQmlComponent']]],
  ['propertyname_22452',['propertyName',['http://doc.qt.io/qt-5/qpropertyanimation.html#propertyName-prop',1,'QPropertyAnimation']]]
];
