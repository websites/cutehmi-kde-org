var hierarchy =
[
    [ "QObject", "http://doc.qt.io/qt-5/qobject.html", [
      [ "QCoreApplication", "http://doc.qt.io/qt-5/qcoreapplication.html", [
        [ "QGuiApplication", "http://doc.qt.io/qt-5/qguiapplication.html", [
          [ "QApplication", "http://doc.qt.io/qt-5/qapplication.html", [
            [ "cutehmi::app::CuteApp", "classcutehmi_1_1app_1_1_cute_app.html", [
              [ "CuteHMI::App::CuteApp", "class_cute_h_m_i_1_1_app_1_1_cute_app.html", null ]
            ] ]
          ] ]
        ] ]
      ] ],
      [ "QQmlExtensionPlugin", "http://doc.qt.io/qt-5/qqmlextensionplugin.html", [
        [ "cutehmi::app::internal::QMLPlugin", "classcutehmi_1_1app_1_1internal_1_1_q_m_l_plugin.html", null ]
      ] ]
    ] ]
];