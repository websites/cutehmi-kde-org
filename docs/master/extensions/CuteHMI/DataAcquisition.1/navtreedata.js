/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CuteHMI - Data Acquisition (CuteHMI.DataAcquisition.1)", "index.html", [
    [ "Main Page", "../../../index.html", null ],
    [ "Extensions", "../../../extensions_list.html", null ],
    [ "Tools", "../../../tools_list.html", null ],
    [ "Data Acquisition", "index.html", [
      [ "Creating the schema", "index.html#autotoc_md3", [
        [ "Console tool", "index.html#autotoc_md4", [
          [ "TL;DR", "index.html#autotoc_md5", null ],
          [ "Custom setup", "index.html#autotoc_md6", null ]
        ] ],
        [ "PostgreSQL", "index.html#autotoc_md7", null ],
        [ "SQLite", "index.html#autotoc_md8", null ]
      ] ]
    ] ],
    [ "Assumptions", "assumptions.html", null ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Properties", "functions_prop.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"../../../extensions_list.html",
"classcutehmi_1_1dataacquisition_1_1_history_model.html#a4ea035eccf74a53ba64fa22e505d1b6a",
"classcutehmi_1_1dataacquisition_1_1_tag_value.html#a096dda7869c75764a2116ad7043c692c",
"structcutehmi_1_1dataacquisition_1_1internal_1_1_history_collective_1_1_tuple.html#abf8282fb76583c4b548b668099cba626"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';