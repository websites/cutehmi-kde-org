var searchData=
[
  ['namedcolorspace_0',['NamedColorSpace',['http://doc.qt.io/qt-5/qcolorspace.html#NamedColorSpace-enum',1,'QColorSpace']]],
  ['nameformat_1',['NameFormat',['http://doc.qt.io/qt-5/qcolor.html#NameFormat-enum',1,'QColor']]],
  ['nametype_2',['NameType',['http://doc.qt.io/qt-5/qtimezone.html#NameType-enum',1,'QTimeZone']]],
  ['nativegesturetype_3',['NativeGestureType',['http://doc.qt.io/qt-5/qt.html#NativeGestureType-enum',1,'Qt']]],
  ['nativeobjecttype_4',['NativeObjectType',['http://doc.qt.io/qt-5/qquickwindow.html#NativeObjectType-enum',1,'QQuickWindow']]],
  ['navigationmode_5',['NavigationMode',['http://doc.qt.io/qt-5/qt.html#NavigationMode-enum',1,'Qt']]],
  ['networkaccessibility_6',['NetworkAccessibility',['http://doc.qt.io/qt-5/qnetworkaccessmanager-obsolete.html#NetworkAccessibility-enum',1,'QNetworkAccessManager']]],
  ['networkerror_7',['NetworkError',['http://doc.qt.io/qt-5/qnetworkreply.html#NetworkError-enum',1,'QNetworkReply']]],
  ['networklayerprotocol_8',['NetworkLayerProtocol',['http://doc.qt.io/qt-5/qabstractsocket.html#NetworkLayerProtocol-enum',1,'QAbstractSocket']]],
  ['nextprotocolnegotiationstatus_9',['NextProtocolNegotiationStatus',['http://doc.qt.io/qt-5/qsslconfiguration.html#NextProtocolNegotiationStatus-enum',1,'QSslConfiguration']]],
  ['nodetype_10',['NodeType',['http://doc.qt.io/qt-5/qsgnode.html#NodeType-enum',1,'QSGNode']]],
  ['normalizationform_11',['NormalizationForm',['http://doc.qt.io/qt-5/qstring.html#NormalizationForm-enum',1,'QString']]],
  ['notation_12',['Notation',['http://doc.qt.io/qt-5/qdoublevalidator.html#Notation-enum',1,'QDoubleValidator']]],
  ['numberflag_13',['NumberFlag',['http://doc.qt.io/qt-5/qtextstream.html#NumberFlag-enum',1,'QTextStream']]],
  ['numberoption_14',['NumberOption',['http://doc.qt.io/qt-5/qlocale.html#NumberOption-enum',1,'QLocale']]]
];
