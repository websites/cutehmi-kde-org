/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CuteHMI - Modbus (CuteHMI.Modbus.4)", "index.html", [
    [ "Main Page", "../../../index.html", null ],
    [ "Extensions", "../../../extensions_list.html", null ],
    [ "Tools", "../../../tools_list.html", null ],
    [ "Modbus", "index.html", [
      [ "Device classes", "index.html#autotoc_md4", null ],
      [ "Register controllers", "index.html#autotoc_md5", null ],
      [ "Register items", "index.html#autotoc_md6", null ],
      [ "Relationship between classes", "index.html#autotoc_md7", null ],
      [ "QML components", "index.html#autotoc_md8", null ],
      [ "Examples", "index.html#autotoc_md9", null ],
      [ "Related pages", "index.html#autotoc_md10", null ]
    ] ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Properties", "functions_prop.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"../../../extensions_list.html",
"classcutehmi_1_1modbus_1_1_abstract_device.html#a3be076cc6a23f6cb9b72e2c30734480d",
"classcutehmi_1_1modbus_1_1_abstract_register_controller.html#a942db547ef4a3b6597006d4866221789",
"classcutehmi_1_1modbus_1_1_r_t_u_client.html#afce6389d851636cb81eb4b1544a5c69f",
"classcutehmi_1_1modbus_1_1_t_c_p_server.html#a380b62df82c5bdafeb93d8780a1b77e8",
"classcutehmi_1_1modbus_1_1internal_1_1_dummy_client_backend.html#ad537f399150605e33db0f80b8301c5ea",
"classcutehmi_1_1modbus_1_1internal_1_1_qt_server_mixin.html",
"index.html#autotoc_md5"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';