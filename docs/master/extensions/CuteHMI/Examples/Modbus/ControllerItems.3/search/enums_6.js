var searchData=
[
  ['gesturecancelpolicy_0',['GestureCancelPolicy',['http://doc.qt.io/qt-5/qgesture.html#GestureCancelPolicy-enum',1,'QGesture']]],
  ['gestureflag_1',['GestureFlag',['http://doc.qt.io/qt-5/qt.html#GestureFlag-enum',1,'Qt']]],
  ['gesturestate_2',['GestureState',['http://doc.qt.io/qt-5/qt.html#GestureState-enum',1,'Qt']]],
  ['gesturetype_3',['GestureType',['http://doc.qt.io/qt-5/qt.html#GestureType-enum',1,'Qt']]],
  ['globalcolor_4',['GlobalColor',['http://doc.qt.io/qt-5/qt.html#GlobalColor-enum',1,'Qt']]],
  ['glyphrunflag_5',['GlyphRunFlag',['http://doc.qt.io/qt-5/qglyphrun.html#GlyphRunFlag-enum',1,'QGlyphRun']]],
  ['graphicsapi_6',['GraphicsApi',['http://doc.qt.io/qt-5/qsgrendererinterface.html#GraphicsApi-enum',1,'QSGRendererInterface']]],
  ['graphicsitemchange_7',['GraphicsItemChange',['http://doc.qt.io/qt-5/qgraphicsitem.html#GraphicsItemChange-enum',1,'QGraphicsItem']]],
  ['graphicsitemflag_8',['GraphicsItemFlag',['http://doc.qt.io/qt-5/qgraphicsitem.html#GraphicsItemFlag-enum',1,'QGraphicsItem']]]
];
