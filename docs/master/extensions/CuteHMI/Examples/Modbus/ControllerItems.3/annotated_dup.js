var annotated_dup =
[
    [ "CuteHMI", "namespace_cute_h_m_i.html", [
      [ "Examples", "namespace_cute_h_m_i_1_1_examples.html", [
        [ "Modbus", "namespace_cute_h_m_i_1_1_examples_1_1_modbus.html", [
          [ "ControllerItems", "namespace_cute_h_m_i_1_1_examples_1_1_modbus_1_1_controller_items.html", [
            [ "Screen", "class_cute_h_m_i_1_1_examples_1_1_modbus_1_1_controller_items_1_1_screen.html", null ],
            [ "View", "class_cute_h_m_i_1_1_examples_1_1_modbus_1_1_controller_items_1_1_view.html", null ]
          ] ]
        ] ]
      ] ]
    ] ]
];