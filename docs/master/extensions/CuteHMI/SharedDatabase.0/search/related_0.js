var searchData=
[
  ['service_0',['Service',['../../Services.2/classcutehmi_1_1services_1_1_service_manager.html#a6474ceb8669761e879329b39c3afb899',1,'cutehmi::services::ServiceManager']]],
  ['servicemanager_1',['ServiceManager',['../../Services.2/classcutehmi_1_1services_1_1_service.html#afbee2935cd32a05038cdd44f6d753735',1,'cutehmi::services::Service']]],
  ['singleton_2',['Singleton',['../../Services.2/classcutehmi_1_1services_1_1_service_manager.html#a87235845261d4dcd1683c094ccf10641',1,'cutehmi::services::ServiceManager::Singleton()'],['../../../CuteHMI.2/classcutehmi_1_1_internationalizer.html#a5179bd94f3e88fda72bdea338677d584',1,'cutehmi::Internationalizer::Singleton()'],['../../../CuteHMI.2/classcutehmi_1_1_messenger.html#a5d14986b1a8c30cc553cc85bef4bb620',1,'cutehmi::Messenger::Singleton()'],['../../../CuteHMI.2/classcutehmi_1_1_notifier.html#a3b622a7382088d8fe59f96206290a916',1,'cutehmi::Notifier::Singleton()']]],
  ['singleton_3c_20databasedictionary_20_3e_3',['Singleton&lt; DatabaseDictionary &gt;',['../classcutehmi_1_1shareddatabase_1_1internal_1_1_database_dictionary.html#a203becf1f9375a5a80dc306aff5f2bd9',1,'cutehmi::shareddatabase::internal::DatabaseDictionary']]]
];
