var hierarchy =
[
    [ "cutehmi::Error", "structcutehmi_1_1_error.html", [
      [ "cutehmi::InplaceError", "structcutehmi_1_1_inplace_error.html", null ]
    ] ],
    [ "cutehmi::ErrorInfo", "structcutehmi_1_1_error_info.html", null ],
    [ "cutehmi::internal::LoggingCategoryCheck", "classcutehmi_1_1internal_1_1_logging_category_check.html", null ],
    [ "cutehmi::Messenger::Members", "structcutehmi_1_1_messenger_1_1_members.html", null ],
    [ "cutehmi::NonCopyable", "classcutehmi_1_1_non_copyable.html", [
      [ "cutehmi::Initializer< Init >", "classcutehmi_1_1_initializer.html", [
        [ "cutehmi::Init", "classcutehmi_1_1_init.html", null ]
      ] ],
      [ "cutehmi::Singleton< Internationalizer >", "classcutehmi_1_1_singleton.html", [
        [ "cutehmi::Internationalizer", "classcutehmi_1_1_internationalizer.html", [
          [ "CuteHMI::Internationalizer", "class_cute_h_m_i_1_1_internationalizer.html", null ]
        ] ]
      ] ],
      [ "cutehmi::Singleton< Messenger >", "classcutehmi_1_1_singleton.html", [
        [ "cutehmi::Messenger", "classcutehmi_1_1_messenger.html", [
          [ "CuteHMI::Messenger", "class_cute_h_m_i_1_1_messenger.html", null ]
        ] ]
      ] ],
      [ "cutehmi::Singleton< Notifier >", "classcutehmi_1_1_singleton.html", [
        [ "cutehmi::Notifier", "classcutehmi_1_1_notifier.html", [
          [ "CuteHMI::Notifier", "class_cute_h_m_i_1_1_notifier.html", null ]
        ] ]
      ] ],
      [ "cutehmi::Initializer< DERIVED >", "classcutehmi_1_1_initializer.html", null ],
      [ "cutehmi::Singleton< C >", "classcutehmi_1_1_singleton.html", null ]
    ] ],
    [ "cutehmi::NonMovable", "classcutehmi_1_1_non_movable.html", [
      [ "cutehmi::Singleton< Internationalizer >", "classcutehmi_1_1_singleton.html", null ],
      [ "cutehmi::Singleton< Messenger >", "classcutehmi_1_1_singleton.html", null ],
      [ "cutehmi::Singleton< Notifier >", "classcutehmi_1_1_singleton.html", null ],
      [ "cutehmi::Singleton< C >", "classcutehmi_1_1_singleton.html", null ]
    ] ],
    [ "QEvent", "http://doc.qt.io/qt-5/qevent.html", [
      [ "cutehmi::Worker::WorkEvent", "classcutehmi_1_1_worker_1_1_work_event.html", null ]
    ] ],
    [ "QException", "http://doc.qt.io/qt-5/qexception.html", [
      [ "cutehmi::Exception", "classcutehmi_1_1_exception.html", [
        [ "cutehmi::ExceptionMixin< ErrorException >", "classcutehmi_1_1_exception_mixin.html", [
          [ "cutehmi::ErrorException", "classcutehmi_1_1_error_exception.html", null ]
        ] ],
        [ "cutehmi::ExceptionMixin< NoAdvertiserException >", "classcutehmi_1_1_exception_mixin.html", [
          [ "cutehmi::Messenger::NoAdvertiserException", "classcutehmi_1_1_messenger_1_1_no_advertiser_exception.html", null ]
        ] ],
        [ "cutehmi::ExceptionMixin< DERIVED >", "classcutehmi_1_1_exception_mixin.html", null ]
      ] ]
    ] ],
    [ "QObject", "http://doc.qt.io/qt-5/qobject.html", [
      [ "QAbstractItemModel", "http://doc.qt.io/qt-5/qabstractitemmodel.html", [
        [ "QAbstractListModel", "http://doc.qt.io/qt-5/qabstractlistmodel.html", [
          [ "cutehmi::NotificationListModel", "classcutehmi_1_1_notification_list_model.html", null ]
        ] ]
      ] ],
      [ "QQmlEngineExtensionPlugin", "http://doc.qt.io/qt-5/qqmlengineextensionplugin.html", [
        [ "cutehmi::internal::QMLPlugin", "classcutehmi_1_1internal_1_1_q_m_l_plugin.html", null ]
      ] ],
      [ "cutehmi::Internationalizer", "classcutehmi_1_1_internationalizer.html", null ],
      [ "cutehmi::Message", "classcutehmi_1_1_message.html", [
        [ "CuteHMI::Message", "class_cute_h_m_i_1_1_message.html", null ]
      ] ],
      [ "cutehmi::Messenger", "classcutehmi_1_1_messenger.html", null ],
      [ "cutehmi::Notification", "classcutehmi_1_1_notification.html", [
        [ "CuteHMI::Notification", "class_cute_h_m_i_1_1_notification.html", null ]
      ] ],
      [ "cutehmi::Notifier", "classcutehmi_1_1_notifier.html", null ],
      [ "cutehmi::Worker", "classcutehmi_1_1_worker.html", null ]
    ] ],
    [ "std::unique_ptr< cutehmi::Messenger::Members, DELETER >", "https://en.cppreference.com/w/cpp/memory/unique_ptr.html", [
      [ "cutehmi::MPtr< cutehmi::Messenger::Members >", "classcutehmi_1_1_m_ptr.html", null ]
    ] ],
    [ "std::unique_ptr< Members, DELETER >", "https://en.cppreference.com/w/cpp/memory/unique_ptr.html", [
      [ "cutehmi::MPtr< Members >", "classcutehmi_1_1_m_ptr.html", null ]
    ] ],
    [ "std::unique_ptr< T, DELETER >", "https://en.cppreference.com/w/cpp/memory/unique_ptr.html", [
      [ "cutehmi::MPtr< T, DELETER >", "classcutehmi_1_1_m_ptr.html", null ]
    ] ]
];