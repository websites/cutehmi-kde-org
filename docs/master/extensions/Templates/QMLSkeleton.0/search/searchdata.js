var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz~",
  1: "abcdefghijklmnopqrstuvwyz",
  2: "qst",
  3: "adru",
  4: "_abcdefghijklmnopqrstuvwxyz~",
  5: "abcdefghijklmnopqrstuvwxyz",
  6: "abcdefghijklmnopqrstuvwy",
  7: "acdefghilmnopqrstuv",
  8: "adqu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "properties",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Properties",
  8: "Pages"
};

