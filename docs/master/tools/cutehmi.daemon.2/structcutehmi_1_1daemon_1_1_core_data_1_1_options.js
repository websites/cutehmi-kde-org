var structcutehmi_1_1daemon_1_1_core_data_1_1_options =
[
    [ "app", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#aa42b63a1c1ed6889eda62e682819d200", null ],
    [ "basedir", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#aa1cf3ffb46b8a3449ae667b14c07f450", null ],
    [ "component", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#ab7eab17d6cb23634e2eb24a05ba03e21", null ],
    [ "extension", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#a5b1f7f8f3ba88b8d0780d80a9fb51f27", null ],
    [ "init", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#ad2e092db033dab94bfc4849591cbf9f8", null ],
    [ "lang", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#ab433ef85bcbeb4782371195ca835303a", null ],
    [ "minor", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#a2e639e93a608fdf082857578ac932a40", null ],
    [ "nforks", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#a1205db16a1995a1235b441a575733153", null ],
    [ "pidfile", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#a80ce1b4392bc564b6f0e35110b4565ed", null ]
];