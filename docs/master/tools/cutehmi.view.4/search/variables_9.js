var searchData=
[
  ['jalali_0',['Jalali',['http://doc.qt.io/qt-5/qcalendar.html#System-enum',1,'QCalendar']]],
  ['jamaica_1',['Jamaica',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['jamoscript_2',['JamoScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['japan_3',['Japan',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['japanblush_4',['JapanBlush',['http://doc.qt.io/qt-5/qgradient.html#Preset-enum',1,'QGradient']]],
  ['japanese_5',['Japanese',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale::Japanese()'],['http://doc.qt.io/qt-5/qfontdatabase.html#WritingSystem-enum',1,'QFontDatabase::Japanese()']]],
  ['japanesescript_6',['JapaneseScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['javanese_7',['Javanese',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['javanesescript_8',['JavaneseScript',['http://doc.qt.io/qt-5/qlocale.html#Script-enum',1,'QLocale']]],
  ['javascriptfile_9',['JavaScriptFile',['http://doc.qt.io/qt-5/qqmlabstracturlinterceptor.html#DataType-enum',1,'QQmlAbstractUrlInterceptor']]],
  ['javascriptownership_10',['JavaScriptOwnership',['http://doc.qt.io/qt-5/qqmlengine.html#ObjectOwnership-enum',1,'QQmlEngine']]],
  ['jersey_11',['Jersey',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['jisb0_12',['JisB0',['http://doc.qt.io/qt-5/qpagesize.html#PageSizeId-enum',1,'QPageSize::JisB0()'],['http://doc.qt.io/qt-5/qpagedpaintdevice.html#PageSize-enum',1,'QPagedPaintDevice::JisB0()']]],
  ['jisb1_13',['JisB1',['http://doc.qt.io/qt-5/qpagesize.html#PageSizeId-enum',1,'QPageSize::JisB1()'],['http://doc.qt.io/qt-5/qpagedpaintdevice.html#PageSize-enum',1,'QPagedPaintDevice::JisB1()']]],
  ['jisb10_14',['JisB10',['http://doc.qt.io/qt-5/qpagesize.html#PageSizeId-enum',1,'QPageSize::JisB10()'],['http://doc.qt.io/qt-5/qpagedpaintdevice.html#PageSize-enum',1,'QPagedPaintDevice::JisB10()']]],
  ['jisb2_15',['JisB2',['http://doc.qt.io/qt-5/qpagesize.html#PageSizeId-enum',1,'QPageSize::JisB2()'],['http://doc.qt.io/qt-5/qpagedpaintdevice.html#PageSize-enum',1,'QPagedPaintDevice::JisB2()']]],
  ['jisb3_16',['JisB3',['http://doc.qt.io/qt-5/qpagesize.html#PageSizeId-enum',1,'QPageSize::JisB3()'],['http://doc.qt.io/qt-5/qpagedpaintdevice.html#PageSize-enum',1,'QPagedPaintDevice::JisB3()']]],
  ['jisb4_17',['JisB4',['http://doc.qt.io/qt-5/qpagedpaintdevice.html#PageSize-enum',1,'QPagedPaintDevice::JisB4()'],['http://doc.qt.io/qt-5/qpagesize.html#PageSizeId-enum',1,'QPageSize::JisB4()']]],
  ['jisb5_18',['JisB5',['http://doc.qt.io/qt-5/qpagesize.html#PageSizeId-enum',1,'QPageSize::JisB5()'],['http://doc.qt.io/qt-5/qpagedpaintdevice.html#PageSize-enum',1,'QPagedPaintDevice::JisB5()']]],
  ['jisb6_19',['JisB6',['http://doc.qt.io/qt-5/qpagesize.html#PageSizeId-enum',1,'QPageSize::JisB6()'],['http://doc.qt.io/qt-5/qpagedpaintdevice.html#PageSize-enum',1,'QPagedPaintDevice::JisB6()']]],
  ['jisb7_20',['JisB7',['http://doc.qt.io/qt-5/qpagesize.html#PageSizeId-enum',1,'QPageSize::JisB7()'],['http://doc.qt.io/qt-5/qpagedpaintdevice.html#PageSize-enum',1,'QPagedPaintDevice::JisB7()']]],
  ['jisb8_21',['JisB8',['http://doc.qt.io/qt-5/qpagesize.html#PageSizeId-enum',1,'QPageSize::JisB8()'],['http://doc.qt.io/qt-5/qpagedpaintdevice.html#PageSize-enum',1,'QPagedPaintDevice::JisB8()']]],
  ['jisb9_22',['JisB9',['http://doc.qt.io/qt-5/qpagesize.html#PageSizeId-enum',1,'QPageSize::JisB9()'],['http://doc.qt.io/qt-5/qpagedpaintdevice.html#PageSize-enum',1,'QPagedPaintDevice::JisB9()']]],
  ['jju_23',['Jju',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['joining_5fcausing_24',['Joining_Causing',['http://doc.qt.io/qt-5/qchar.html#JoiningType-enum',1,'QChar']]],
  ['joining_5fdual_25',['Joining_Dual',['http://doc.qt.io/qt-5/qchar.html#JoiningType-enum',1,'QChar']]],
  ['joining_5fleft_26',['Joining_Left',['http://doc.qt.io/qt-5/qchar.html#JoiningType-enum',1,'QChar']]],
  ['joining_5fnone_27',['Joining_None',['http://doc.qt.io/qt-5/qchar.html#JoiningType-enum',1,'QChar']]],
  ['joining_5fright_28',['Joining_Right',['http://doc.qt.io/qt-5/qchar.html#JoiningType-enum',1,'QChar']]],
  ['joining_5ftransparent_29',['Joining_Transparent',['http://doc.qt.io/qt-5/qchar.html#JoiningType-enum',1,'QChar']]],
  ['jolafonyi_30',['JolaFonyi',['http://doc.qt.io/qt-5/qlocale.html#Language-enum',1,'QLocale']]],
  ['jordan_31',['Jordan',['http://doc.qt.io/qt-5/qlocale.html#Country-enum',1,'QLocale']]],
  ['juicycake_32',['JuicyCake',['http://doc.qt.io/qt-5/qgradient.html#Preset-enum',1,'QGradient']]],
  ['juicypeach_33',['JuicyPeach',['http://doc.qt.io/qt-5/qgradient.html#Preset-enum',1,'QGradient']]],
  ['julian_34',['Julian',['http://doc.qt.io/qt-5/qcalendar.html#System-enum',1,'QCalendar']]],
  ['jungleday_35',['JungleDay',['http://doc.qt.io/qt-5/qgradient.html#Preset-enum',1,'QGradient']]]
];
