var structcutehmi_1_1daemon_1_1_core_data_1_1_options =
[
    [ "app", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#aa42b63a1c1ed6889eda62e682819d200", null ],
    [ "basedir", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#aa1cf3ffb46b8a3449ae667b14c07f450", null ],
    [ "component", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#aead713e9dfaa1f1593fdf6c620ac1679", null ],
    [ "extension", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#aceed94d544145da6fbc51cb96972b3a9", null ],
    [ "forks", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#af4e850b2a856eeb208ac8685828b5c6d", null ],
    [ "init", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#ad2e092db033dab94bfc4849591cbf9f8", null ],
    [ "lang", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#ab433ef85bcbeb4782371195ca835303a", null ],
    [ "minor", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#a2e639e93a608fdf082857578ac932a40", null ],
    [ "pidfile", "structcutehmi_1_1daemon_1_1_core_data_1_1_options.html#a80ce1b4392bc564b6f0e35110b4565ed", null ]
];