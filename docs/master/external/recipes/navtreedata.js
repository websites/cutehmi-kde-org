/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "External libraries recipes", "index.html", [
    [ "Main Page", "../../index.html", null ],
    [ "Extensions", "../../extensions_list.html", null ],
    [ "Tools", "../../tools_list.html", null ],
    [ "External libraries recipes.", "index.html", null ],
    [ "GNU gettext", "md_i686_w64_mingw32_gettext__r_e_a_d_m_e.html", null ],
    [ "GnuPG Libgcrypt", "md_i686_w64_mingw32_libgcrypt__r_e_a_d_m_e.html", null ],
    [ "libgit2", "md_i686_w64_mingw32_libgit2__r_e_a_d_m_e.html", null ],
    [ "GnuPG Libgpg-error", "md_i686_w64_mingw32_libgpg_error__r_e_a_d_m_e.html", null ],
    [ "GNU libiconv (internationalized)", "md_i686_w64_mingw32_libiconv_nls__r_e_a_d_m_e.html", null ],
    [ "GNU libiconv", "md_i686_w64_mingw32_libiconv__r_e_a_d_m_e.html", null ],
    [ "Libmodbus", "md_i686_w64_mingw32_libmodbus__r_e_a_d_m_e.html", null ],
    [ "Libssh", "md_i686_w64_mingw32_libssh__r_e_a_d_m_e.html", null ],
    [ "LIBSSH2", "md_i686_w64_mingw32_libssh2__r_e_a_d_m_e.html", null ],
    [ "pkg-config-lite", "md_i686_w64_mingw32_pkg_config_lite__r_e_a_d_m_e.html", null ],
    [ "PostgreSQL", "md_i686_w64_mingw32_postgresql__r_e_a_d_m_e.html", null ],
    [ "Windows mingw32 recipes.", "md_i686_w64_mingw32__r_e_a_d_m_e.html", null ],
    [ "Zlib", "md_i686_w64_mingw32_zlib__r_e_a_d_m_e.html", null ]
  ] ]
];

var NAVTREEINDEX =
[
"../../extensions_list.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';