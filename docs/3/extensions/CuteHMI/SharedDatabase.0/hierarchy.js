var hierarchy =
[
    [ "cutehmi::shareddatabase::internal::DatabaseConfig", "classcutehmi_1_1shareddatabase_1_1internal_1_1_database_config.html", null ],
    [ "cutehmi::NonCopyable", "../../CuteHMI.2/classcutehmi_1_1_non_copyable.html", [
      [ "cutehmi::Singleton< DatabaseDictionary >", "../../CuteHMI.2/classcutehmi_1_1_singleton.html", [
        [ "cutehmi::shareddatabase::internal::DatabaseDictionary", "classcutehmi_1_1shareddatabase_1_1internal_1_1_database_dictionary.html", null ]
      ] ]
    ] ],
    [ "cutehmi::NonMovable", "../../CuteHMI.2/classcutehmi_1_1_non_movable.html", [
      [ "cutehmi::Singleton< DatabaseDictionary >", "../../CuteHMI.2/classcutehmi_1_1_singleton.html", null ]
    ] ],
    [ "QException", "http://doc.qt.io/qt-5/qexception.html", [
      [ "cutehmi::Exception", "../../CuteHMI.2/classcutehmi_1_1_exception.html", [
        [ "cutehmi::ExceptionMixin< Exception >", "../../CuteHMI.2/classcutehmi_1_1_exception_mixin.html", [
          [ "cutehmi::shareddatabase::Exception", "classcutehmi_1_1shareddatabase_1_1_exception.html", null ]
        ] ]
      ] ]
    ] ],
    [ "QObject", "http://doc.qt.io/qt-5/qobject.html", [
      [ "cutehmi::shareddatabase::Database", "classcutehmi_1_1shareddatabase_1_1_database.html", [
        [ "CuteHMI::SharedDatabase::Database", "class_cute_h_m_i_1_1_shared_database_1_1_database.html", null ]
      ] ],
      [ "cutehmi::shareddatabase::DatabaseWorker", "classcutehmi_1_1shareddatabase_1_1_database_worker.html", null ],
      [ "cutehmi::shareddatabase::internal::DatabaseConnectionHandler", "classcutehmi_1_1shareddatabase_1_1internal_1_1_database_connection_handler.html", null ],
      [ "cutehmi::shareddatabase::internal::DatabaseDictionary", "classcutehmi_1_1shareddatabase_1_1internal_1_1_database_dictionary.html", null ],
      [ "QQmlExtensionPlugin", "http://doc.qt.io/qt-5/qqmlextensionplugin.html", [
        [ "cutehmi::shareddatabase::internal::QMLPlugin", "classcutehmi_1_1shareddatabase_1_1internal_1_1_q_m_l_plugin.html", null ]
      ] ],
      [ "QThread", "http://doc.qt.io/qt-5/qthread.html", [
        [ "cutehmi::shareddatabase::internal::DatabaseThread", "classcutehmi_1_1shareddatabase_1_1internal_1_1_database_thread.html", null ]
      ] ]
    ] ],
    [ "QSharedData", "http://doc.qt.io/qt-5/qshareddata.html", [
      [ "cutehmi::shareddatabase::internal::DatabaseConfig::Data", "classcutehmi_1_1shareddatabase_1_1internal_1_1_database_config_1_1_data.html", null ]
    ] ],
    [ "cutehmi::services::Serviceable", "../Services.2/classcutehmi_1_1services_1_1_serviceable.html", [
      [ "cutehmi::shareddatabase::Database", "classcutehmi_1_1shareddatabase_1_1_database.html", null ]
    ] ]
];