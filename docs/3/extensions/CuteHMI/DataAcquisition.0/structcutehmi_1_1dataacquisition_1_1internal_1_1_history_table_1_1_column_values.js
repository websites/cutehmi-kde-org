var structcutehmi_1_1dataacquisition_1_1internal_1_1_history_table_1_1_column_values =
[
    [ "ColumnValues", "structcutehmi_1_1dataacquisition_1_1internal_1_1_history_table_1_1_column_values.html#aac86479d8c649f8beba2d5c5700afd59", null ],
    [ "close", "structcutehmi_1_1dataacquisition_1_1internal_1_1_history_table_1_1_column_values.html#ac8eec6e5974e16a8d160b6df704183ec", null ],
    [ "closeTime", "structcutehmi_1_1dataacquisition_1_1internal_1_1_history_table_1_1_column_values.html#a37bbae5ff610ec09301d6ba533769dbb", null ],
    [ "count", "structcutehmi_1_1dataacquisition_1_1internal_1_1_history_table_1_1_column_values.html#a7b8c5d9add74f6acf3aec35e3be536ef", null ],
    [ "max", "structcutehmi_1_1dataacquisition_1_1internal_1_1_history_table_1_1_column_values.html#a1af8a93146c433eadcbe47104d7aef04", null ],
    [ "min", "structcutehmi_1_1dataacquisition_1_1internal_1_1_history_table_1_1_column_values.html#a1ecbc97d6dee64949c6289e7f2f87778", null ],
    [ "open", "structcutehmi_1_1dataacquisition_1_1internal_1_1_history_table_1_1_column_values.html#a54dc1342c204b9934c8bafa403a5af97", null ],
    [ "openTime", "structcutehmi_1_1dataacquisition_1_1internal_1_1_history_table_1_1_column_values.html#a9cc6b9ecbab655f2959503591a2dcf5a", null ],
    [ "tagName", "structcutehmi_1_1dataacquisition_1_1internal_1_1_history_table_1_1_column_values.html#a4e0bc7be8ed722f9579df9fe25f240dd", null ]
];