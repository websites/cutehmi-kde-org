var hierarchy =
[
    [ "cutehmi::dataacquisition::internal::HistoryTable< T >::ColumnValues", "structcutehmi_1_1dataacquisition_1_1internal_1_1_history_table_1_1_column_values.html", null ],
    [ "cutehmi::dataacquisition::internal::RecencyTable< T >::ColumnValues", "structcutehmi_1_1dataacquisition_1_1internal_1_1_recency_table_1_1_column_values.html", null ],
    [ "QException", "http://doc.qt.io/qt-5/qexception.html", [
      [ "cutehmi::Exception", "../../CuteHMI.2/classcutehmi_1_1_exception.html", [
        [ "cutehmi::ExceptionMixin< Exception >", "../../CuteHMI.2/classcutehmi_1_1_exception_mixin.html", [
          [ "cutehmi::dataacquisition::Exception", "classcutehmi_1_1dataacquisition_1_1_exception.html", null ]
        ] ]
      ] ]
    ] ],
    [ "QObject", "http://doc.qt.io/qt-5/qobject.html", [
      [ "cutehmi::dataacquisition::AbstractWriter", "classcutehmi_1_1dataacquisition_1_1_abstract_writer.html", [
        [ "cutehmi::dataacquisition::EventWriter", "classcutehmi_1_1dataacquisition_1_1_event_writer.html", null ],
        [ "cutehmi::dataacquisition::HistoryWriter", "classcutehmi_1_1dataacquisition_1_1_history_writer.html", null ],
        [ "cutehmi::dataacquisition::RecencyWriter", "classcutehmi_1_1dataacquisition_1_1_recency_writer.html", null ]
      ] ],
      [ "cutehmi::dataacquisition::DataObject", "classcutehmi_1_1dataacquisition_1_1_data_object.html", [
        [ "cutehmi::dataacquisition::internal::TableObject", "classcutehmi_1_1dataacquisition_1_1internal_1_1_table_object.html", [
          [ "cutehmi::dataacquisition::internal::EventTable< T >", "classcutehmi_1_1dataacquisition_1_1internal_1_1_event_table.html", null ],
          [ "cutehmi::dataacquisition::internal::HistoryTable< T >", "classcutehmi_1_1dataacquisition_1_1internal_1_1_history_table.html", null ],
          [ "cutehmi::dataacquisition::internal::RecencyTable< T >", "classcutehmi_1_1dataacquisition_1_1internal_1_1_recency_table.html", null ],
          [ "cutehmi::dataacquisition::internal::TagCache", "classcutehmi_1_1dataacquisition_1_1internal_1_1_tag_cache.html", null ],
          [ "cutehmi::dataacquisition::internal::HistoryTable< bool >", "classcutehmi_1_1dataacquisition_1_1internal_1_1_history_table.html", null ],
          [ "cutehmi::dataacquisition::internal::HistoryTable< double >", "classcutehmi_1_1dataacquisition_1_1internal_1_1_history_table.html", null ],
          [ "cutehmi::dataacquisition::internal::HistoryTable< int >", "classcutehmi_1_1dataacquisition_1_1internal_1_1_history_table.html", null ]
        ] ],
        [ "cutehmi::dataacquisition::Schema", "classcutehmi_1_1dataacquisition_1_1_schema.html", null ]
      ] ],
      [ "cutehmi::dataacquisition::internal::TableCollective", "classcutehmi_1_1dataacquisition_1_1internal_1_1_table_collective.html", [
        [ "cutehmi::dataacquisition::internal::EventCollective", "classcutehmi_1_1dataacquisition_1_1internal_1_1_event_collective.html", null ],
        [ "cutehmi::dataacquisition::internal::HistoryCollective", "classcutehmi_1_1dataacquisition_1_1internal_1_1_history_collective.html", null ],
        [ "cutehmi::dataacquisition::internal::RecencyCollective", "classcutehmi_1_1dataacquisition_1_1internal_1_1_recency_collective.html", null ]
      ] ],
      [ "cutehmi::dataacquisition::TagValue", "classcutehmi_1_1dataacquisition_1_1_tag_value.html", null ],
      [ "QQmlExtensionPlugin", "http://doc.qt.io/qt-5/qqmlextensionplugin.html", [
        [ "cutehmi::dataacquisition::internal::QMLPlugin", "classcutehmi_1_1dataacquisition_1_1internal_1_1_q_m_l_plugin.html", null ]
      ] ]
    ] ],
    [ "cutehmi::services::Serviceable", "../Services.2/classcutehmi_1_1services_1_1_serviceable.html", [
      [ "cutehmi::dataacquisition::AbstractWriter", "classcutehmi_1_1dataacquisition_1_1_abstract_writer.html", null ]
    ] ],
    [ "cutehmi::dataacquisition::internal::TableNameTraits< T >", "structcutehmi_1_1dataacquisition_1_1internal_1_1_table_name_traits.html", null ],
    [ "cutehmi::dataacquisition::internal::TableNameTraits< bool >", "structcutehmi_1_1dataacquisition_1_1internal_1_1_table_name_traits_3_01bool_01_4.html", null ],
    [ "cutehmi::dataacquisition::internal::TableNameTraits< double >", "structcutehmi_1_1dataacquisition_1_1internal_1_1_table_name_traits_3_01double_01_4.html", null ],
    [ "cutehmi::dataacquisition::internal::TableNameTraits< int >", "structcutehmi_1_1dataacquisition_1_1internal_1_1_table_name_traits_3_01int_01_4.html", null ],
    [ "cutehmi::dataacquisition::internal::HistoryTable< T >::Tuple", "structcutehmi_1_1dataacquisition_1_1internal_1_1_history_table_1_1_tuple.html", null ],
    [ "cutehmi::dataacquisition::internal::RecencyTable< T >::Tuple", "structcutehmi_1_1dataacquisition_1_1internal_1_1_recency_table_1_1_tuple.html", null ],
    [ "cutehmi::dataacquisition::internal::EventTable< T >::Tuple", "structcutehmi_1_1dataacquisition_1_1internal_1_1_event_table_1_1_tuple.html", null ]
];