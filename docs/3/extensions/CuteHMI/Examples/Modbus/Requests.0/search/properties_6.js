var searchData=
[
  ['genericiconname_23739',['genericIconName',['http://doc.qt.io/qt-5/qmimetype.html#genericIconName-prop',1,'QMimeType']]],
  ['geometry_23740',['geometry',['http://doc.qt.io/qt-5/qscreen.html#geometry-prop',1,'QScreen::geometry()'],['http://doc.qt.io/qt-5/qwidget.html#geometry-prop',1,'QWidget::geometry()'],['http://doc.qt.io/qt-5/qgraphicswidget.html#geometry-prop',1,'QGraphicsWidget::geometry()']]],
  ['gesturecancelpolicy_23741',['gestureCancelPolicy',['http://doc.qt.io/qt-5/qgesture.html#gestureCancelPolicy-prop',1,'QGesture']]],
  ['gesturetype_23742',['gestureType',['http://doc.qt.io/qt-5/qgesture.html#gestureType-prop',1,'QGesture']]],
  ['globalrestorepolicy_23743',['globalRestorePolicy',['http://doc.qt.io/qt-5/qstatemachine.html#globalRestorePolicy-prop',1,'QStateMachine']]],
  ['globalstrut_23744',['globalStrut',['http://doc.qt.io/qt-5/qapplication.html#globalStrut-prop',1,'QApplication']]],
  ['globpatterns_23745',['globPatterns',['http://doc.qt.io/qt-5/qmimetype.html#globPatterns-prop',1,'QMimeType']]],
  ['gridsize_23746',['gridSize',['http://doc.qt.io/qt-5/qlistview.html#gridSize-prop',1,'QListView']]],
  ['gridstyle_23747',['gridStyle',['http://doc.qt.io/qt-5/qtableview.html#gridStyle-prop',1,'QTableView']]],
  ['gridvisible_23748',['gridVisible',['http://doc.qt.io/qt-5/qcalendarwidget.html#gridVisible-prop',1,'QCalendarWidget']]]
];
